const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const postSchema = new Schema({
    content: {
        type: String
    },
    like: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Like'
        }
    ],
    comments: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Comment'
        }
    ],
    imageUrl: {
        type: String,
        required: true
    },
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
}, {
    // cini da se zapisuje vreme kreiranje i update-ovanja kao dodatna dva jos polja(createdAt, updatedAt)
    timestamps: true
})

module.exports = mongoose.model('Post', postSchema);