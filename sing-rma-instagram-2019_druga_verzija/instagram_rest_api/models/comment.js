const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const commentSchema = new Schema({
    content: {
        type: String,
        required: true
    },
    like: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Like'
        }
    ],
    comments: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Comment'
        }
    ],
    comment: {
        type: Schema.Types.ObjectId,
        ref: 'Comment',
    },
    post: {
        type: Schema.Types.ObjectId,
        ref: 'Post',
    },
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
}, {
    // cini da se zapisuje vreme kreiranje i update-ovanja kao dodatna dva jos polja(createdAt, updatedAt)
    timestamps: true
})

module.exports = mongoose.model('Comment', commentSchema);