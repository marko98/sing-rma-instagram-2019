const express = require('express');

const feedController = require('../controllers/feed');

const router = express.Router();

// POST -> /feed/post-image
router.post('/post-image', feedController.putPostImage);

module.exports = router;