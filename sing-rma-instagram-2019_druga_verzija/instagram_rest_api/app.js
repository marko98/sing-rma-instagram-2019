const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const graphqlHttp = require('express-graphql');
const graphqlSchema = require('./graphql/schema');
const graphqlResolver = require('./graphql/resolvers');
const multer = require('multer');
const uuidv4 = require('uuid/v4');
const fs = require('fs');
const path = require('path');

const auth = require('./middleware/auth');
const feedRoutes = require('./routes/feed');

const app = express();

const fileStorage = multer.diskStorage({
    destination: function(req, file, cb) {
        // folder images mora vec da postoji inace nece raditi
        cb(null, "images");
    },
    filename: function(req, file, cb) {
        cb(null, uuidv4() + file.originalname);
        // cb(null, file.originalname);
    }
})

const fileFilter = (req, file, cb) => {
    if(
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpeg' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'jpg'
    ){
        cb(null, true);
    } else {
        cb(null, false);
    }
}

// app.use(bodyParser.urlencoded({extended: false})); // za x-www-forms-urlencoded <- default data ako je submitovana kroz formu
// bodyParser middlewear ce dodati req.body
app.use(bodyParser.json()); // za application/json
app.use(
    // multer({storage: fileStorage, fileFilter: fileFilter}).single('image')
    multer({storage: fileStorage}).single('image')
);

app.use((req, res, next) => {
    /**
     * za resavanje CORS gresaka(procitaj.txt 1)) moramo poslati informacije 
     * o tome sta dozvoljavamo na browser da bi on dozvolio komunikaciju izmedju
     * servera i klijenta
     * 
     * 'Access-Control-Allow-Origin' -> definisemo ko sve sme da pristupi nasem server-side serveru
     * npr.:
     * 'Access-Control-Allow-Origin': 'codepen.io,...' -> samo neki odredjeni
     * 'Access-Control-Allow-Origin': '*' -> svi
     * 
     * 'Access-Control-Allow-Methods' -> koje metode dozvoljavamo
     * 
     * 'Access-Control-Allow-Headers' -> koje header-e dozvoljavamo da budu postavljeni
     */
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    /**
     * GraphQL ne dozvoljava requestovima koji nisu metode 'POST' da prodju kroz njegov middlewear,
     * buduci da browser pre nego sto posalje request metode 'POST' ili 'PUT' salje request metode 'OPTIONS', da bi
     * video da li sme da posalje zeljeni request, GraphQL ce ga odbiti
     * 
     * zato moramo ovde da presretnemo 'OPTIONS' request i da vratimo response sa statusom 200 -> za dozvoljeno slanje 
     * zeljenog requesta
     */
    if(req.method === 'OPTIONS'){
        return res.sendStatus(200);
    }
    next();
});

// app.get('/json', (req, res, next) => {
//     res.
//         status(200)
//         .json({
//             "ime": "david"
//         });
// });

app.use('/images', express.static(path.join(__dirname, 'images')));

app.use(auth);

app.use('/feed', feedRoutes);

app.use('/graphql', graphqlHttp({
    schema: graphqlSchema,
    rootValue: graphqlResolver,
    graphiql: true, // poseti http://localhost:8080/graphql
    customFormatErrorFn(err) {
        if(!err.originalError){
            return err;
        }
        const data = err.originalError.data;
        const message = err.message || 'An error occurred.';
        const statusCode = err.originalError.statusCode || 500;
        return {message: message, status: statusCode, data: data};
    }
}));

app.use((error, req, res, next) => {
    console.log(error);

    const status = error.statusCode || 500;
    const data = error.data;
    // error.message -> string koji je prosledjen konstruktoru
    const message = error.message;
    res.
        status(status)
        .json({
            message: message,
            data: data
        });
});

mongoose
    .connect('mongodb+srv://marko:marko@cluster0-mekhr.mongodb.net/instagram?retryWrites=true&w=majority')
    .then(result => {
        app.listen(8080);
    })
    .catch(err => console.log(err));