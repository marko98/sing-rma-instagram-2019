const { buildSchema } = require('graphql');

module.exports = buildSchema(`
    type Comment {
        _id: ID!        
        content: String!
        like: [Like!]!
        comments: [Comment!]!
        comment: Comment!
        post: Post!
        creator: User!
        createdAt: String!
        updatedAt: String!
    }

    type Like {
        _id: ID!
        comment: Comment
        post: Post
        creator: User!
        createdAt: String!
        updatedAt: String!
    }

    type Post {
        _id: ID!        
        content: String!
        like: [Like!]!
        comments: [Comment!]!
        imageUrl: String!
        creator: User!
        createdAt: String!
        updatedAt: String!
    }

    type User {
        _id: ID!
        email: String!
        username: String!
        password: String!
        status: String!
        posts: [Post!]!
        following: [User!]
        followers: [User!]
        createdAt: String!
        updatedAt: String!
    }

    type AuthData {
        token: String!
        loggedUser: AuthDataUser!
    }

    type AuthDataUser {
        _id: String!
        email: String!
        username: String!
        status: String!
        posts: [Post!]!
        following: [User!]
        followers: [User!]
        createdAt: String!
        updatedAt: String!
        comments: [Comment!]
    }

    type GetUser {
        user: User!
        comments: [Comment!]
    }

    type GetUsersByUsername {
        message: String!
        users: [User!]!
        totalFoundUsers: Int!
        totalUsers: Int!
    }

    input UserInputData {
        email: String
        username: String
        fullName: String
        password: String
        status: String
    }

    input PostInputData {
        content: String!
        imageUrl: String!
    }

    type RootMutation {
        postUser(userInput: UserInputData): User!
        postPost(postInput: PostInputData): Post!
        putPost(postId: ID!, postInput: PostInputData): Post!
        deletePost(postId: ID!): Boolean!
        follow(userId: ID!): Boolean!
        unfollow(userId: ID!): Boolean!
        followUnfollow(userId: ID!): Boolean!
        like(whatToLike: String!, postCommentId: ID!): Boolean!
        unlike(whatToUnlike: String!, postCommentId: ID!): Boolean!
        likeUnlike(whatToLikeUnlike: String!, postCommentId: ID!): Boolean!
        postComment(content: String!, whatToComment: String!, postCommentId: ID!): Boolean!
        putComment(content: String!, commentId: ID!): Comment!
        deleteComment(commentId: ID!): Boolean!
        updateStatus(status: String!): String!
    }

    type RootQuery {
        getLogin(username: String!, password: String!): AuthData!
        getUser: GetUser!
        getOtherUserById(userId: ID!): User!
        getAllPosts: [Post!]!
        getPosts: [Post!]!
        getPost(postId: ID!): Post!
        getUsersByUsername(username: String!, perPage: Int!, page: Int): GetUsersByUsername!
        getComment(commentId: ID!): Comment!
        getCommentsForComment(commentId: ID!): [Comment!]
        getLikes: [Like!]
    }

    schema {
        query: RootQuery
        mutation: RootMutation
    }
`);