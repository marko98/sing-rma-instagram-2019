const bcrypt = require('bcryptjs');
const validator = require('validator');
const jwt = require('jsonwebtoken');

const { deleteImage } = require('../util/file');

const User = require('../models/user');
const Post = require('../models/post');
const Comment = require('../models/comment');
const Like = require('../models/like');

module.exports = {
    postUser({ userInput }, req) {
        console.log(userInput);
        

        const email = userInput.email;
        const username = userInput.username;
        const fullName = userInput.fullName;
        const password = userInput.password;

        const errors = [];
        if(!validator.isEmail(email)){
            errors.push({message: 'Invalid email.'});
        }
        if(validator.isEmpty(password) || !validator.isLength(password, {min:5})){
            errors.push({message: 'Invalid password.'});
        }
        if(validator.isEmpty(username)){
            errors.push({message: 'Invalid username.'});
        }
        if(validator.isEmpty(fullName)){
            errors.push({message: 'Invalid fullName.'});
        }
        if(errors.length > 0){
            const error = new Error('Invalid data.');
            error.data = errors;
            error.statusCode = 422;
            throw error;
        }

        return User.findOne({username: username})
            .then(userDoc => {
                if(userDoc){
                    const error = new Error('User exists already!');
                    throw error;
                }
                return User.findOne({email: email});
            })
            .then(userDoc => {
                if(userDoc){
                    const error = new Error('User exists already!');
                    throw error;
                }
                return bcrypt.hash(password, 12);
            })
            .then(hashedPassword => {
                const user = new User({
                    email: email,
                    password: hashedPassword,
                    username: username,
                    fullName: fullName
                });
                return user.save();
            })
            .then(user => {
                // user._doc -> mongoose ce nam dati samo podatke koje smo mi definisali u schemi, a ne i meta podatke                
                return { ...user._doc, _id: user._id.toString() };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
    },

    getUser(args, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        let theUser;
        return User.findById(req.userId)
            .populate('posts')
            // .populate('followers', 'username')
            // .populate('following', 'username')
            .populate('followers')
            .populate('following')
            .then(user => {
                if(!user){
                    const error = new Error('The user not found.');
                    error.statusCode = 404;
                    throw error;
                }
                theUser = user;
                return getComments(theUser._id.toString(), req);
            })
            .then(comments => {
                return {
                    user: {
                        ...theUser._doc,
                        _id: theUser._id.toString(),
                        posts: theUser.posts.map(post => {
                            return {
                                ...post._doc,
                                _id: post._id.toString(),
                                like: post.like.map(l => {
                                    return getLike(l._id.toString(), req);
                                }),
                                creator: theUser,
                                createdAt: post.createdAt.toISOString(),
                                updatedAt: post.updatedAt.toISOString()
                            }
                        }),
                        createdAt: theUser.createdAt.toISOString(),
                        updatedAt: theUser.updatedAt.toISOString()
                    },
                    comments: comments
                }
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
    },

    getOtherUserById({userId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        return User.findById(userId)
            .populate('posts')
            .populate('followers', 'username')
            .populate('following', 'username')
            .then(user => {
                if(!user){
                    const error = new Error('The user not found.');
                    error.statusCode = 404;
                    throw error;
                }
                return {
                    ...user._doc,
                    _id: user._id.toString(),
                    posts: user.posts.map(post => {
                        return {
                            ...post._doc,
                            _id: post._id.toString(),
                            createdAt: post.createdAt.toISOString(),
                            updatedAt: post.updatedAt.toISOString()
                        }
                    }),
                    createdAt: user.createdAt.toISOString(),
                    updatedAt: user.updatedAt.toISOString()
                }
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
    },

    getUsersByUsername({username, perPage, page}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        if(!page || page == 0){
            page = 1;
        }

        if(perPage == 0){
            perPage = 1
        }

        totalFoundUsers = 0
        totalUsers = 0
        return User.find().countDocuments()
            .then(total => {
                totalUsers = total;

                // {$regex: ".*"+username+".*"} -> moze da ima bilo sta pre, a i posle trazenog username-a
                return User.find({username: {$regex: "^"+username+".*"}})
                    /**
                     * sortiranje po opadajucem redosledu, od prvog kreiranog user-a pa na dalje
                     * 1 -> asc
                     * // -1 -> desc (od najskorije kreiranog user-a)
                     */
                    .sort({createdAt: 1})
                    .skip((page - 1) * perPage)
            })
            .then(users => {
                totalFoundUsers = users.length;

                return User.find({username: {$regex: "^"+username+".*"}})
                    .sort({createdAt: 1})
                    .skip((page - 1) * perPage)
                    .limit(perPage);
            })
            .then(users => {
                // console.log(users);
                return {
                    message: 'Fetched users successfully.', 
                    users: users.map(user => {
                        return {
                            ...user._doc,
                            _id: user._id.toString(), 
                            createdAt: user.createdAt.toISOString(),
                            updatedAt: user.updatedAt.toISOString()
                        };
                    }),
                    totalFoundUsers: totalFoundUsers,
                    totalUsers: totalUsers
                };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })

    },

    postPost({ postInput }, req) {
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        content = validator.trim(postInput.content);
        const imageUrl = validator.trim(postInput.imageUrl);

        const errors = [];
        if(validator.isEmpty(imageUrl) || !validator.isLength(imageUrl, {min: 5})){
            errors.push({message: 'Invalid imageUrl.'});
        }

        if(errors.length > 0){
            const error = new Error('Invalid data.');
            error.data = errors;
            error.statusCode = 422;
            throw error;
        }

        let creator;
        let createdPost;
        return User.findById(req.userId)
            .then(user => {
                if(!user){
                    const error = new Error('Invalid user.');
                    error.statusCode = 401;
                    throw error;
                }
                creator = user;
                const post = new Post({
                    content: content,
                    imageUrl: imageUrl,
                    creator: creator
                });
                return post.save();
            })
            .then(post => {
                createdPost = post;
                // console.log(post);
                creator.posts.push(post);
                return creator.save();
            })
            .then(user => {
                return {
                    ...createdPost._doc, 
                    _id: createdPost._id.toString(), 
                    createdAt: createdPost.createdAt.toISOString(),
                    updatedAt: createdPost.updatedAt.toISOString()
                };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
    },

    getPost({postId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        return Post.findById(postId)
            .populate('creator')
            .then(post => {
                if(!post){
                    const error = new Error('The post not found.');
                    error.statusCode = 404;
                    throw error;
                }
                return {
                    ...post._doc,
                    _id: post._id.toString(),
                    like: post.like.map(l => {
                        return getLike(l._id.toString(), req);
                    }),
                    createdAt: post.createdAt.toISOString(),
                    updatedAt: post.updatedAt.toISOString()
                }
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    },

    putPost({postId, postInput}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        return Post.findById(postId)
            .populate('creator')
            .then(post => {
                if(!post){
                    const error = new Error('No post found.');
                    error.statusCode = 404;
                    throw error;
                }
                if(req.userId.toString() !== post.creator._id.toString()){
                    const error = new Error('Not authorized.');
                    error.statusCode = 403;
                    throw error;
                }

                const imageUrl = validator.trim(postInput.imageUrl);
                const content = validator.trim(postInput.content);

                const errors = [];
                if(validator.isEmpty(imageUrl) || !validator.isLength(imageUrl, {min: 5})){
                    errors.push({message: 'Invalid imageUrl.'});
                }
                // if(validator.isEmpty(content) || !validator.isLength(content, {min: 5})){
                //     errors.push({message: 'Invalid content.'});
                // }

                if(errors.length > 0){
                    const error = new Error('Invalid data.');
                    error.data = errors;
                    error.statusCode = 422;
                    throw error;
                }

                if(postInput.imageUrl !== 'undefined'){
                    post.imageUrl = postInput.imageUrl;
                }
                
                post.content = content;
                return post.save();
            })
            .then(post => {
                return {
                    ...post._doc,
                    _id: post._id.toString(),
                    createdAt: post.createdAt.toISOString(),
                    updatedAt: post.updatedAt.toISOString()
                };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    },

    deletePost({postId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        return Post.findById(postId)
            .populate('creator')
            .then(post => {
                if(!post){
                    const error = new Error('No post found.');
                    error.statusCode = 404;
                    throw error;
                }
                if(req.userId.toString() !== post.creator._id.toString()){
                    const error = new Error('Not authorized.');
                    error.statusCode = 403;
                    throw error;
                }

                deleteImage(post.imageUrl);
                return Post.findByIdAndDelete(postId);
            })
            .then(result => {
                // console.log(result);
                return User.findById(req.userId);
            })
            .then(user => {
                user.posts.pull(postId);
                return user.save();
            })
            .then(user => {
                return true;
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    },

    getAllPosts: async function(args, req) {
        if(!req.isAuth){
            console.log("Not authenticated. getAllPosts")
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        try{
            const posts = await Post.find()
                    .populate('creator')
                    .populate('like')
                    .populate('comments')
                    /**
                     * sortiranje po opadajucem redosledu, od najskorije kreiranog posta
                     * -1 -> desc
                     */
                    .sort({createdAt: -1});

            // hocemo samo tudje postove
            let newPosts = [];
            for(post of posts){
                if(post.creator._id.toString() != req.userId.toString()){
                    newPosts.push(post);
                }
            }

            return newPosts.map(p => {                    
                    return {
                        ...p._doc,
                        _id: p._id.toString(), 
                        // comments: p.comments.map(c => {
                        //     // console.log(c._id.toString());
                        //     return getComment(c._id.toString(), req);
                        // }),
                        like: p.like.map(l => {
                                return getLike(l._id.toString(), req);
                            }),
                        createdAt: p.createdAt.toISOString(),
                        updatedAt: p.updatedAt.toISOString()
                    };
                });
        } catch(err) {
            console.log("No posts.")
            if(!err.statusCode){
                err.statusCode = 500;
            }
            throw err;
        }
    },

    getPosts: async function(args, req) {
        if(!req.isAuth){
            console.log("Not authenticated.")
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        
        try{
            const c = await User.findById(req.userId);
            const posts = await Post.find({creator: c})
                    .populate('creator')
                    .populate('like')
                    .populate('comments')
                    /**
                     * sortiranje po opadajucem redosledu, od najskorije kreiranog posta
                     * -1 -> desc
                     */
                    .sort({createdAt: -1});

            return posts.map(p => {                    
                    return {
                        ...p._doc,
                        _id: p._id.toString(), 
                        // comments: p.comments.map(c => {
                        //     // console.log(c._id.toString());
                        //     return getComment(c._id.toString(), req);
                        // }),
                        like: p.like.map(l => {
                            return getLike(l._id.toString(), req);
                        }),
                        createdAt: p.createdAt.toISOString(),
                        updatedAt: p.updatedAt.toISOString()
                    };
                });
        } catch(err) {
            console.log("No posts.")
            if(!err.statusCode){
                err.statusCode = 500;
            }
            throw err;
        }
    },

    getLogin({username, password}, req) {
        let loadedUser;     
        let token;   
        return User.findOne({username: username})
            .populate('followers')
            .populate('following')
            .populate('posts')
            .then(user => {
                if(!user){
                    const error = new Error('A user with this username could not be found.');
                    // 401 status code for not authenticated
                    error.statusCode = 401;
                    throw error;
                }
                loadedUser = user;
                return bcrypt.compare(password, user.password);
            })
            .then(isEqual => {
                if(!isEqual){
                    const error = new Error('Wrong password.');
                    // 401 status code for not authenticated
                    error.statusCode = 401;
                    throw error;
                }
                token = jwt.sign(
                        {
                            username: loadedUser.username, 
                            userId: loadedUser._id.toString()
                        }, 
                        'somesupersecretsecret', 
                        {expiresIn: '1h'}
                    );
                return getComments(loadedUser._id.toString(), req);
            })
            .then(comments => {
                return {
                    token: token, 
                    loggedUser: {
                        ...loadedUser._doc,
                        userId: loadedUser._id.toString(),
                        posts: loadedUser.posts.map(post => {
                            return {
                                ...post._doc,
                                _id: post._id.toString(),
                                like: post.like.map(l => {
                                    return getLike(l._id.toString(), req);
                                }),
                                creator: loadedUser,
                                createdAt: post.createdAt.toISOString(),
                                updatedAt: post.updatedAt.toISOString()
                            }
                        }),                        
                        createdAt: loadedUser.createdAt.toISOString(),
                        updatedAt: loadedUser.updatedAt.toISOString(),
                        comments: comments
                    }
                };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw(err);
            })
    },

    follow({userId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        userToFollow = undefined;
        return User.findById(userId)
            .then(user => {
                if(!user){
                    const error = new Error('The user not found.');
                    error.statusCode = 404;
                    throw error;
                }

                user.followers.addToSet(req.userId);
                return user.save();
            })
            .then(user => {
                userToFollow = user;
                return User.findById(req.userId);
            })
            .then(user => {
                user.following.addToSet(userToFollow._id);
                return user.save();
            })
            .then(user => {
                return true;
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    },

    unfollow({userId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        userToFollow = undefined;
        return User.findById(userId)
            .then(user => {
                if(!user){
                    const error = new Error('The user not found.');
                    error.statusCode = 404;
                    throw error;
                }

                user.followers.pull(req.userId);
                return user.save();
            })
            .then(user => {
                userToFollow = user;
                return User.findById(req.userId);
            })
            .then(user => {
                user.following.pull(userToFollow._id);
                return user.save();
            })
            .then(user => {
                return true;
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    },

    followUnfollow({userId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        userToFollow = undefined;
        toFollow = true;
        return User.findById(userId)
            .populate('followers')
            .then(user => {
                if(!user){
                    const error = new Error('The user not found.');
                    error.statusCode = 404;
                    throw error;
                }

                for(follower of user.followers){
                    if(follower._id.toString() == req.userId.toString()){
                        toFollow = false;
                    }
                }
                
                if(toFollow){
                    user.followers.addToSet(req.userId);
                } else {
                    user.followers.pull(req.userId);
                }
                return user.save();
            })
            .then(user => {
                userToFollow = user;
                return User.findById(req.userId);
            })
            .then(user => {
                if(toFollow){
                    user.following.addToSet(userToFollow._id);
                } else {
                    user.following.pull(userToFollow._id);
                }
                return user.save();
            })
            .then(user => {
                if(toFollow){
                    return true;
                } else {
                    return false;
                }                
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    },

    like({whatToLike, postCommentId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        
        if (whatToLike == "Post"){
            foundPost = undefined;
            return Post.findById(postCommentId)
                .populate('like')
                .then(post => {
                    if(!post){
                        const error = new Error('The post not found.');
                        error.statusCode = 404;
                        throw error;
                    }

                    // console.log(post.like);

                    for(like of post.like){
                        if(req.userId.toString() === like.creator.toString()){
                            const error = new Error('Already liked that post/comment.');
                            // ne znam statusni kod
                            error.statusCode = 403;
                            throw error;
                        }
                    }

                    foundPost = post;
                    like = new Like({
                        post: post,
                        creator: req.userId
                    });
                    return like.save();
                })
                .then(like => {
                    foundPost.like.addToSet(like);
                    return foundPost.save();
                })
                .then(post => {
                    return true;
                })
                .catch(err => {
                    if(!err.statusCode){
                        err.statusCode = 500;
                    }
                    throw err;
                });
        } else {
            foundComment = undefined;
            return Comment.findById(postCommentId)
                .populate('like')
                .then(comment => {
                    if(!comment){
                        const error = new Error('The comment not found.');
                        error.statusCode = 404;
                        throw error;
                    }

                    // console.log(comment.like);

                    for(like of comment.like){
                        if(req.userId.toString() === like.creator.toString()){
                            const error = new Error('Already liked that post/comment.');
                            // ne znam statusni kod
                            error.statusCode = 403;
                            throw error;
                        }
                    }

                    foundComment = comment;
                    like = new Like({
                        comment: comment,
                        creator: req.userId
                    });
                    return like.save();                    
                })
                .then(comment => {
                    foundComment.like.addToSet(comment);
                    return foundComment.save();
                })
                .then(like => {
                    return true;
                })
                .catch(err => {
                    if(!err.statusCode){
                        err.statusCode = 500;
                    }
                    throw err;
                });
        }
    },

    unlike({whatToUnlike, postCommentId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        if (whatToUnlike == "Post"){
            foundLike = undefined;
            return Post.findById(postCommentId)
                .populate('like')
                .then(post => {
                    if(!post){
                        const error = new Error('The post not found.');
                        error.statusCode = 404;
                        throw error;
                    }

                    for(like of post.like){
                        if(req.userId.toString() === like.creator.toString()){
                            foundLike = like;
                        }
                    }
    
                    if(foundLike == undefined){
                        const error = new Error('You didn\'t like the post, like not found.');
                        error.statusCode = 404;
                        throw error;
                    }
    
                    // console.log(post);
                    post.like.pull(foundLike);
                    // console.log(post);
                    return post.save();
                })
                .then(post => {
                    return Like.findByIdAndDelete(foundLike);
                })
                .then(like => {
                    // console.log(like);
                    return true;
                })
                .catch(err => {
                    if(!err.statusCode){
                        err.statusCode = 500;
                    }
                    throw err;
                });
        } else {
            foundLike = undefined;
            return Comment.findById(postCommentId)
            .populate('like')
            .then(comment => {
                if(!comment){
                    const error = new Error('The comment not found.');
                    error.statusCode = 404;
                    throw error;
                }

                for(like of comment.like){
                    if(req.userId.toString() === like.creator.toString()){
                        foundLike = like;
                    }
                }

                if(foundLike == undefined){
                    const error = new Error('You didn\'t like the comment, like not found.');
                    error.statusCode = 404;
                    throw error;
                }

                // console.log(comment);
                comment.like.pull(foundLike);
                // console.log(comment);
                return comment.save();
            })
            .then(comment => {
                return Like.findByIdAndDelete(foundLike);
            })
            .then(like => {
                // console.log(like);
                return true;
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
        }
    },

    likeUnlike({whatToLikeUnlike, postCommentId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        if (whatToLikeUnlike == "Post"){
            toLike = true;
            foundLike = undefined;
            return Post.findById(postCommentId)
                .populate('like')
                .then(post => {
                    if(!post){
                        const error = new Error('The post not found.');
                        error.statusCode = 404;
                        throw error;
                    }

                    for(like of post.like){
                        if(req.userId.toString() === like.creator.toString()){
                            foundLike = like;
                            toLike = false;
                        }
                    }
    
                    // if(foundLike == undefined){
                    //     const error = new Error('You didn\'t like the post, like not found.');
                    //     error.statusCode = 404;
                    //     throw error;
                    // }

                    if(toLike){
                        like = new Like({
                            post: post,
                            creator: req.userId
                        });
                        return like.save()
                            .then(like => {
                                post.like.addToSet(like);
                                return post.save();
                            })
                            .catch(err => {
                                if(!err.statusCode){
                                    err.statusCode = 500;
                                }
                                throw err;
                            });
                    } else {
                        return Like.findByIdAndDelete(foundLike)
                            .then(like => {
                                post.like.pull(foundLike); 
                                return post.save();
                            })
                            .catch(err => {
                                if(!err.statusCode){
                                    err.statusCode = 500;
                                }
                                throw err;
                            });
                    }
                })
                .then(post => {
                    if(toLike){
                        return true;
                    } else {
                        return false;;
                    }
                })
                .catch(err => {
                    if(!err.statusCode){
                        err.statusCode = 500;
                    }
                    throw err;
                });
        } else {
            toLike = true;
            foundLike = undefined;
            return Comment.findById(postCommentId)
                .populate('like')
                .then(comment => {
                    if(!comment){
                        const error = new Error('The comment not found.');
                        error.statusCode = 404;
                        throw error;
                    }

                    for(like of comment.like){
                        if(req.userId.toString() === like.creator.toString()){
                            foundLike = like;
                            toLike = false;
                        }
                    }

                    // if(foundLike == undefined){
                    //     const error = new Error('You didn\'t like the comment, like not found.');
                    //     error.statusCode = 404;
                    //     throw error;
                    // }

                    if(toLike){
                        like = new Like({
                            comment: comment,
                            creator: req.userId
                        });
                        return like.save()
                            .then(like => {
                                comment.like.addToSet(like);
                                return comment.save();
                            })
                            .catch(err => {
                                if(!err.statusCode){
                                    err.statusCode = 500;
                                }
                                throw err;
                            });
                    } else {
                        return Like.findByIdAndDelete(foundLike)
                            .then(like => {
                                comment.like.pull(foundLike);
                                return comment.save();
                            })
                            .catch(err => {
                                if(!err.statusCode){
                                    err.statusCode = 500;
                                }
                                throw err;
                            });
                    }
                })
                .then(comment => {
                    if(toLike){
                        return true;
                    } else {
                        return false;
                    }
                })
                .catch(err => {
                    if(!err.statusCode){
                        err.statusCode = 500;
                    }
                    throw err;
                });
        }
    },

    getLikes(args, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        return Like.find()
            .populate('post')
            .populate('comment')
            .populate('creator')
            .sort({createdAt: -1})
            .then(likes => {
                usersLikes = [];
                for(like of likes){
                    if(like.post != undefined){
                        if(req.userId.toString() == like.post.creator.toString()){
                            // console.log('post');
                            usersLikes.push(like);
                        }
                    } else {
                        if(req.userId.toString() == like.comment.creator.toString()){
                            // console.log('comment');
                            usersLikes.push(like);
                        }
                    }
                }
                // console.log(usersLikes);
                return usersLikes.map(like => {
                    if(like.post != undefined){
                        return {
                            ...like._doc,
                            post: getPost(like.post._id.toString(), req),
                            _id: like._id.toString(),
                            createdAt: like.createdAt.toISOString(),
                            updatedAt: like.updatedAt.toISOString()
                        };                 
                    }
                    return {
                            ...like._doc,
                            _id: like._id.toString(),
                            createdAt: like.createdAt.toISOString(),
                            updatedAt: like.updatedAt.toISOString()
                        };                 
                    });
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
    },

    postComment({content, whatToComment, postCommentId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        content = validator.trim(content);

        const errors = [];
        if(validator.isEmpty(content) || !validator.isLength(content, {min: 1})){
            errors.push({message: 'Invalid content.'});
        }

        if(errors.length > 0){
            const error = new Error('Invalid data.');
            error.data = errors;
            error.statusCode = 422;
            throw error;
        }

        comment = new Comment({
            content: content,
            creator: req.userId
        });

        if (whatToComment == "Post"){
            comment.post = postCommentId;
        } else {
            comment.comment = postCommentId;
        }
        
        return comment.save()
            .then(theComment => {
                if (whatToComment == "Post"){
                    return Post.findById(postCommentId)
                        .populate('comments')
                        .then(post => {
                            if(!post){
                                const error = new Error('The post not found.');
                                error.statusCode = 404;
                                throw error;
                            }

                            post.comments.addToSet(theComment);
                            return post.save();
                        })
                        .then(post => {
                            return true;
                        })
                        .catch(err => {
                            if(!err.statusCode){
                                err.statusCode = 500;
                            }
                            throw err;
                        });
                } else {
                    return Comment.findById(postCommentId)
                        .populate('comments')
                        .then(comment => {
                            if(!comment){
                                const error = new Error('The comment not found.');
                                error.statusCode = 404;
                                throw error;
                            }

                            comment.comments.addToSet(theComment);
                            return comment.save();
                        })
                        .then(comment => {
                            return true;
                        })
                        .catch(err => {
                            if(!err.statusCode){
                                err.statusCode = 500;
                            }
                            throw err;
                        });
                }
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
    },

    getComment({commentId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        return Comment.findById(commentId)
            .populate('creator')
            .populate('comments')
            .then(comment => {
                if(!comment){
                    const error = new Error('The comment not found.');
                    error.statusCode = 404;
                    throw error;
                }
                // if(req.userId.toString() !== comment.creator._id.toString()){
                //     const error = new Error('Not authorized.');
                //     error.statusCode = 403;
                //     throw error;
                // }             

                return {
                    ...comment._doc,
                    _id: comment._id.toString(), 
                    comments: comment.comments.map(comment => {
                        return {
                            _id: comment._id.toString(), 
                            createdAt: comment.createdAt.toISOString(),
                            updatedAt: comment.updatedAt.toISOString()
                        }
                    }),
                    like: comment.like.map(like => {
                        return getLike(like, req)
                    }),
                    createdAt: comment.createdAt.toISOString(),
                    updatedAt: comment.updatedAt.toISOString()
                };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    },

    getCommentsForComment({commentId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        return Comment.findById(commentId)
            .populate('comments')
            /**
             * sortiranje po opadajucem redosledu, od najskorije kreiranog komentara
             * -1 -> desc
             */
            .sort({createdAt: -1})
            .then(comment => {
                if(!comment){
                    const error = new Error('The comment not found.');
                    error.statusCode = 404;
                    throw error;
                }                

                return comment.comments.map(comment => {
                    return Comment.findById(comment._id)
                        .populate('creator')
                        .then(comment => {
                            return {
                                ...comment._doc,
                                _id: comment._id.toString(), 
                                createdAt: comment.createdAt.toISOString(),
                                updatedAt: comment.updatedAt.toISOString()
                            };
                        })
                        .catch(err => {
                            if(!err.statusCode){
                                err.statusCode = 500;
                            }
                            throw err;
                        });                        
                    });
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    },

    putComment({content, commentId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        return Comment.findById(commentId)
            .populate('creator')
            .then(comment => {
                if(!comment){
                    const error = new Error('No comment found.');
                    error.statusCode = 404;
                    throw error;
                }
                if(req.userId.toString() !== comment.creator._id.toString()){
                    const error = new Error('Not authorized.');
                    error.statusCode = 403;
                    throw error;
                }

                content = validator.trim(content);

                const errors = [];
                if(validator.isEmpty(content) || !validator.isLength(content, {min: 1})){
                    errors.push({message: 'Invalid content.'});
                }

                if(errors.length > 0){
                    const error = new Error('Invalid data.');
                    error.data = errors;
                    error.statusCode = 422;
                    throw error;
                }
                
                comment.content = content;
                return comment.save();
            })
            .then(comment => {
                return {
                    ...comment._doc,
                    _id: comment._id.toString(),
                    createdAt: comment.createdAt.toISOString(),
                    updatedAt: comment.updatedAt.toISOString()
                };
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    },

    deleteComment({commentId}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }

        commentedCommentId = undefined;
        commentedPostId = undefined;
        return Comment.findById(commentId)
            .populate('creator')
            .populate('comments')
            .then(comment => {
                if(!comment){
                    const error = new Error('No comment found.');
                    error.statusCode = 404;
                    throw error;
                }
                if(req.userId.toString() !== comment.creator._id.toString()){
                    const error = new Error('Not authorized.');
                    error.statusCode = 403;
                    throw error;
                }

                commentedCommentId = comment.comment;
                commentedPostId = comment.post;
                return this.deleteCommentChildren(comment);
            })
            .then(result => {
                if(commentedPostId != undefined){
                    return Post.findById(commentedPostId);
                } else {
                    return Comment.findById(commentedCommentId);
                }
            })
            .then(postOrComment => {
                if(commentedPostId != undefined){
                    postOrComment.comments.pull(commentId);
                } else {
                    postOrComment.comments.pull(commentId);
                }
                return postOrComment.save();
            })
            .then(postOrComment => {
                return true;
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            })
    },

    deleteCommentChildren(comment){
        // depth first search
        neposeceni = [comment];
        poseceni = [];

        while(neposeceni.length > 0){
            naObradi = neposeceni.pop();

            // console.log(commentedCommentId, commentedPostId, naObradi._id);
            // console.log(naObradi._id);
            Comment.findById(naObradi._id)
                .then(comment => {
                    // console.log(comment._id);
                    if(!comment){
                        console.log('No comment found.');
                        const error = new Error('No comment found.');
                        error.statusCode = 404;
                        throw error;
                    }

                    return Comment.findByIdAndDelete(comment._id);
                })
                .then(result => {
                    // console.log(result);              
                    return true;
                })
                .catch(err => {
                    console.log("greske kod brisanja dece komentara");
                    console.log(err); 
                    return false;
                });

            if (!poseceni.includes(naObradi)){
                poseceni.push(naObradi);
                children = naObradi.comments;

                for(child of children){
                    if(!poseceni.includes(child)){
                        neposeceni.push(child);
                    }
                }
            }

        }
    },

    updateStatus({status}, req){
        if(!req.isAuth){
            const error = new Error('Not authenticated.');
            error.statusCode = 401;
            throw error;
        }
        return User.findById(req.userId)
            .then(user => {
                if(!user){
                    const error = new Error('The user not found.');
                    error.statusCode = 404;
                    throw error;
                }

                user.status = status;
                return user.save();
            })
            .then(user => {
                return status;
            })
            .catch(err => {
                if(!err.statusCode){
                    err.statusCode = 500;
                }
                throw err;
            });
    },
}

getComment = (commentId, req) => {
    // if(!req.isAuth){
    //     const error = new Error('Not authenticated.');
    //     error.statusCode = 401;
    //     throw error;
    // }

    return Comment.findById(commentId)
        .populate('creator')
        .then(comment => {
            if(!comment){
                const error = new Error('The comment not found.');
                error.statusCode = 404;
                throw error;
            }
            // if(req.userId.toString() !== comment.creator._id.toString()){
            //     const error = new Error('Not authorized.');
            //     error.statusCode = 403;
            //     throw error;
            // }             

            return {
                ...comment._doc,
                _id: comment._id.toString(), 
                comments: comment.comments.map(comment => {
                    return getComment(comment._id.toString(), req);
                }),
                createdAt: comment.createdAt.toISOString(),
                updatedAt: comment.updatedAt.toISOString()
            };
        })
        .catch(err => {
            if(!err.statusCode){
                err.statusCode = 500;
            }
            throw err;
        })
};

getComments = (userId, req) => {
    return Comment.find()
        .populate('creator')
        /**
         * sortiranje po opadajucem redosledu, od najskorije kreiranog komentara
         * -1 -> desc
         */
        .sort({createdAt: -1})
        .then(comments => { 

            let newComments = [];
            
            for(comment of comments){
                if(comment.creator._id.toString() == userId){
                    newComments.push(comment);
                }
            }

            return newComments.map(comment => {                
                    return {
                        ...comment._doc,
                        _id: comment._id.toString(), 
                        // comments: comment.comments.map(c => {
                        //     // console.log(c._id.toString());
                        //     return getComment(c._id.toString(), req);
                        // }),
                        like: comment.like.map(l => {
                                return getLike(l._id.toString(), req);
                            }),
                        createdAt: comment.createdAt.toISOString(),
                        updatedAt: comment.updatedAt.toISOString()
                    };
                });
        })
        .catch(err => {
            if(!err.statusCode){
                err.statusCode = 500;
            }
            throw err;
        })
};

getUserGetComments = (req) => {
    return Comment.find()
        .populate('creator')
        /**
         * sortiranje po opadajucem redosledu, od najskorije kreiranog komentara
         * -1 -> desc
         */
        .sort({createdAt: -1})
        .then(comments => { 

            let newComments = [];
            
            for(comment of comments){
                if(comment.creator._id.toString() == req.userId){
                    newComments.push(comment);
                }
            }

            return newComments.map(comment => {                    
                    return {
                        ...comment._doc,
                        _id: comment._id.toString(), 
                        // comments: comment.comments.map(c => {
                        //     // console.log(c._id.toString());
                        //     return getComment(c._id.toString(), req);
                        // }),
                        like: comment.like.map(l => {
                                return getLike(l._id.toString(), req);
                            }),
                        createdAt: comment.createdAt.toISOString(),
                        updatedAt: comment.updatedAt.toISOString()
                    };
                });
        })
        .catch(err => {
            if(!err.statusCode){
                err.statusCode = 500;
            }
            throw err;
        })
};

getLike = (likeId, req) => {
    // if(!req.isAuth){
    //     const error = new Error('Not authenticated.');
    //     error.statusCode = 401;
    //     throw error;
    // }

    return Like.findById(likeId)
        .populate('creator')
        .populate('post')
        .populate('comment')
        .then(like => {
            if(!like){
                const error = new Error('The like not found.');
                error.statusCode = 404;
                throw error;
            }        

            return {
                ...like._doc,
                _id: like._id.toString(),
                createdAt: like.createdAt.toISOString(),
                updatedAt: like.updatedAt.toISOString()
            };
        })
        .catch(err => {
            if(!err.statusCode){
                err.statusCode = 500;
            }
            throw err;
        })
};

getPost = (postId, req) => {
    // if(!req.isAuth){
    //     const error = new Error('Not authenticated.');
    //     error.statusCode = 401;
    //     throw error;
    // }
    return Post.findById(postId)
        .populate('creator')
        .then(post => {
            if(!post){
                const error = new Error('The post not found.');
                error.statusCode = 404;
                throw error;
            }
            return {
                ...post._doc,
                _id: post._id.toString(),
                like: post.like.map(l => {
                    return getLike(l._id.toString(), req);
                }),
                createdAt: post.createdAt.toISOString(),
                updatedAt: post.updatedAt.toISOString()
            }
        })
        .catch(err => {
            if(!err.statusCode){
                err.statusCode = 500;
            }
            throw err;
        })
};