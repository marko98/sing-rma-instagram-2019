package com.example.instagram.controllers.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.controllers.interfaces.FragmentInteraction;
import com.example.instagram.R;
import com.example.instagram.controllers.State;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;

public class SignupFragment extends InstagramFragment{
    private static final String DATA = "SIGNUP";
    public static SignupFragment instance;

    private String data;
    private View mainView;
    private LayoutInflater layoutInflater;

    private FragmentInteraction listener;

    public SignupFragment() {
        super(State.STATE.SIGNUP);
        System.out.println("SignupFragment");
        Bundle args = new Bundle();
        args.putString(DATA, "Signup");
        this.setArguments(args);
    }

    public static synchronized SignupFragment getInstance() {
        if (SignupFragment.instance == null) {
            SignupFragment.instance = new SignupFragment();
        }
        return SignupFragment.instance;
    }

    @Override
    public void onAttach(Context context) {
        System.out.println("onAttach");
        super.onAttach(context);
        //ova metoda se poziva kada se nakaci na context, odnosno na activity i desi se sledece
        //proveravamo da li activity implementira nas interfejs
        if (context instanceof FragmentInteraction) {
            //ako implementira, dobijamo referencu na activity
            this.listener = (FragmentInteraction) context;
        } else {
            //u suportnom bacamo gresku, jer nije moguce komunicirati
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        System.out.println("onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            this.data = getArguments().getString(DATA);
            System.out.println("Data vrednost: " + this.data);
            this.data = "Signup";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("onCreateView");
        // Inflate the layout for this fragment
        this.layoutInflater = inflater;
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        this.mainView = view;
        drawData();
        return view;
    }

    private void drawData(){
        System.out.println("drawData");

        ((TextView) this.mainView.findViewById(R.id.labelLogin)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                State.setCurrentState(State.STATE.LOGIN);
            }
        });

        ((Button) this.mainView.findViewById(R.id.buttonSignup)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final EditText inputEmail = (EditText) mainView.findViewById(R.id.inputEmail);
                final EditText inputFullName = (EditText) mainView.findViewById(R.id.inputFullName);
                final EditText inputUsername = (EditText) mainView.findViewById(R.id.inputUsername);
                final EditText inputPassword = (EditText) mainView.findViewById(R.id.inputPassword);

                String email = inputEmail.getText().toString();
                String fullName = inputFullName.getText().toString();
                String username = inputUsername.getText().toString();
                String password = inputPassword.getText().toString();

                String query = "mutation PostUser($email: String, $username: String, $fullName: String, $password: String){ postUser(userInput: {email: $email, username: $username, fullName: $fullName, password: $password}){status}}";
                String variables = "{\"email\": \"" + email + "\", \"username\": \"" + username + "\", \"fullName\": \"" + fullName + "\", \"password\": \"" + password + "\"}";

                HttpRequests.postRequest(query, variables);
            }
        });
    }

    public void manipulisiFragmentom() {
        System.out.println("Manipulacija nad SignupFragment-om");
    }

    @Override
    public void onDetach() {
        System.out.println("onDetach");
        super.onDetach();
        this.listener = null;
    }

}
