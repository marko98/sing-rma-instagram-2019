package com.example.instagram.controllers.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.controllers.ProcessBitmap;
import com.example.instagram.controllers.MyViewModel;
import com.example.instagram.controllers.interfaces.AsyncTaskInteraction;
import com.example.instagram.controllers.interfaces.FragmentInteraction;
import com.example.instagram.R;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.recyclerView.RecyclerViewItemListener;
import com.example.instagram.controllers.recyclerView.ProfilePostRecyclerViewAdapter;
import com.example.instagram.models.Comment;
import com.example.instagram.models.Like;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class ProfileFragment extends InstagramFragment implements AsyncTaskInteraction, RecyclerViewItemListener {
    private static final String DATA = "PROFILE";
    public static ProfileFragment instance;

    private String data;
    private View mainView;
    private RecyclerView recyclerView;
    private Button buttonChangeData;
    private MyViewModel mvm;
    private LayoutInflater inflater;

    private FragmentInteraction listener;

    public ProfileFragment(String text) {
        super(State.STATE.PROFILE);
        System.out.println("ProfileFragment");
        Bundle args = new Bundle();
        args.putString(DATA, text);
        this.setArguments(args);
        this.mvm = new MyViewModel();
    }

    public static synchronized ProfileFragment getInstance(String text) {
        if (ProfileFragment.instance == null) {
            ProfileFragment.instance = new ProfileFragment(text);
        }
        return ProfileFragment.instance;
    }

    @Override
    public void onAttach(Context context) {
        System.out.println("onAttach");
        super.onAttach(context);
        //ova metoda se poziva kada se nakaci na context, odnosno na activity i desi se sledece
        //proveravamo da li activity implementira nas interfejs
        if (context instanceof FragmentInteraction) {
            //ako implementira, dobijamo referencu na activity
            this.listener = (FragmentInteraction) context;
            this.inflater = getLayoutInflater().from(context);

//            if(State.getLoggedUser() == null && State.getToken() != null){
//                String query = "query GetUser{getUser{ _id, email, username, following { username }, followers { username }, posts {_id,content,like {_id},comments {_id},imageUrl,createdAt,updatedAt},status, createdAt, updatedAt}}";
//                String variables = "{}";
//                HttpRequests.getRequest(query, variables);
//            }
        } else {
            //u suportnom bacamo gresku, jer nije moguce komunicirati
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        System.out.println("onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            this.data = getArguments().getString(DATA);
            System.out.println("Data vrednost: " + this.data);
//            System.out.println("Data vrednost: " + this.data);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("onCreateView");
        View mView = inflater.inflate(R.layout.fragment_profile, container, false);
        this.mainView = mView;
        this.recyclerView = mView.findViewById(R.id.recyclerView);

        // setujemo sta zelimo da osluskujemo, inace postavljamo ga ovde jer su tu i view-ovi kreirani
        this.setWhatToObserve();

//        if(!(State.getLoggedUser() == null && State.getToken() != null)){
//            ArrayList<Post> posts = State.getLoggedUser().getPosts();
//            if(posts != null){
//                drawData(posts);
//            }
//        }

        return mView;
    }

    private void setWhatToObserve(){
//        OnClick Listeners
        final EditText editTextStatus = this.mainView.findViewById(R.id.editTextStatus);
        this.mainView.findViewById(R.id.buttonEditProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HttpRequests.postRequest("mutation UpdateStatus($status: String!){ updateStatus(status: $status)}", "{\"status\":\"" + editTextStatus.getText().toString() + "\"}");
            }
        });

        this.mvm.getCurrentStateOfState().observe((LifecycleOwner) this.listener, new Observer<State.STATE>(){
            @Override
            public void onChanged(State.STATE state) {
//                if (State.getCurrentState().equals(State.STATE.PROFILE)){
//                    if (state.equals(State.STATE.PROFILE)) {
//                        if (State.getLoggedUser().getPosts() == null) {
//                            if(!State.receivedGetAllPosts){
//                                String query = "query GetAllPosts{ getAllPosts{ _id, content, like { _id, creator{ _id, username } },comments { _id, content, createdAt },imageUrl, creator { _id, username },createdAt, updatedAt } }";
//                                HttpRequests.getRequest(query, "");
//                            } else {
//                                mainView.findViewById(R.id.progressBarSpinnerHead).setVisibility(View.GONE);
//                                mainView.findViewById(R.id.constraintLayoutHead).setVisibility(View.VISIBLE);
//                            }
//                        } else {
//                            drawData(State.getLoggedUser().getPosts());
//                        }
//                    }
//                }
            }
        });

        this.mvm.getLoggedUserOfState().observe((LifecycleOwner) this.listener, new Observer<User>(){
            @Override
            public void onChanged(User user) {
//                nije bitno u kojem stanju se nalazim, ako postoji ovaj fragment treba da se azurira
                if (user != null && user.getPosts() != null && user.getFollowers() != null && user.getFollowings() != null) {
                    drawData();
                } else {
                    mainView.findViewById(R.id.progressBarSpinnerHead).setVisibility(View.VISIBLE);
                    mainView.findViewById(R.id.constraintLayoutHead).setVisibility(View.GONE);
                }
            }
        });

        this.mvm.getPostsOfState().observe((LifecycleOwner) this.listener, new Observer<ArrayList<Post>>(){
            @Override
            public void onChanged(ArrayList<Post> posts) {
//                if (State.getCurrentState().equals(State.STATE.PROFILE)){
//                    if (posts != null && !posts.isEmpty()) {
//                        drawData(State.getLoggedUser().getPosts());
//                    } else {
//                        if(!State.receivedGetAllPosts){
//                            String query = "query GetAllPosts{ getAllPosts{ _id, content, like { _id, creator{ _id, username } },comments { _id, content, createdAt },imageUrl, creator { _id, username },createdAt, updatedAt } }";
//                            HttpRequests.getRequest(query, "");
//                        }
//                    }
//                }
            }
        });
    }

    private void drawData(){
        System.out.println("drawData");

        User user = State.getLoggedUser();
        ArrayList<Post> posts = new ArrayList<>();
        if(user != null) {
            posts = user.getPosts();
        }

        if (mainView != null) {
            mainView.findViewById(R.id.progressBarSpinnerHead).setVisibility(View.GONE);
            mainView.findViewById(R.id.constraintLayoutHead).setVisibility(View.VISIBLE);

            ((TextView)mainView.findViewById(R.id.textViewUsername)).setText(State.getLoggedUser().getUsername());
            ((TextView)mainView.findViewById(R.id.textViewUsername2)).setText(State.getLoggedUser().getUsername());
            String status = State.getLoggedUser().getStatus();
            ((EditText)mainView.findViewById(R.id.editTextStatus)).setText(status);

            ((TextView)mainView.findViewById(R.id.textViewNumOfPosts)).setText(String.valueOf(State.getLoggedUser().getPosts().size()) + "\n Posts");
            ((TextView)mainView.findViewById(R.id.textViewNumOfFollowers)).setText(String.valueOf(State.getLoggedUser().getFollowers().size()) + "\n Followers");
            ((TextView)mainView.findViewById(R.id.textViewNumOfFollowing)).setText(String.valueOf(State.getLoggedUser().getFollowings().size()) + "\n Following");

            ((ImageView)mainView.findViewById(R.id.imageViewProfileImage)).setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource((Context) listener, R.drawable.user_icon));
        }

        if(posts != null){
            ArrayList<Post> postsToGetBitmap = new ArrayList<>();
            for(Post post : posts){
                if(post.getBitmap() == null){
                    postsToGetBitmap.add(post);
                }
            }
            if(!postsToGetBitmap.isEmpty()){
//                nisu spremni za prikaz
//                salji zahtev za bitmape
                HttpRequests.getPostsBitmaps(ProfileFragment.instance, postsToGetBitmap);
                this.mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.VISIBLE);
                this.recyclerView.setVisibility(View.GONE);
            } else {
//                spremni su za prikaz, ovo se poziva cak i kada ulogovani korisnik nema svojih postova
//                for(Post post : posts){
//                    post.setCreator(State.getLoggedUser());
//                }

                ProfilePostRecyclerViewAdapter postRecyclerViewAdapter = new ProfilePostRecyclerViewAdapter((Context) listener, posts, (RecyclerViewItemListener) this);
                this.recyclerView.setAdapter(postRecyclerViewAdapter);
                GridLayoutManager gridLayoutManager = new GridLayoutManager((Context) listener, 3, GridLayoutManager.VERTICAL, false);
                this.recyclerView.setLayoutManager(gridLayoutManager);

                this.mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
                this.recyclerView.setVisibility(View.VISIBLE);
            }
        }
    }

    public void manipulisiFragmentom() {
        System.out.println("Manipulacija nad ProfileFragment-om");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        System.out.println("onViewCreated");
    }

    @Override
    public void onDetach() {
        System.out.println("onDetach");
        super.onDetach();
        this.listener = null;
    }

//    ----------------------------- INTERFACE METHODS IMPLEMENTATION -------------------------------

    @Override
    public void onPostGotBitmap() {
//        if(State.getLoggedUser() != null && State.getLoggedUser().getPosts() != null)
//            drawData(State.getLoggedUser().getPosts());
//        ((ProgressBar)this.mainView.findViewById(R.id.progressBarSpinner)).setVisibility(View.GONE);
//        this.recyclerView.setVisibility(View.VISIBLE);
        drawData();
    }

    @Override
    public void onHttpRequestReceived(HttpRequests.REQUEST_NAME requestName) {

    }

    @Override
    public void onHttpPostRequestRecieved(HashMap<String, Object> hashMap, HttpRequests.REQUEST_NAME requestName) {

    }

    @Override
    public void onPostClick(Post post) {
        Toast.makeText((Context) listener, post.getId() + " is clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLikeClick(Like like) {

    }

    @Override
    public void onCommentClick(Comment comment) {

    }
}


//    private void setWhatToObserve2(){
//        this.mvm.getCurrentStateOfState().observe((LifecycleOwner) this.listener, new Observer<State.STATE>(){
//            @Override
//            public void onChanged(State.STATE state) {
//                if (state.equals(State.STATE.PROFILE)) {
//                    if(State.getLoggedUser().getPosts() == null){
//                        String query = "query GetUser{getUser{ _id, email, username, following { username }, followers { username }, posts {_id,content,like {_id},comments {_id},imageUrl,createdAt,updatedAt},status, createdAt, updatedAt}}";
//                        String variables = "{}";
//                        HttpRequests.getRequest(query, variables);
//                    } else {
//                        drawData(State.getLoggedUser().getPosts());
//                    }
//                }
//            }
//        });
//
//        this.mvm.getLoggedUserOfState().observe((LifecycleOwner) this.listener, new Observer<User>(){
//            @Override
//            public void onChanged(User user) {
//            if (user != null && user.getPosts() != null && user.getFollowers() != null && user.getFollowings() != null) {
//                ArrayList<Post> posts = user.getPosts();
//                if(!posts.isEmpty()){
//                    drawData(posts);
//                }
//            } else {
//                mainView.findViewById(R.id.progressBarSpinnerHead).setVisibility(View.VISIBLE);
//                mainView.findViewById(R.id.constraintLayoutHead).setVisibility(View.GONE);
//            }
//            }
//        });
//    }
