package com.example.instagram.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.instagram.R;
import com.example.instagram.controllers.fragments.HomeFragment;
import com.example.instagram.controllers.fragments.LikeFragment;
import com.example.instagram.controllers.fragments.LoginFragment;
import com.example.instagram.controllers.fragments.OtherProfileFragment;
import com.example.instagram.controllers.fragments.PostFragment;
import com.example.instagram.controllers.fragments.ProfileFragment;
import com.example.instagram.controllers.fragments.SearchFragment;
import com.example.instagram.controllers.fragments.SignupFragment;
import com.example.instagram.models.Comment;
import com.example.instagram.models.Like;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class State{
    public enum STATE {
        LOGIN,
        SIGNUP,
        HOME,
        SEARCH,
        LIKE,
        PROFILE,
        CAMERAGALARY,
        POSTAPOST,
        POST,
        OTHERS_PROFILE
    }
    private static Context mainActivityContext = null;
    private static STATE currentState = STATE.LOGIN;
    private static ArrayList<STATE> previousStates = new ArrayList<>();
    private static String TOKEN = null;
    private static User loggedUser = null;
    private static User otherUser = null;
    private static Post postToBeShown = null;
    private static ArrayList<Like> likes = new ArrayList<Like>();
    private static ArrayList<Post> posts = new ArrayList<Post>();

    public static String IPv4Address = "192.168.91.52";
    public static String port = "8080";
    public static int requestsForUpdatingPostsToRecive = 0;

    public static boolean receivedGetAllPosts = false;
    public static boolean receivedGetLogin = false;


    public State() {
        State.posts = new ArrayList<Post>();
    }

    public static STATE getCurrentState() {
        return currentState;
    }

    public static void setCurrentState(STATE currentState) {
        System.out.println("previous state changed(" + State.currentState + "), current state: " + currentState);
        if (currentState != State.currentState) {
            State.previousStates.add(State.currentState);
            State.currentState = currentState;
            MyViewModel.fetchStateOfState();
        }
    }

    public static STATE getPreviousState() {
        STATE state = previousStates.remove(previousStates.size() - 1);
        while (state == STATE.POSTAPOST || state == STATE.CAMERAGALARY) {
            state = previousStates.remove(previousStates.size() - 1);
        }
        if (state != null && state != State.currentState){
            State.currentState = state;
            MyViewModel.fetchStateOfState();
        }
        return state;
    }

    public static ArrayList<Post> getPosts() {
        return posts;
    }

    public static void setPosts(ArrayList<Post> posts) {
//        State.addPostToLoggedUser(posts);

        State.posts = posts;
        if(State.requestsForUpdatingPostsToRecive == 0){
            MyViewModel.fetchPostsOfState();
        }
    }

    public static void addPostToPosts(Post post){
//        State.addPostToLoggedUser(post);

        post.setCreator(State.getLoggedUser());
        ArrayList<Post> posts = new ArrayList<>();
        posts.add(post);
        for(Post p : State.posts){
          posts.add(p);
        }
        State.posts = posts;
        if(State.requestsForUpdatingPostsToRecive == 0){
            MyViewModel.fetchPostsOfState();
        }
    }

    public static void addPostsToPosts(ArrayList<Post> posts){
//        State.addPostToLoggedUser(posts);

        if(State.getPosts() == null){
            State.setPosts(new ArrayList<Post>());
        }

        for(Post postToAdd : posts){
            boolean exists = false;
//            for(Post postInPosts : State.getPosts()){
//                if(postToAdd.getId().equals(postInPosts.getId())){
//                    postInPosts = postToAdd;
//                    exists = true;
//                }
//            }
            for(int i = 0; i < State.getPosts().size(); i++){
                if(postToAdd.getId().equals(State.getPosts().get(i).getId())){
                    if(State.getPostToBeShown() != null && State.getPostToBeShown().getId().equals(postToAdd.getId())){
                        State.setPostToBeShown(postToAdd);
                    }
                    State.getPosts().set(i, postToAdd);
                    exists = true;
                }
            }
            if(!exists){
                State.posts.add(postToAdd);
            }
        }

        if(State.requestsForUpdatingPostsToRecive == 0){
            MyViewModel.fetchPostsOfState();
        }
    }

    public static void updatePost(Post post){
        for(int i = 0; i < State.posts.size(); i++){
            if(State.posts.get(i).getId().equals(post.getId())){
                ArrayList<Post> posts = new ArrayList<>();
                for(int j = 0; j < State.posts.size(); j++){
                    if(i == j){
                        posts.add(post);
                    } else {
                        posts.add(State.posts.get(j));
                    }
                }
                if(State.requestsForUpdatingPostsToRecive == 0){
                    MyViewModel.fetchPostsOfState();
                }
                return;
            }
        }
    }

    public static String getToken() {
        return State.TOKEN;
    }

    public static void setToken(String token) {
        if(token == null){
            State.TOKEN = token;
            MyViewModel.fetchTokenOfState();
            State.setPosts(new ArrayList<Post>());
            State.setCurrentState(State.STATE.LOGIN);
        } else {
            System.out.println("Previous token: " + State.TOKEN + "), current token: " + token);
            State.TOKEN = token;
            MyViewModel.fetchTokenOfState();
        }
    }

    public static User getLoggedUser() {
        return loggedUser;
    }

    public static void setLoggedUser(User loggedUser) {
        State.loggedUser = loggedUser;
        MyViewModel.fetchLoggedUserOfState();
    }

    public static User getOtherUser() {
        return otherUser;
    }

    public static void setOtherUser(User otherUser) {
        State.otherUser = otherUser;
        MyViewModel.fetchOtherUserOfState();
    }

    public static ArrayList<Like> getLikes() {
        return State.likes;
    }

    public static void setLikes(ArrayList<Like> likes) {
        State.likes = likes;
        MyViewModel.fetchLikesOfState();
    }

    public static void addLikesToLikes(Like like){
        ArrayList<Like> likes = new ArrayList<>();
        likes.add(like);
        for(Like l : State.likes){
            likes.add(l);
        }
        State.likes = likes;
        MyViewModel.fetchLikesOfState();
    }

//    private static void addPostToLoggedUser(ArrayList<Post> posts){
//        for(Post post : posts){
//            if(post.getCreator() != null && post.getCreator().getUsername().equals(State.getLoggedUser().getUsername())){
//                State.loggedUser.addPost(post);
//            }
//        }
//    }
//
//    private static void addPostToLoggedUser(Post post){
//        if(post.getCreator() != null && post.getCreator().getUsername().equals(State.getLoggedUser().getUsername())){
//            State.loggedUser.addPost(post);
//        }
//    }

    public static void addLikesToLikes(ArrayList<Like> likes){
        for(Like likeToAdd : likes){
            boolean exists = false;
//            for(Like likeInLikes : State.likes){
//                if(likeToAdd.getId().equals(likeInLikes.getId())){
//                    exists = true;
//                }
//            }
            for(int i = 0; i < State.getLikes().size(); i++){
                if(likeToAdd.getId().equals(State.getPosts().get(i).getId())){
                    State.getLikes().set(i, likeToAdd);
                    exists = true;
                }
            }
            if(!exists){
                State.likes.add(likeToAdd);
            }
        }
        MyViewModel.fetchLikesOfState();
    }

    public static Post getPostToBeShown() {
        return postToBeShown;
    }

    public static void setPostToBeShown(Post postToBeShown) {
        State.postToBeShown = postToBeShown;
//        State.addPostToLoggedUser(postToBeShown);
        MyViewModel.fetchPostToBeShown();
    }

    public static void onRequestRecieved(HashMap<String, Object> responseHashMap) {
        JSONObject responseJSON = null;
        int statusCode = (int)responseHashMap.get("statusCode");
        String response = (String) responseHashMap.get("response");
        Object instance = responseHashMap.get("instance");

        if(statusCode == 200){
            try {
                String token = null;
                responseJSON = new JSONObject(response);
                JSONObject jsonObject = responseJSON.getJSONObject("data");

                if(jsonObject.has("getLogin")){
                    if(!State.receivedGetLogin)
                        State.receivedGetLogin = true;

                    jsonObject = jsonObject.getJSONObject("getLogin");
                    token = (String) jsonObject.get("token");
                    token = "Bearer " + token;


                    State.setLoggedUser(User.fromJson(jsonObject.getJSONObject("loggedUser")));
//                    bitno da bude posle setLoggedUser -> MenuPagerAdapter.java 135 linija
                    State.setToken(token);
                    State.setCurrentState(State.STATE.HOME);

                    String query = "query GetAllPosts{ getAllPosts{ _id, content, like { _id, creator{ _id, username } },comments { _id, content, createdAt },imageUrl, creator { _id, username },createdAt, updatedAt } }";
                    HttpRequests.getRequest(query, "");
                }
                else if(jsonObject.has("getUser")){
                    JSONObject jsonGetUser = jsonObject.getJSONObject("getUser");
                    JSONObject jsonUser = jsonGetUser.getJSONObject("user");
                    jsonUser.put("comments", jsonGetUser.getJSONArray("comments"));

                    State.setLoggedUser(User.fromJson(jsonUser));
                    State.setCurrentState(State.STATE.HOME);

                    ((Activity)State.mainActivityContext).findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
                    ((Activity)State.mainActivityContext).findViewById(R.id.pager).setVisibility(View.VISIBLE);
                }
                else if(jsonObject.has("getOtherUserById")){
                    State.setOtherUser(User.fromJson(jsonObject.getJSONObject("getOtherUserById")));
                }
                else if(jsonObject.has("getPost")){
                    State.setPostToBeShown(Post.fromJson(jsonObject.getJSONObject("getPost")));
                }
                else if(jsonObject.has("postPost")) {
//                    ovaj post pripada ulogovanom korisniku
                    jsonObject = jsonObject.getJSONObject("postPost");
                    State.loggedUser.addPost(Post.fromJson(jsonObject));
                    MyViewModel.fetchLoggedUserOfState();
                }
                else if(jsonObject.has("getAllPosts")){
                    if (State.posts == null)
                        State.posts = new ArrayList<Post>();

                    if(!State.receivedGetAllPosts)
                        State.receivedGetAllPosts = true;

                    JSONArray lista = jsonObject.getJSONArray("getAllPosts");
//                    State.addPostsToPosts(Post.fromJsonArray(lista));
                    State.posts = Post.fromJsonArray(lista);
                    MyViewModel.fetchPostsOfState();
                }
                else if(jsonObject.has("postUser")){
                    Toast.makeText(State.getMainActivityContext(), "Log in", Toast.LENGTH_SHORT).show();
                    State.setCurrentState(STATE.LOGIN);
                }
                else if(jsonObject.has("getLikes")){
                    JSONArray lista = jsonObject.getJSONArray("getLikes");
                    State.addLikesToLikes(Like.fromJsonArray(lista));
                }
                else if(jsonObject.has("followUnfollow")){
                    boolean follwed = jsonObject.getBoolean("followUnfollow");
                    if(follwed){
                        State.getOtherUser().addFollower(State.getLoggedUser());
                        State.getLoggedUser().addFollowing(State.getOtherUser());
                    } else {
                        State.getOtherUser().removeFollower(State.getLoggedUser());
                        State.getLoggedUser().removeFollowing(State.getOtherUser());
                    }
                    MyViewModel.fetchOtherUserOfState();
                    MyViewModel.fetchLoggedUserOfState();
                    MyViewModel.fetchPostsOfState();
                }
                else if(jsonObject.has("likeUnlike")){
                    String query = "query GetAllPosts{ getAllPosts{ _id, content, like { _id, creator{ _id, username } },comments { _id, content, createdAt },imageUrl, creator { _id, username },createdAt, updatedAt } }";
                    HttpRequests.getRequest(query, "");

                    query = "query GetUser{getUser{user {_id, email, username, following { username}, followers { username }, posts {_id, content, creator {_id,username}, like {_id,creator {_id,username}, post { _id, imageUrl, creator{ _id, username } }, comment { _id}, createdAt, updatedAt},comments {_id}, imageUrl, createdAt, updatedAt},status, createdAt, updatedAt},comments {_id,content,creator {_id},like {_id,creator {_id,username}, post { _id, imageUrl }, comment { _id}, createdAt, updatedAt},createdAt, updatedAt}}}";
                    String variables = "{}";
                    HttpRequests.getRequest(query, "");
                }
                else if(jsonObject.has("updateStatus")){
                    String status = jsonObject.getString("updateStatus");
                    State.loggedUser.setStatus(status);
                    MyViewModel.fetchLoggedUserOfState();
                }

                if(instance != null){
                    if(jsonObject.has("getComment")){
                        State.requestsForUpdatingPostsToRecive --;
                        jsonObject = jsonObject.getJSONObject("getComment");
                        Comment oldComment = (Comment) instance;
                        Comment newComment = Comment.fromJson(jsonObject);
                        oldComment.updateComment(newComment);
                        if(State.requestsForUpdatingPostsToRecive == 0){
                            MyViewModel.fetchLoggedUserOfState();
                            MyViewModel.fetchPostsOfState();
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                responseJSON = new JSONObject(response);
                JSONArray lista = responseJSON.getJSONArray("errors");
                responseJSON = lista.getJSONObject(0);
                String message = responseJSON.getString("message");
                int status = statusCode;
                if(responseJSON.has("status")){
                    status = responseJSON.getInt("status");
                }
                System.out.println("Status code: " + String.valueOf(statusCode) + ", server sent: \nmessage: " + message + "\nstatus: " + Integer.toString(status));

                if(message.equals("Not authenticated.") || message.equals("The user not found.")){
                    State.logout();
                }
                Toast.makeText(State.getMainActivityContext(), message, Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println("Error occurred! Http Status Code: " + statusCode);
            }
        }
    }

    public static Context getMainActivityContext() {
        return mainActivityContext;
    }

    public static void setMainActivityContext(Context mainActivityContext) {
        State.mainActivityContext = mainActivityContext;
    }

    public static void logout(){
        LoginFragment.instance = null;
        SignupFragment.instance = null;

        PostFragment.instance = null;
        OtherProfileFragment.instance = null;

        HomeFragment.instance = null;
        SearchFragment.instance = null;
        LikeFragment.instance = null;
        ProfileFragment.instance = null;

        State.setLoggedUser(null);
        State.setPosts(null);
        State.setLikes(null);
        State.setToken(null);
    }
}
