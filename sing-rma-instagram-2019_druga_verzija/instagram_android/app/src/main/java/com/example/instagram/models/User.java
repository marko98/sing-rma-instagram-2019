package com.example.instagram.models;

import com.example.instagram.controllers.State;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

public class User {
    private String id, email, username, status;
    private ArrayList<Post> posts;
    private ArrayList<User> followings, followers;
    private ArrayList<Comment> comments;
    private DateTime createdAt, updatedAt;

    public User() {}

    public User(String id, String email, String username, String status,
                ArrayList<Post> posts, ArrayList<User> followings, ArrayList<User> followers,
                DateTime createdAt, DateTime updatedAt) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.status = status;
        this.posts = posts;
        this.followings = followings;
        this.followers = followers;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Post> getPosts() {
        if(this.posts != null && !this.posts.isEmpty()){
            Collections.sort(this.posts, new Comparator<Post>() {
                @Override
                public int compare(Post p1, Post p2) {
                    return p2.getUpdatedAt().compareTo(p1.getUpdatedAt());
                }
            });
        }
        return posts;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    public void addPost(Post post){
        if (this.posts == null){
            this.posts = new ArrayList<Post>();
        }

        for(int i = 0; i < this.posts.size(); i++){
            if (this.posts.get(i).getId().equals(post.getId())) {
                this.posts.set(i, post);
                return;
            }
        }
        this.posts.add(post);
    }

    public ArrayList<User> getFollowings() {
        return followings;
    }

    public void setFollowings(ArrayList<User> followings) {
        this.followings = followings;
    }

    public ArrayList<User> getFollowers() {
        return followers;
    }

    public void setFollowers(ArrayList<User> followers) {
        this.followers = followers;
    }

    public void addFollower(User user){
        this.followers.add(user);
    }

    public void removeFollower(User user){
        for(User follower : this.followers){
            if(follower.getUsername().equals(user.getUsername())){
                this.followers.remove(follower);
            }
        }
    }

    public boolean isFollowing(User user){
        for(User following : this.followings){
            if(following.getUsername() != null && following.getUsername().equals(user.getUsername())){
                return true;
            }
        }
        return false;
    }

    public void addFollowing(User user){
        this.followings.add(user);
    }

    public void removeFollowing(User user){
        try{
            for(User following : this.followings){
                if(following.getUsername().equals(user.getUsername())){
                    this.followings.remove(following);
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public static User fromJson(JSONObject o){
        User user = new User();
        if(o.has("_id")){
            try {
                user.setId(o.getString("_id"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("email")){
            try {
                user.setEmail(o.getString("email"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("username")){
            try {
                user.setUsername(o.getString("username"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("status")){
            try {
                user.setStatus(o.getString("status"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("posts")){
            try {
                JSONArray jsonArray = (JSONArray) o.get("posts");
                user.setPosts(Post.fromJsonArray(jsonArray));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("following")){
            try {
                JSONArray jsonArray = o.getJSONArray("following");
                user.setFollowings(User.fromJsonArray(jsonArray));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("followers")){
            try {
                JSONArray jsonArray = (JSONArray) o.get("followers");
                user.setFollowers(User.fromJsonArray(jsonArray));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("createdAt")){
            try {
                String dateTimeString = o.getString("createdAt");
                DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                DateTime dateTime = fmt.parseDateTime(dateTimeString);
                dateTime = dateTime.plusHours(1);
                user.setCreatedAt(dateTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("updatedAt")){
            try {
                String dateTimeString = o.getString("updatedAt");
                DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                DateTime dateTime = fmt.parseDateTime(dateTimeString);
                dateTime = dateTime.plusHours(1);
                user.setUpdatedAt(dateTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("comments")){
            try {
                JSONArray jsonArray = (JSONArray) o.get("comments");
                user.setComments(Comment.fromJsonArray(jsonArray));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    public static ArrayList<User> fromJsonArray(JSONArray array){
        ArrayList<User> lista = new ArrayList<User>();

        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject o = array.getJSONObject(i);
                lista.add(User.fromJson(o));
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        return lista;
    }
}
