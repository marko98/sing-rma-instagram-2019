package com.example.instagram.controllers.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.controllers.ProcessBitmap;
import com.example.instagram.controllers.MyViewModel;
import com.example.instagram.controllers.interfaces.AsyncTaskInteraction;
import com.example.instagram.controllers.interfaces.FragmentInteraction;
import com.example.instagram.R;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.recyclerView.RecyclerViewItemListener;
import com.example.instagram.controllers.recyclerView.ProfilePostRecyclerViewAdapter;
import com.example.instagram.models.Comment;
import com.example.instagram.models.Like;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import java.util.ArrayList;
import java.util.HashMap;

public class OtherProfileFragment extends InstagramFragment implements AsyncTaskInteraction, RecyclerViewItemListener {
    private static final String DATA = "OTHER_PROFILE";
    public static OtherProfileFragment instance;

    private String data;
    private View mainView;
    private RecyclerView recyclerView;
    private Button buttonChangeData;
    private MyViewModel mvm;
    private LayoutInflater inflater;

    private FragmentInteraction listener;

    public OtherProfileFragment(String text) {
        super(State.STATE.PROFILE);
        System.out.println("OtherProfileFragment");
        Bundle args = new Bundle();
        args.putString(DATA, text);
        this.setArguments(args);
        this.mvm = new MyViewModel();
    }

    public static synchronized OtherProfileFragment getInstance(String text) {
        if (OtherProfileFragment.instance == null) {
            OtherProfileFragment.instance = new OtherProfileFragment(text);
        }
        return OtherProfileFragment.instance;
    }

    @Override
    public void onAttach(Context context) {
        System.out.println("onAttach");
        super.onAttach(context);
        //ova metoda se poziva kada se nakaci na context, odnosno na activity i desi se sledece
        //proveravamo da li activity implementira nas interfejs
        if (context instanceof FragmentInteraction) {
            //ako implementira, dobijamo referencu na activity
            this.listener = (FragmentInteraction) context;
            this.inflater = getLayoutInflater().from(context);
        } else {
            //u suportnom bacamo gresku, jer nije moguce komunicirati
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        System.out.println("onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            this.data = getArguments().getString(DATA);
            System.out.println("Data vrednost: " + this.data);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("onCreateView");
        View mView = inflater.inflate(R.layout.fragment_other_profile, container, false);
        this.mainView = mView;
        this.recyclerView = mView.findViewById(R.id.recyclerView);

        // setujemo sta zelimo da osluskujemo, inace postavljamo ga ovde jer su tu i view-ovi kreirani
        this.setWhatToObserve();
        return mView;
    }

    private void setWhatToObserve(){
        this.mvm.getOtherUserOfState().observe((LifecycleOwner) this, new Observer<User>(){
            @Override
            public void onChanged(User user) {
                if(State.getCurrentState().equals(State.STATE.OTHERS_PROFILE)) {
                    if (user != null && user.getPosts() != null && user.getFollowers() != null && user.getFollowings() != null) {
                        ArrayList<Post> posts = user.getPosts();
                        if (!posts.isEmpty()) {
                            drawData(posts);
                        }
                    } else {
                        mainView.findViewById(R.id.progressBarSpinnerHead).setVisibility(View.VISIBLE);
                        mainView.findViewById(R.id.constraintLayoutHead).setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void setWhatToObserve2(){
        this.mvm.getOtherUserOfState().observe((LifecycleOwner) this, new Observer<User>(){
            @Override
            public void onChanged(User user) {
                if (user != null && user.getPosts() != null && user.getFollowers() != null && user.getFollowings() != null) {
                    ArrayList<Post> posts = user.getPosts();
                    if(!posts.isEmpty()){
                        drawData(posts);
                    }
                } else {
                    mainView.findViewById(R.id.progressBarSpinnerHead).setVisibility(View.VISIBLE);
                    mainView.findViewById(R.id.constraintLayoutHead).setVisibility(View.GONE);
                }
            }
        });
    }

    private void drawData(ArrayList<Post> posts){
        System.out.println("drawData");

        User user = State.getOtherUser();

        if (user != null && user.getPosts() != null && user.getFollowers() != null && user.getFollowings() != null) {
            mainView.findViewById(R.id.progressBarSpinnerHead).setVisibility(View.GONE);
            mainView.findViewById(R.id.constraintLayoutHead).setVisibility(View.VISIBLE);

            ((TextView) mainView.findViewById(R.id.textViewUsername)).setText(State.getOtherUser().getUsername());
            ((TextView) mainView.findViewById(R.id.textViewUsername2)).setText(State.getOtherUser().getUsername());
            ((EditText) mainView.findViewById(R.id.editTextStatus)).setText(State.getOtherUser().getStatus());

            ((TextView) mainView.findViewById(R.id.textViewNumOfPosts)).setText(String.valueOf(State.getOtherUser().getPosts().size()) + "\n Posts");
            ((TextView) mainView.findViewById(R.id.textViewNumOfFollowers)).setText(String.valueOf(State.getOtherUser().getFollowers().size()) + "\n Followers");
            ((TextView) mainView.findViewById(R.id.textViewNumOfFollowing)).setText(String.valueOf(State.getOtherUser().getFollowings().size()) + "\n Following");

            if(State.getOtherUser() != null){
                Button buttonFollowUnfollow = (Button) mainView.findViewById(R.id.buttonFollowUnfollow);
                boolean result = State.getLoggedUser().isFollowing(State.getOtherUser());
                if(!result){
                    buttonFollowUnfollow.setText("follow");
                } else {
                    buttonFollowUnfollow.setText("unfollow");
                }
                buttonFollowUnfollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HttpRequests.postRequest("mutation FollowUnfollow($userId: ID!){followUnfollow(userId: $userId)}", "{\"userId\": \"" + State.getOtherUser().getId() + "\"}");
//                        boolean result = State.getLoggedUser().isFollowing(State.getOtherUser());
//                        if(!result){
//                            ((Button)v).setText("follow");
//                        } else {
//                            ((Button)v).setText("unfollow");
//                        }
                    }
                });
            }

            ((ImageView) mainView.findViewById(R.id.imageViewProfileImage)).setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource((Context) listener, R.drawable.user_icon));
        }

        if(posts != null){
            ArrayList<Post> postsToGetBitmap = new ArrayList<>();
            for(Post post : posts){
                if(post.getBitmap() == null){
                    postsToGetBitmap.add(post);
                }
            }
            if(!postsToGetBitmap.isEmpty()){
                HttpRequests.getPostsBitmaps((AsyncTaskInteraction) this, postsToGetBitmap);
                ((ProgressBar) mainView.findViewById(R.id.progressBarSpinner)).setVisibility(View.VISIBLE);
                this.recyclerView.setVisibility(View.GONE);
            } else {
                for(Post post : posts){
                    if (user != null)
                        post.setCreator(user);
                }
                ProfilePostRecyclerViewAdapter postRecyclerViewAdapter = new ProfilePostRecyclerViewAdapter((Context) listener, posts, (RecyclerViewItemListener) this);
                this.recyclerView.setAdapter(postRecyclerViewAdapter);
                GridLayoutManager gridLayoutManager = new GridLayoutManager((Context) listener, 3, GridLayoutManager.VERTICAL, false);
                this.recyclerView.setLayoutManager(gridLayoutManager);

                ((ProgressBar) mainView.findViewById(R.id.progressBarSpinner)).setVisibility(View.GONE);
                this.recyclerView.setVisibility(View.VISIBLE);
            }
        }
    }

    public void manipulisiFragmentom() {
        System.out.println("Manipulacija nad OtherProfileFragment-om");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        System.out.println("onViewCreated");
    }

    @Override
    public void onDetach() {
        System.out.println("onDetach");
        super.onDetach();
        this.listener = null;
    }

    //    ----------------------------- INTERFACE METHODS IMPLEMENTATION -------------------------------

    @Override
    public void onPostGotBitmap() {
        if(State.getOtherUser() != null && State.getOtherUser().getPosts() != null)
            drawData(State.getOtherUser().getPosts());
        ((ProgressBar) mainView.findViewById(R.id.progressBarSpinner)).setVisibility(View.GONE);
        this.recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHttpRequestReceived(HttpRequests.REQUEST_NAME requestName) {

    }

    @Override
    public void onHttpPostRequestRecieved(HashMap<String, Object> hashMap, HttpRequests.REQUEST_NAME requestName) {

    }

    @Override
    public void onPostClick(Post post) {
        Toast.makeText((Context) listener, post.getId() + " is clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLikeClick(Like like) {

    }

    @Override
    public void onCommentClick(Comment comment) {

    }
}
