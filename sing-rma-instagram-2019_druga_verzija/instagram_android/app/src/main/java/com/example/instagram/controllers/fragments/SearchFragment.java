package com.example.instagram.controllers.fragments;


import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.controllers.MyViewModel;
import com.example.instagram.controllers.interfaces.AsyncTaskInteraction;
import com.example.instagram.controllers.interfaces.FragmentInteraction;
import com.example.instagram.R;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.recyclerView.ProfilePostRecyclerViewAdapter;
import com.example.instagram.controllers.recyclerView.RecyclerViewItemListener;
import com.example.instagram.controllers.recyclertreeview_lib.TreeNode;
import com.example.instagram.models.Comment;
import com.example.instagram.models.Like;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import java.util.ArrayList;
import java.util.HashMap;

public class SearchFragment extends InstagramFragment implements AsyncTaskInteraction, RecyclerViewItemListener {
    private static final String DATA = "SEARCH";
    public static SearchFragment instance;

    private String data;
    private View mainView;
    private LayoutInflater layoutInflater;
    private Button buttonChangeData;
    private RecyclerView postRecyclerView;
    private MyViewModel mvm;
    private InstagramFragment searchFragment;
    private ArrayList<Post> postsToDisplay;

    private FragmentInteraction listener;

    public SearchFragment(String text) {
        super(State.STATE.SEARCH);
        System.out.println("SearchFragment");
        Bundle args = new Bundle();
        args.putString(DATA, text);
        this.setArguments(args);
        this.mvm = new MyViewModel();
        this.searchFragment = this;
    }

    public static synchronized SearchFragment getInstance(String text) {
        if (SearchFragment.instance == null) {
            SearchFragment.instance = new SearchFragment(text);
        }
        return SearchFragment.instance;
    }

    @Override
    public void onAttach(Context context) {
        System.out.println("onAttach");
        super.onAttach(context);
        //ova metoda se poziva kada se nakaci na context, odnosno na activity i desi se sledece
        //proveravamo da li activity implementira nas interfejs
        if (context instanceof FragmentInteraction) {
            //ako implementira, dobijamo referencu na activity
            this.listener = (FragmentInteraction) context;

        } else {
            //u suportnom bacamo gresku, jer nije moguce komunicirati
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        System.out.println("onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            this.data = getArguments().getString(DATA);
            System.out.println("Data vrednost: " + this.data);
//            System.out.println("Data vrednost: " + this.data);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("onCreateView");
        // Inflate the layout for this fragment
        this.layoutInflater = inflater;
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        this.mainView = view;
        this.postRecyclerView = this.mainView.findViewById(R.id.recyclerView);

//        drawData();

        // setujemo sta zelimo da osluskujemo, inace postavljamo ga ovde jer su tu i view-ovi kreirani
        this.setWhatToObserve();
        return view;
    }

    private void setWhatToObserve(){
//        this.mvm.getCurrentStateOfState().observe((LifecycleOwner) this.listener, new Observer<State.STATE>(){
//            @Override
//            public void onChanged(State.STATE state) {
//                if (State.getCurrentState().equals(State.STATE.SEARCH)){
//                    if (state.equals(State.STATE.SEARCH)) {
//                        User loggedUser = State.getLoggedUser();
//                        if(loggedUser != null){
//                            postsToDisplay = new ArrayList<>();
//                            ArrayList<User> followings = State.getLoggedUser().getFollowings();
//                            ArrayList<Post> posts = State.getPosts();
//                            if(posts != null){
//                                for(Post post : posts){
//                                    if(!post.getCreator().getUsername().equals(loggedUser.getUsername())){
//                                        if(followings == null || followings.isEmpty()){
//                                            postsToDisplay.add(post);
//                                        } else {
//                                            boolean exists = false;
//                                            for(User user : followings){
//                                                if(post.getCreator().getUsername().equals(user.getUsername())){
//                                                    exists = true;
//                                                }
//                                            }
//                                            if(!exists){
//                                                postsToDisplay.add(post);
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//
//                            if(!postsToDisplay.isEmpty()){
//
//                                ArrayList<Post> postsToGetBitmap = new ArrayList<>();
//                                for(Post post : postsToDisplay){
//                                    if(post.getBitmap() == null){
//                                        postsToGetBitmap.add(post);
//                                    }
//                                }
//
//                                if(!postsToGetBitmap.isEmpty()){
//                                    HttpRequests.getPostsBitmaps((AsyncTaskInteraction) searchFragment, postsToGetBitmap);
//
//                                    mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.VISIBLE);
//                                    postRecyclerView.setVisibility(View.GONE);
//                                } else {
//                                    drawData(postsToDisplay);
//                                    mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
//                                    postRecyclerView.setVisibility(View.VISIBLE);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        });

//        this.mvm.getPostsOfState().observe((LifecycleOwner) this.listener, new Observer<ArrayList<Post>>(){
//            @Override
//            public void onChanged(ArrayList<Post> posts) {
//                User loggedUser = State.getLoggedUser();
//                if (loggedUser != null) {
//                    postsToDisplay = new ArrayList<>();
//                    ArrayList<User> followings = State.getLoggedUser().getFollowings();
//
//                    if (posts != null) {
//                        for (Post post : posts) {
//                            if (!post.getCreator().getUsername().equals(loggedUser.getUsername())) {
//                                if (followings == null || followings.isEmpty()) {
//                                    postsToDisplay.add(post);
//                                } else {
//                                    boolean exists = false;
//                                    for (User user : followings) {
//                                        if (post.getCreator().getUsername().equals(user.getUsername())) {
//                                            exists = true;
//                                        }
//                                    }
//                                    if (!exists) {
//                                        postsToDisplay.add(post);
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                    if (!postsToDisplay.isEmpty()) {
//
//                        ArrayList<Post> postsToGetBitmap = new ArrayList<>();
//                        for (Post post : postsToDisplay) {
//                            if (post.getBitmap() == null) {
//                                postsToGetBitmap.add(post);
//                            }
//                        }
//
//                        if (!postsToGetBitmap.isEmpty()) {
//                            HttpRequests.getPostsBitmaps((AsyncTaskInteraction) searchFragment, postsToGetBitmap);
//
//                            mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.VISIBLE);
//                            postRecyclerView.setVisibility(View.GONE);
//                        } else {
//                            drawData(postsToDisplay);
//                            mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
//                            postRecyclerView.setVisibility(View.VISIBLE);
//                        }
//                    } else {
//                        mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
//                    }
//                }
//            }
//        });

        this.mvm.getPostsOfState().observe((LifecycleOwner) this.listener, new Observer<ArrayList<Post>>(){
            @Override
            public void onChanged(ArrayList<Post> posts) {
                if (posts != null && !posts.isEmpty()) {
                    ArrayList<Post> postsToGetBitmap = new ArrayList<>();
                    for (Post post : posts) {
                        if (post.getBitmap() == null) {
                            postsToGetBitmap.add(post);
                        }
                    }
                    if (!postsToGetBitmap.isEmpty()) {
                        HttpRequests.getPostsBitmaps((AsyncTaskInteraction) searchFragment, postsToGetBitmap);

                        mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.VISIBLE);
                        postRecyclerView.setVisibility(View.GONE);
                    } else {
                        drawData();
                        mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
                        postRecyclerView.setVisibility(View.VISIBLE);
                    }
                } else if((posts == null || posts.isEmpty()) && State.receivedGetAllPosts){
                    mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
                    postRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void drawData(){
        System.out.println("drawData");

        ArrayList<Post> posts = State.getPosts();
        ArrayList<Post> newPosts = new ArrayList<>();
        for(Post post : posts){
//            moraju biti postovi ljudi koje ulogovani korisnik ne prati
            if(!post.getCreator().getUsername().equals(State.getLoggedUser().getUsername()) && !State.getLoggedUser().isFollowing(post.getCreator())){
                newPosts.add(post);
            }
        }

        if(!newPosts.isEmpty()){
            ProfilePostRecyclerViewAdapter postRecyclerViewAdapter = new ProfilePostRecyclerViewAdapter((Context) listener, newPosts, (RecyclerViewItemListener) searchFragment);
            postRecyclerView.setAdapter(postRecyclerViewAdapter);
            GridLayoutManager gridLayoutManager = new GridLayoutManager((Context) listener, 3, GridLayoutManager.VERTICAL, false);
            postRecyclerView.setLayoutManager(gridLayoutManager);
        }
    }

    public void manipulisiFragmentom() {
        System.out.println("Manipulacija nad SearchFragment-om");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        System.out.println("onViewCreated");
    }

    @Override
    public void onDetach() {
        System.out.println("onDetach");
        super.onDetach();
        this.listener = null;
    }

    //    ----------------------------- INTERFACE METHODS IMPLEMENTATION -------------------------------

    @Override
    public void onPostGotBitmap() {
        drawData();
        mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
        postRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHttpRequestReceived(HttpRequests.REQUEST_NAME requestName) {

    }

    @Override
    public void onHttpPostRequestRecieved(HashMap<String, Object> hashMap, HttpRequests.REQUEST_NAME requestName) {

    }

    @Override
    public void onPostClick(Post post) {
        Toast.makeText((Context) listener, post.getId() + " is clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLikeClick(Like like) {

    }

    @Override
    public void onCommentClick(Comment comment) {

    }

}


//    private void setWhatToObserve2(){
//        this.mvm.getCurrentStateOfState().observe((LifecycleOwner) this.listener, new Observer<State.STATE>(){
//            @Override
//            public void onChanged(State.STATE state) {
//                if (state.equals(State.STATE.SEARCH)) {
//                    User loggedUser = State.getLoggedUser();
//                    if(loggedUser != null){
//                        postsToDisplay = new ArrayList<>();
//                        ArrayList<User> followings = State.getLoggedUser().getFollowings();
//                        ArrayList<Post> posts = State.getPosts();
//                        if(posts != null){
//                            for(Post post : posts){
//                                if(!post.getCreator().getUsername().equals(loggedUser.getUsername())){
//                                    if(followings == null || followings.isEmpty()){
//                                        postsToDisplay.add(post);
//                                    } else {
//                                        boolean exists = false;
//                                        for(User user : followings){
//                                            if(post.getCreator().getUsername().equals(user.getUsername())){
//                                                exists = true;
//                                            }
//                                        }
//                                        if(!exists){
//                                            postsToDisplay.add(post);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//
//                        if(!postsToDisplay.isEmpty()){
//
//                            ArrayList<Post> postsToGetBitmap = new ArrayList<>();
//                            for(Post post : postsToDisplay){
//                                if(post.getBitmap() == null){
//                                    postsToGetBitmap.add(post);
//                                }
//                            }
//
//                            if(!postsToGetBitmap.isEmpty()){
//                                HttpRequests.getPostsBitmaps((AsyncTaskInteraction) searchFragment, postsToGetBitmap);
//
//                                mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.VISIBLE);
//                                postRecyclerView.setVisibility(View.GONE);
//                            } else {
//                                drawData(postsToDisplay);
//                                mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
//                                postRecyclerView.setVisibility(View.VISIBLE);
//                            }
//                        }
//                    }
//                }
//            }
//        });
//
//        this.mvm.getPostsOfState().observe((LifecycleOwner) this.listener, new Observer<ArrayList<Post>>(){
//            @Override
//            public void onChanged(ArrayList<Post> posts) {
//                User loggedUser = State.getLoggedUser();
//                if(loggedUser != null){
//                    postsToDisplay = new ArrayList<>();
//                    ArrayList<User> followings = State.getLoggedUser().getFollowings();
//
//                    if(posts != null){
//                        for(Post post : posts){
//                            if(!post.getCreator().getUsername().equals(loggedUser.getUsername())){
//                                if(followings == null || followings.isEmpty()){
//                                    postsToDisplay.add(post);
//                                } else {
//                                    boolean exists = false;
//                                    for(User user : followings){
//                                        if(post.getCreator().getUsername().equals(user.getUsername())){
//                                            exists = true;
//                                        }
//                                    }
//                                    if(!exists){
//                                        postsToDisplay.add(post);
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                    if(!postsToDisplay.isEmpty()){
//
//                        ArrayList<Post> postsToGetBitmap = new ArrayList<>();
//                        for(Post post : postsToDisplay){
//                            if(post.getBitmap() == null){
//                                postsToGetBitmap.add(post);
//                            }
//                        }
//
//                        if(!postsToGetBitmap.isEmpty()){
//                            HttpRequests.getPostsBitmaps((AsyncTaskInteraction) searchFragment, postsToGetBitmap);
//
//                            mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.VISIBLE);
//                            postRecyclerView.setVisibility(View.GONE);
//                        } else {
//                            drawData(postsToDisplay);
//                            mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
//                            postRecyclerView.setVisibility(View.VISIBLE);
//                        }
//                    }
//                }
//            }
//        });
//    }