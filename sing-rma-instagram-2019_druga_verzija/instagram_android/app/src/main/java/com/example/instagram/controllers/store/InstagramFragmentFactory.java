package com.example.instagram.controllers.store;

import com.example.instagram.controllers.fragments.HomeFragment;
import com.example.instagram.controllers.fragments.LikeFragment;
import com.example.instagram.controllers.fragments.LoginFragment;
import com.example.instagram.controllers.fragments.OtherProfileFragment;
import com.example.instagram.controllers.fragments.PostFragment;
import com.example.instagram.controllers.fragments.ProfileFragment;
import com.example.instagram.controllers.fragments.SearchFragment;
import com.example.instagram.controllers.fragments.SignupFragment;
import com.example.instagram.controllers.fragments.InstagramFragment;

public class InstagramFragmentFactory implements AbstractFactory {
    public InstagramFragmentFactory(){}

//    public InstagramFragment makeInstagramFragment(InstagramFragment.TYPES type){
//        InstagramFragment instagramFragment = null;
//        if(type.equals(InstagramFragment.TYPES.HOME)){
//            instagramFragment = HomeFragment.getInstance("Home");
//        } else if(type.equals(InstagramFragment.TYPES.SEARCH)){
//            instagramFragment = SearchFragment.getInstance("Search");
//        } else if(type.equals(InstagramFragment.TYPES.LIKE)){
//            instagramFragment = LikeFragment.getInstance("Like");
//        } else if(type.equals(InstagramFragment.TYPES.PROFILE)){
//            instagramFragment = ProfileFragment.getInstance("Profile");
//        } else if(type.equals(InstagramFragment.TYPES.LOGIN)){
//            instagramFragment = LoginFragment.getInstance();
//        } else if(type.equals(InstagramFragment.TYPES.SIGNUP)){
//            instagramFragment = SignupFragment.getInstance();
//        } else {
//            instagramFragment = ProfileFragment.getInstance("Profile");
//        }
//
//        return instagramFragment;
//    }

    @Override
    public Object create(Object instagramFactoryType) {
        Object instagramFragment = null;
        switch((InstagramFragment.INSTAGRAM_FRAGMENT_TYPES)instagramFactoryType){
            case POST:
                instagramFragment = PostFragment.getInstance("Post");
                break;
            case OTHER_PROFILE:
                instagramFragment = OtherProfileFragment.getInstance("OtherProfile");
                break;
            case HOME:
                instagramFragment = HomeFragment.getInstance("Home");
                break;
            case SEARCH:
                instagramFragment = SearchFragment.getInstance("Search");
                break;
            case LIKE:
                instagramFragment = LikeFragment.getInstance("Like");
                break;
            case PROFILE:
                instagramFragment = ProfileFragment.getInstance("Profile");
                break;
            case LOGIN:
                instagramFragment = LoginFragment.getInstance();
                break;
            case SIGNUP:
                instagramFragment = SignupFragment.getInstance();
                break;
            default:
                instagramFragment = ProfileFragment.getInstance("Profile");
        }
        return instagramFragment;
    }
}
