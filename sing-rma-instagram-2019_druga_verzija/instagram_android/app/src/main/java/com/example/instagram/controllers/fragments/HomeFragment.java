package com.example.instagram.controllers.fragments;


import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.controllers.MyViewModel;
import com.example.instagram.controllers.interfaces.AsyncTaskInteraction;
import com.example.instagram.controllers.interfaces.FragmentInteraction;
import com.example.instagram.R;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.recyclerView.RecyclerViewItemListener;
import com.example.instagram.controllers.recyclerView.treeViewBinders.CommentNodeBinder;
import com.example.instagram.controllers.recyclerView.treeViewBinders.CommentWithCommentsNodeBinder;
import com.example.instagram.controllers.recyclerView.treeViewBinders.PostNodeBinder;
import com.example.instagram.controllers.recyclertreeview_lib.TreeNode;
import com.example.instagram.controllers.recyclertreeview_lib.TreeViewAdapter;
import com.example.instagram.models.Comment;
import com.example.instagram.models.Like;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class HomeFragment extends InstagramFragment implements RecyclerViewItemListener, AsyncTaskInteraction {
    private static final String DATA = "HOME";
    public static HomeFragment instance;

    private String data;
    private View mainView;
    private LayoutInflater layoutInflater;
    private MyViewModel mvm;
    private RecyclerView postRecyclerView;
    private InstagramFragment homeFragment;
    private TreeViewAdapter treeViewAdapter;

    private FragmentInteraction listener;

    public HomeFragment(String text) {
        super(State.STATE.HOME);
        System.out.println("HomeFragment");
        Bundle args = new Bundle();
        args.putString(DATA, text);
        this.setArguments(args);
        this.mvm = new MyViewModel();
        this.homeFragment = this;
    }

    public static synchronized HomeFragment getInstance(String text) {
        if (HomeFragment.instance == null) {
            HomeFragment.instance = new HomeFragment(text);
        }
        return HomeFragment.instance;
    }

    @Override
    public void onAttach(Context context) {
        System.out.println("onAttach");
        super.onAttach(context);
        //ova metoda se poziva kada se nakaci na context, odnosno na activity i desi se sledece
        //proveravamo da li activity implementira nas interfejs
        if (context instanceof FragmentInteraction) {
            //ako implementira, dobijamo referencu na activity
            this.listener = (FragmentInteraction) context;

        } else {
            //u suportnom bacamo gresku, jer nije moguce komunicirati
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        System.out.println("onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            this.data = getArguments().getString(DATA);
            System.out.println("Data vrednost: " + this.data);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        System.out.println("onCreateView");
        // Inflate the layout for this fragment
        this.layoutInflater = inflater;
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        this.mainView = view;
        this.postRecyclerView = this.mainView.findViewById(R.id.postRecyclerView);

//        drawData();

        // setujemo sta zelimo da osluskujemo, inace postavljamo ga ovde jer su tu i view-ovi kreirani
        this.setWhatToObserve();
        return view;
    }

    private void setWhatToObserve(){
//        OnClick Listeners
        Button buttonLogout = (Button) this.mainView.findViewById(R.id.buttonLogout);

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                State.logout();
            }
        });

//        this.mvm.getCurrentStateOfState().observe((LifecycleOwner) this.listener, new Observer<State.STATE>(){
//            @Override
//            public void onChanged(State.STATE state) {
//            if (State.getCurrentState().equals(State.STATE.HOME)){
//                drawData();
//            }
//            }
//        });

        this.mvm.getLoggedUserOfState().observe((LifecycleOwner) this.listener, new Observer<User>(){
            @Override
            public void onChanged(User user) {
                User i = user;
            }
        });

//        this.mvm.getLoggedUserOfState().observe((LifecycleOwner) this.listener, new Observer<User>(){
//            @Override
//            public void onChanged(User user) {
//            if (State.getCurrentState().equals(State.STATE.HOME)){
//                if(user != null && State.getToken() != null){
//                    if(State.getPosts() == null || State.getPosts().isEmpty()){
//                        String query = "query GetAllPosts{ getAllPosts{ _id, content, like { _id, creator{ _id, username } },comments { _id, content, createdAt },imageUrl, creator { _id, username },createdAt, updatedAt } }";
//                        HttpRequests.getRequest(query, "");
//                    } else {
//                        drawData();
//                    }
//                }
//            }
//            }
//        });

//        this.mvm.getPostsOfState().observe((LifecycleOwner) this.listener, new Observer<ArrayList<Post>>(){
//            @Override
//            public void onChanged(ArrayList<Post> posts) {
//                if (State.getCurrentState().equals(State.STATE.HOME)){
//                    if (posts != null && !posts.isEmpty()) {
//                        ArrayList<Post> postsToGetBitmap = new ArrayList<>();
//                        for (Post post : State.getPosts()) {
//                            if (post.getBitmap() == null) {
//                                postsToGetBitmap.add(post);
//                            }
//                        }
//                        if (!postsToGetBitmap.isEmpty()) {
//                            HttpRequests.getPostsBitmaps((AsyncTaskInteraction) homeFragment, postsToGetBitmap);
//
//                            mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.VISIBLE);
//                            postRecyclerView.setVisibility(View.GONE);
//                        } else {
//                            mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
//                            postRecyclerView.setVisibility(View.VISIBLE);
//                        }
//                        drawTreeView(posts);
//                    }
//                }
//            }
//        });

        this.mvm.getPostsOfState().observe((LifecycleOwner) this.listener, new Observer<ArrayList<Post>>(){
            @Override
            public void onChanged(ArrayList<Post> posts) {
                    if (posts != null && !posts.isEmpty()) {
                        ArrayList<Post> postsToGetBitmap = new ArrayList<>();
                        for (Post post : State.getPosts()) {
                            if (post.getBitmap() == null) {
                                postsToGetBitmap.add(post);
                            }
                        }
                        if (!postsToGetBitmap.isEmpty()) {
                            HttpRequests.getPostsBitmaps((AsyncTaskInteraction) homeFragment, postsToGetBitmap);

                            mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.VISIBLE);
                            postRecyclerView.setVisibility(View.GONE);

                        } else {
                            mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
                            postRecyclerView.setVisibility(View.VISIBLE);
                        }
                        drawTreeView(posts);
                    } else if((posts == null || posts.isEmpty()) && State.receivedGetAllPosts){
                        mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
                        postRecyclerView.setVisibility(View.VISIBLE);
                    }
            }
        });
    }

    private void setWhatToObserve2(){
        this.mvm.getCurrentStateOfState().observe((LifecycleOwner) this.listener, new Observer<State.STATE>(){
            @Override
            public void onChanged(State.STATE state) {
            if (state.equals(State.STATE.HOME)) {
                drawData();
            }
            }
        });

        this.mvm.getLoggedUserOfState().observe((LifecycleOwner) this.listener, new Observer<User>(){
            @Override
            public void onChanged(User user) {
                if(user != null && State.getToken() != null){
                    if(State.getPosts() == null || State.getPosts().isEmpty()){
                        String query = "query GetAllPosts{ getAllPosts{ _id, content, like { _id },comments { _id, content, createdAt },imageUrl, creator { _id, username },createdAt, updatedAt } }";
                        HttpRequests.getRequest(query, "");
                    }
//                    if(State.getLoggedUser() == null || State.getLoggedUser() == null){
//                        String query = "query GetUser{getUser{ _id, email, username, following { username }, followers { username }, posts {_id,content,like {_id},comments {_id},imageUrl,createdAt,updatedAt},status, createdAt, updatedAt}}";
//                        String variables = "{}";
//                        HttpRequests.getRequest(query, variables);
//                    }
//                    if(State.getLikes().isEmpty()){
//                        String query = "query GetLikes{getLikes{_id, comment {_id},post {_id, imageUrl},creator {_id, username},createdAt,updatedAt}}";
//                        String variables = "{}";
//                        HttpRequests.getRequest(query, variables);
//                    }
                }
            }
        });

        this.mvm.getPostsOfState().observe((LifecycleOwner) this.listener, new Observer<ArrayList<Post>>(){
            @Override
            public void onChanged(ArrayList<Post> posts) {
                if (posts != null && !posts.isEmpty()) {
                    ArrayList<Post> postsToGetBitmap = new ArrayList<>();
                    for(Post post : State.getPosts()){
                        if(post.getBitmap() == null){
                            postsToGetBitmap.add(post);
                        }
                    }
                    if(!postsToGetBitmap.isEmpty()){
                        HttpRequests.getPostsBitmaps((AsyncTaskInteraction) homeFragment, postsToGetBitmap);

                        mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.VISIBLE);
                        postRecyclerView.setVisibility(View.GONE);
                    } else {
                        mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
                        postRecyclerView.setVisibility(View.VISIBLE);
                    }
                    drawTreeView(posts);
                }
            }
        });
    }

    private void drawData(){
        System.out.println("drawData");

//        if((State.getPosts() == null || State.getPosts().isEmpty()) && State.getToken() != null){
//            String query = "query GetAllPosts{ getAllPosts{ _id, content, like { _id, creator{ _id, username } },comments { _id, content, createdAt },imageUrl, creator { _id, username },createdAt, updatedAt } }";
//            HttpRequests.getRequest(query, "");
//        } else {
//            ArrayList<Post> posts = State.getPosts();
//            if(posts != null){
//                boolean readyToShow = true;
//                for(Post post : posts){
//                    if(post.getBitmap() == null){
//                        readyToShow = false;
//                    }
//                }
//                if(readyToShow){
//                    if(this.treeViewAdapter != null){
//                        this.treeViewAdapter.notifyDataSetChanged();
//                    } else {
//                        drawTreeView(State.getPosts());
//                    }
//                    mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
//                    postRecyclerView.setVisibility(View.VISIBLE);
//                }
//            }
//        }
    }

    public List<TreeNode> getTreeCommentNodesForPost(Post post){
        List<TreeNode> nodes = new ArrayList<>();

        ArrayList<Comment> neposeceni = new ArrayList<>();
        for (Comment comment : post.getComments()) {
            neposeceni.add(comment);
        }
        ArrayList<Comment> poseceni = new ArrayList<>();


        ArrayList<HashMap<String, Object>> parents = new ArrayList<>();

        TreeNode<Comment> rootCommentNode = new TreeNode<>(new Comment());
        HashMap<String, Object> rootEl = new HashMap<>();
        rootEl.put("el", rootCommentNode);
        rootEl.put("numChildren", -1);
        rootEl.put("added", 0);
        parents.add(rootEl);

        while (neposeceni.size() > 0) {
            Comment naObradi = neposeceni.remove(neposeceni.size() - 1);

            TreeNode<Comment> treeNode = new TreeNode<>(naObradi);

            ((TreeNode) parents.get(parents.size() - 1).get("el")).addChild(treeNode);
            parents.get(parents.size() - 1).put("added", (Integer)parents.get(parents.size() - 1).get("added") + 1);
            if (((Integer) parents.get(parents.size() - 1).get("numChildren")).equals((Integer)parents.get(parents.size() - 1).get("added"))) {
                parents.remove(parents.size() - 1);
            }

            if (!poseceni.contains(naObradi)) {
                poseceni.add(naObradi);

                ArrayList<Comment> children = naObradi.getComments();
                if (children != null && !children.isEmpty()) {
//                    dodajemo novog roditelja
                    HashMap<String, Object> newParent = new HashMap<>();
                    newParent.put("el", treeNode);
                    newParent.put("numChildren", children.size());
                    newParent.put("added", 0);
                    parents.add(newParent);

                    for (Comment child : children) {
                        if (!poseceni.contains(child)) {
                            neposeceni.add(child);
                        }
                    }
                }
            }
        }
        return rootCommentNode.getChildList();
    }

    public void drawTreeView(ArrayList<Post> posts){
        List<TreeNode> treeNodeList = new ArrayList<>();
        for(Post post : posts){
//            moraju biti postovi ljudi koje ulogovani korisnik prati
            if(!post.getCreator().getId().equals(State.getLoggedUser().getId()) && State.getLoggedUser().isFollowing(post.getCreator())){
                TreeNode parentNode = new TreeNode(post);
                for(TreeNode treeNode : getTreeCommentNodesForPost(post)){
                    parentNode.addChild(treeNode);
                }
                treeNodeList.add(parentNode);
            }
        }

        postRecyclerView.setLayoutManager(new LinearLayoutManager((Context) listener));
        treeViewAdapter = new TreeViewAdapter(treeNodeList, Arrays.asList(new PostNodeBinder((Context) listener), new CommentNodeBinder((Context) listener), new CommentWithCommentsNodeBinder((Context) listener)));
        treeViewAdapter.setOnTreeNodeListener(new TreeViewAdapter.OnTreeNodeListener() {
            @Override
            public boolean onClick(TreeNode node, RecyclerView.ViewHolder holder) {
                if (!node.isLeaf()) {
                    //Update and toggle the node.
                    onToggle(!node.isExpand(), holder);
//                    if (!node.isExpand())
//                        adapter.collapseBrotherNode(node);
                }
//                Toast.makeText(getContext(), node.getContent().getTheComment().getContent(), Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public void onToggle(boolean isExpand, RecyclerView.ViewHolder holder) {
                try{
                    CommentWithCommentsNodeBinder.ViewHolder dirViewHolder = (CommentWithCommentsNodeBinder.ViewHolder) holder;
                    final ImageView ivArrow = dirViewHolder.getImageViewArrow();
                    int rotateDegree = isExpand ? 180 : -180;
                    ivArrow.animate().rotationBy(rotateDegree).start();
                } catch (Exception e) {

                }
            }
        });
        postRecyclerView.setAdapter(treeViewAdapter);
    }

    public void manipulisiFragmentom() {
        System.out.println("Manipulacija nad HomeFragment-om");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        System.out.println("onViewCreated");
    }

    @Override
    public void onDetach() {
        System.out.println("onDetach");
        super.onDetach();
        this.listener = null;
    }

    //    ----------------------------- INTERFACE METHODS IMPLEMENTATION -------------------------------

    @Override
    public void onPostGotBitmap() {
        mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
        postRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHttpRequestReceived(HttpRequests.REQUEST_NAME requestName) {

    }

    @Override
    public void onHttpPostRequestRecieved(HashMap<String, Object> hashMap, HttpRequests.REQUEST_NAME requestName) {

    }

    @Override
    public void onPostClick(Post post) {
        Toast.makeText((Context) listener, post.getId() + " is clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLikeClick(Like like) {

    }

    @Override
    public void onCommentClick(Comment comment) {

    }
}
