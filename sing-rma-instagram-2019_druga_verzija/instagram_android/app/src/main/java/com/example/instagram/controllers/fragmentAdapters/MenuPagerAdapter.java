package com.example.instagram.controllers.fragmentAdapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.viewpager.widget.ViewPager;

import com.example.instagram.R;
import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.controllers.fragments.InstagramFragment;
import com.example.instagram.controllers.MyViewModel;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.store.InstagramFragmentFactory;

import java.util.ArrayList;

public class MenuPagerAdapter extends FragmentStatePagerAdapter {
    private static MenuPagerAdapter instance;
    private ViewPager viewPager;
    private InstagramFragmentFactory instagramFragmentFactory;
    private MyViewModel mvm;

    private ArrayList<InstagramFragment> fragments;

    private MenuPagerAdapter(FragmentManager fm, final Context context, final ViewPager viewPager){
        super(fm);
        this.viewPager = viewPager;

        this.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                InstagramFragment i = (InstagramFragment) getItem(position);
//                i.updateState();
//                i.update();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        this.instagramFragmentFactory = new InstagramFragmentFactory();
        fragments = new ArrayList<>();
        if((State.getCurrentState().equals(State.STATE.LOGIN) || State.getCurrentState().equals(State.STATE.SIGNUP))){
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.LOGIN));
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.SIGNUP));
        } else if(State.getCurrentState().equals(State.STATE.POST)){
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.POST));
        } else if(State.getCurrentState().equals(State.STATE.OTHERS_PROFILE)){
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.OTHER_PROFILE));
        } else {
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.HOME));
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.SEARCH));
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.LIKE));
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.PROFILE));
        }


        this.mvm = new MyViewModel();
        this.mvm.getCurrentStateOfState().observe((LifecycleOwner) context, new Observer<State.STATE>() {
            @Override
            public void onChanged(State.STATE state) {
                if((state.equals(State.STATE.LOGIN) || state.equals(State.STATE.SIGNUP)) && fragments.size()!=2){
                    fragments.clear();
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.LOGIN));
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.SIGNUP));
                    notifyDataSetChanged();
                } else if(State.getCurrentState().equals(State.STATE.POST)){
                    fragments.clear();
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.POST));
                    notifyDataSetChanged();
                } else if(State.getCurrentState().equals(State.STATE.OTHERS_PROFILE)){
                    fragments.clear();
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.OTHER_PROFILE));
                    notifyDataSetChanged();
                } else if((state.equals(State.STATE.HOME) || state.equals(State.STATE.SEARCH) || state.equals(State.STATE.LIKE) || state.equals(State.STATE.PROFILE)) && fragments.size()!=4){
                    fragments.clear();
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.HOME));
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.SEARCH));
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.LIKE));
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.PROFILE));
                    notifyDataSetChanged();
                }

                switch (state){
                    case LOGIN:
                        setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.LOGIN);
                        break;
                    case SIGNUP:
                        setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.SIGNUP);
                        break;
                    case HOME:
                        setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.HOME);
                        break;
                    case SEARCH:
                        setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.SEARCH);
                        break;
                    case LIKE:
                        setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.LIKE);
                        break;
                    case PROFILE:
                        setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.PROFILE);
                        break;
                    case POST:
                        setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.POST);
                        break;
                    case OTHERS_PROFILE:
                        setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.OTHER_PROFILE);
                        break;
                }
            }
        });

        this.mvm.getCurrentTokenOfState().observe((LifecycleOwner) context, new Observer<String>() {
            @Override
            public void onChanged(String token) {
                if(token != null){
//                    prebacicemo state na HOME kada dobije ulogovanog korisnika

//                    slucaj kada smo ostali ulogovani od gasenja aplikacije
                    if(State.getLoggedUser() == null){
                        String query = "query GetUser{getUser{user {_id, email, username, following { username}, followers { username }, posts {_id, content, creator {_id,username}, like {_id,creator {_id,username}, post { _id, imageUrl, creator{ _id, username } }, comment { _id}, createdAt, updatedAt},comments {_id}, imageUrl, createdAt, updatedAt},status, createdAt, updatedAt},comments {_id,content,creator {_id},like {_id,creator {_id,username}, post { _id, imageUrl }, comment { _id}, createdAt, updatedAt},createdAt, updatedAt}}}";
                        String variables = "{}";
                        HttpRequests.getRequest(query, variables);

                        query = "query GetAllPosts{ getAllPosts{ _id, content, like { _id, creator{ _id, username } },comments { _id, content, createdAt },imageUrl, creator { _id, username },createdAt, updatedAt } }";
                        HttpRequests.getRequest(query, "");
                    }
//                    slucaj kada smo se ulogovali
                    else {
//                        nista
                    }
                } else {
                    State.setCurrentState(State.STATE.LOGIN);

                    Context mainActivityContext = State.getMainActivityContext();
                    ((Activity)mainActivityContext).findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
                    ((Activity)mainActivityContext).findViewById(R.id.pager).setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public static synchronized MenuPagerAdapter getInstance(FragmentManager fm, Context context, ViewPager viewPager){
        if(MenuPagerAdapter.instance == null){
            MenuPagerAdapter.instance = new MenuPagerAdapter(fm, context, viewPager);
        }
        return MenuPagerAdapter.instance;
    }

    public static synchronized MenuPagerAdapter getInstance(){
        return MenuPagerAdapter.instance;
    }

    @Override
    public int getItemPosition(Object object) {
        int index = this.fragments.indexOf(object);
        if(this.fragments.indexOf(object) == -1){
            return POSITION_NONE;
        }
        return index;
    }

    @Override
    public Fragment getItem(int position) {
        System.out.println(position);
        Fragment fragment = (Fragment) this.fragments.get(position);

        Bundle args = new Bundle();
        args.putInt(InstagramFragment.ARG_OBJECT, position + 1);
        fragment.setArguments(args);
        return fragment;
    }

    public void setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES type){
        switch (type){
            case POST:
            case OTHER_PROFILE:
            case LOGIN:
            case HOME:
                this.viewPager.setCurrentItem(0);
                break;
            case SIGNUP:
            case SEARCH:
                this.viewPager.setCurrentItem(1);
                break;
            case LIKE:
                this.viewPager.setCurrentItem(2);
                break;
            case PROFILE:
                this.viewPager.setCurrentItem(3);
                break;
        }
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return "OBJECT " + (position + 1);
    }

}
