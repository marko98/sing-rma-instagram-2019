package com.example.instagram.models;

import android.graphics.Bitmap;

import com.example.instagram.R;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.recyclertreeview_lib.LayoutItemType;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Post implements LayoutItemType {
    private String id, content, imageUrl;
    private ArrayList<Like> likes;
    private ArrayList<Comment> comments;
    private User creator;
    private DateTime createdAt, updatedAt;
    private Bitmap bitmap;

    public Post() {}

    public Post(String id, String content, String imageUrl,
                ArrayList<Like> likes, ArrayList<Comment> comments,
                User creator,
                DateTime createdAt, DateTime updatedAt) {
        this.id = id;
        this.content = content;
        this.imageUrl = imageUrl;
        this.likes = likes;
        this.comments = comments;
        this.creator = creator;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArrayList<Like> getLikes() {
        return likes;
    }

    public void setLikes(ArrayList<Like> likes) {
        this.likes = likes;
    }

    public void addLike(Like like){
        this.likes.add(like);
    }

    public void removeLike(Like like){
        this.likes.remove(like);
    }

    public boolean isLiked(User user){
        if (this.likes != null) {
            for (Like like : this.likes) {
                if (like.getCreator() != null && like.getCreator().getUsername().equals(State.getLoggedUser().getUsername())) {
                    return true;
                }
            }
        }
        return false;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public static Post fromJson(JSONObject o){
        Post post = new Post();
        if(o.has("_id")){
            try {
                post.setId(o.getString("_id"));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("content")){
            try {
                post.setContent(o.getString("content"));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("like")){
            try {
                post.setLikes(Like.fromJsonArray(o.getJSONArray("like")));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("comments")){
            try {
                post.setComments(Comment.fromJsonArray(o.getJSONArray("comments")));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("imageUrl")){
            try {
                post.setImageUrl(o.getString("imageUrl"));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("creator")){
            try {
                User user = User.fromJson(o.getJSONObject("creator"));
                if(State.getLoggedUser() != null){
                    if(State.getLoggedUser().getId().equals(user.getId()))
                        user = State.getLoggedUser();
                }

                post.setCreator(user);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("createdAt")){
            try {
                String dateTimeString = o.getString("createdAt");
                DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                DateTime dateTime = fmt.parseDateTime(dateTimeString);

//                DateTimeZone f = DateTimeZone.getDefault();
//
//                DateTimeZone timeZone = dateTime.getZone();

                dateTime = dateTime.plusHours(1);

                post.setCreatedAt(dateTime);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("updatedAt")){
            try {
                String dateTimeString = o.getString("updatedAt");
                DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                DateTime dateTime = fmt.parseDateTime(dateTimeString);
                dateTime = dateTime.plusHours(1);
                post.setUpdatedAt(dateTime);
            } catch (Exception e){
                e.printStackTrace();
            }
        }

//        for(Like like : State.getLikes()){
//            if(like.getPost() != null && like.getPost().getId().equals(post.getId())){
//                like.setPost(post);
//            }
//        }

        return post;
    }

    public static ArrayList<Post> fromJsonArray(JSONArray array){
        ArrayList<Post> lista = new ArrayList<Post>();

        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject o = array.getJSONObject(i);
                lista.add(Post.fromJson(o));
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        return lista;
    }

    //    ----------------------------- INTERAFCE METHODS IMPLEMENTATION -------------------------------

    @Override
    public int getLayoutId() {
        return R.layout.post;
    }

    @Override
    public Comment getTheComment() {
        return null;
    }

    @Override
    public Post getThePost() {
        return this;
    }
}
