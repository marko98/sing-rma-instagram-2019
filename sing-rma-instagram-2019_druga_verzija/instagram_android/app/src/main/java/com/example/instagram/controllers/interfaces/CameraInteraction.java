package com.example.instagram.controllers.interfaces;

import android.content.Intent;

public interface CameraInteraction {
    public void onImageGet(Intent i);
}
