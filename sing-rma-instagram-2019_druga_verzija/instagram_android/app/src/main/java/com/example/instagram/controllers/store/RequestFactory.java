//package com.example.instagram.controllers.store;
//
//import android.content.Context;
//import android.os.Build;
//import android.util.Log;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.example.instagram.controllers.fragmentAdapters.MenuPagerAdapter;
//import com.example.instagram.controllers.fragments.SignupFragment;
//import com.example.instagram.R;
//import com.example.instagram.controllers.State;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.UnsupportedEncodingException;
//import java.util.HashMap;
//import java.util.Map;
//
//public class RequestFactory implements AbstractFactory {
//    private static RequestFactory instance;
//    private Context context;
//    private RequestInteraction mListener;
//
//    private RequestFactory(Context context){
//        this.context = context;
//        this.mListener = (RequestInteraction) context;
//    }
//
//    public static synchronized RequestFactory getInstance(Context context) {
//        if (RequestFactory.instance == null) {
//            RequestFactory.instance = new RequestFactory(context);
//        }
//        return RequestFactory.instance;
//    }
//
//    public static synchronized RequestFactory getInstance() {
//        return RequestFactory.instance;
//    }
//
//    @Override
//    public Object create(Object requestAction) {
//        Object request = null;
//        final com.example.instagram.controllers.Request.REQUEST_ACTIONS action = (com.example.instagram.controllers.Request.REQUEST_ACTIONS) requestAction;
//        if(action.equals(com.example.instagram.controllers.Request.REQUEST_ACTIONS.LOGIN)){
////            MenuPagerAdapter menuPagerAdapter = MenuPagerAdapter.getInstance();
////            if(menuPagerAdapter == null){
////                return request;
////            }
////            LoginFragment loginFragment = (LoginFragment)menuPagerAdapter.getItem(0);
////            View fragmentView = loginFragment.getView();
////
////            Map<String,String> parameters = new HashMap<String,String>();
////
////            final EditText inputUsername = (EditText) fragmentView.findViewById(R.id.inputUsername);
////            final EditText inputPassword = (EditText) fragmentView.findViewById(R.id.inputPassword);
////
////            String username = inputUsername.getText().toString();
////            String password = inputPassword.getText().toString();
////
////            if(username.equals("") || password.equals("")){
////                Toast.makeText(context, "fields should not be empty", Toast.LENGTH_SHORT).show();
////                return request;
////            }
////
////            parameters.put("query", "query GetLogin($username: String!, $password: String!){getLogin(username: $username, password: $password){token, userId}}");
////            parameters.put("variables", "{\"username\": \"" + username + "\",\"password\": \"" + password + "\"}");
////
////            String url = this.getUrl(parameters);
////
////            request = new JsonObjectRequest(
////                    Request.Method.GET,
////                    url, null,
////                    new Response.Listener<JSONObject>() {
////                        @Override
////                        public void onResponse(JSONObject response) {
////                            System.out.println(response.toString());
////                            inputUsername.setText("");
////                            inputPassword.setText("");
//////                                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
////                            try {
////                                String token = null;
////                                JSONObject jsonObject = (JSONObject) response.get("data");
////                                jsonObject = (JSONObject) jsonObject.get("getLogin");
////                                token = (String) jsonObject.get("token");
//////                                System.out.println(token);
////
////                                if(token != null){
////                                    SharedPreferences sharedPreferences = context.getSharedPreferences(MainActivity.SHARED_PREFERENCES_PREFIX, 0);
////                                    SharedPreferences.Editor editor = sharedPreferences.edit();
////
////                                    token += "Bearer " + token;
////                                    MainActivity.TOKEN = token;
////                                    editor.putString(MainActivity.SHARED_PREFERENCES_KEY_TOKEN, token);
////                                    editor.commit();
////
//////                                    State.setCurrentState(State.STATE.HOME);
////                                    mListener.onRequestSuccess();
////                                }
////                            }catch (JSONException err){
////                                Log.d("Error", err.toString());
////                            }
////                        }
////                    },
////                    new Response.ErrorListener() {
////                        @Override
////                        public void onErrorResponse(VolleyError error) {
////                            Toast.makeText(context, handleVolleyError(error), Toast.LENGTH_SHORT).show();
////                        }
////                    });
//        }
//        else if (action.equals(com.example.instagram.controllers.Request.REQUEST_ACTIONS.SIGNUP)){
//            MenuPagerAdapter menuPagerAdapter = MenuPagerAdapter.getInstance();
//            if(menuPagerAdapter == null){
//                return request;
//            }
//            SignupFragment signupFragment = (SignupFragment)menuPagerAdapter.getItem(1);
//            View fragmentView = signupFragment.getView();
//
//            Map<String,String> parameters = new HashMap<String,String>();
//
//            final EditText inputEmail = (EditText) fragmentView.findViewById(R.id.inputEmail);
//            final EditText inputFullName = (EditText) fragmentView.findViewById(R.id.inputFullName);
//            final EditText inputUsername = (EditText) fragmentView.findViewById(R.id.inputUsername);
//            final EditText inputPassword = (EditText) fragmentView.findViewById(R.id.inputPassword);
//
//            String email = inputEmail.getText().toString();
//            String fullName = inputFullName.getText().toString();
//            String username = inputUsername.getText().toString();
//            String password = inputPassword.getText().toString();
//
//            if(email.equals("") || username.equals("") || fullName.equals("") || password.equals("")){
//                Toast.makeText(context, "fields should not be empty", Toast.LENGTH_SHORT).show();
//                return request;
//            }
//
//            parameters.put("query", "mutation PostUser($email: String, $username: String, $fullName: String, $password: String){ postUser(userInput: {email: $email, username: $username, fullName: $fullName, password: $password}){status}}");
//            parameters.put("variables", "{\"email\": \"" + email + "\", \"username\": \"" + username + "\", \"fullName\": \"" + fullName + "\", \"password\": \"" + password + "\"}");
//
//            String url = this.getUrl(parameters);
//
//            request = new JsonObjectRequest(
//                    Request.Method.POST,
//                    url, null,
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            System.out.println(response.toString());
//                            inputEmail.setText("");
//                            inputFullName.setText("");
//                            inputUsername.setText("");
//                            inputPassword.setText("");
//                            State.setCurrentState(State.STATE.LOGIN);
//                            Toast.makeText(context, "Log in", Toast.LENGTH_SHORT).show();
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            Toast.makeText(context, handleVolleyError(error), Toast.LENGTH_SHORT).show();
//                        }
//                    });
//        }
////        else if(action.equals(com.example.instagram.controllers.Request.REQUEST_ACTIONS.HOME_GET_DATA)){
////
////            Map<String,String> parameters = new HashMap<String,String>();
////
////            parameters.put("query", "query GetData{getData}");
////
////            String url = this.getUrl(parameters);
////
////            request = new JsonObjectRequest(
////                    Request.Method.GET,
////                    url, null,
////                    new Response.Listener<JSONObject>() {
////                        @Override
////                        public void onResponse(JSONObject response) {
////                            System.out.println(response.toString());
////
////                            // za sada
////                            SharedPreferences sharedPreferences = context.getSharedPreferences(MainActivity.SHARED_PREFERENCES_PREFIX, 0);
////                            SharedPreferences.Editor editor = sharedPreferences.edit();
////                            MainActivity.TOKEN = null;
////                            editor.putString(MainActivity.SHARED_PREFERENCES_KEY_TOKEN, null);
////                            editor.commit();
////
////                            State.setCurrentState(State.STATE.LOGIN);
////                            mListener.onRequestSuccess();
////                        }
////                    },
////                    new Response.ErrorListener() {
////                        @Override
////                        public void onErrorResponse(VolleyError error) {
////                            Toast.makeText(context, handleVolleyError(error), Toast.LENGTH_SHORT).show();
////
////                            SharedPreferences sharedPreferences = context.getSharedPreferences(MainActivity.SHARED_PREFERENCES_PREFIX, 0);
////                            SharedPreferences.Editor editor = sharedPreferences.edit();
////                            MainActivity.TOKEN = null;
////                            editor.putString(MainActivity.SHARED_PREFERENCES_KEY_TOKEN, null);
////                            editor.commit();
////                            State.setCurrentState(State.STATE.LOGIN);
////                        }
////                    }){
////                @Override
////                public Map<String, String> getHeaders() throws AuthFailureError {
////                    HashMap<String, String> headers = new HashMap<String, String>();
////                    headers.put("Content-Type", "application/json");
////                    headers.put("Authorization", MainActivity.TOKEN);
////                    return headers;
////                }
////            };
////        }
//
//        return request;
//    }
//
//    public String handleVolleyError(VolleyError error){
//        String message = "Unknown Error";
//        if (error == null || error.networkResponse == null) {
//            return message;
//        }
//        String body;
//        //get status code here
//        final String statusCode = String.valueOf(error.networkResponse.statusCode);
//        //get response body and parse with appropriate encoding
//        try {
//            body = new String(error.networkResponse.data,"UTF-8");
//            try {
//                JSONObject jsonObject = new JSONObject(body);
////                System.out.println(jsonObject);
//
//
//
//                JSONArray lista = (JSONArray) jsonObject.get("errors");
//                jsonObject = (JSONObject) lista.get(0);
////                System.out.println(jsonObject);
//
//                try {
//                    JSONArray data = (JSONArray) jsonObject.get("data");
////                    System.out.println(data);
//                    if(data != null){
//                        jsonObject = (JSONObject) data.get(0);
//                    }
//                } catch (JSONException err){
//                    Log.d("Error", err.toString());
//                }
//
//                message = jsonObject.getString("message");
//                Integer status = jsonObject.getInt("status");
//                System.out.println("\nStatus code: " + statusCode + ", server sent: \nmessage: " + message + "\nstatus: " + Integer.toString(status));
//            }catch (JSONException err){
//                Log.d("Error", err.toString());
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        return message;
//    }
//
//    public String getUrl(Map<String,String> parameters){
////        POGLEDAJ https://hc.apache.org/httpcomponents-client-4.5.x/android-port.html
//
//        org.apache.http.client.utils.URIBuilder builderApi22AndLower = new org.apache.http.client.utils.URIBuilder();
//        cz.msebera.android.httpclient.client.utils.URIBuilder builderApi23AndHigher = new cz.msebera.android.httpclient.client.utils.URIBuilder();
//
//        if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.M) {
//            try {
//                builderApi23AndHigher.setScheme("http")
//                        .setHost(State.IPv4Address)
//                        .setPort(Integer.valueOf(State.port))
//                        .setPath("/graphql");
//
//                for (Map.Entry<String,String> entry : parameters.entrySet())
//                    builderApi23AndHigher.addParameter(entry.getKey(), entry.getValue());
//
//                builderApi23AndHigher.build();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            String myUrl = builderApi23AndHigher.toString();
//            return  myUrl;
//        }
//
//        try {
//            builderApi22AndLower.setScheme("http")
//                    .setHost(State.IPv4Address)
//                    .setPort(Integer.valueOf(State.port))
//                    .setPath("/graphql");
//
//            for (Map.Entry<String,String> entry : parameters.entrySet())
//                builderApi22AndLower.addParameter(entry.getKey(), entry.getValue());
//
//            builderApi22AndLower.build();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        String myUrl = builderApi22AndLower.toString();
//        return  myUrl;
//
//    }
//}
