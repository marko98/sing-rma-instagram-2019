package com.example.instagram.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.instagram.R;
import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.fragmentAdapters.MenuPagerAdapter;
import com.example.instagram.controllers.fragments.InstagramFragment;
import com.example.instagram.controllers.interfaces.AsyncTaskInteraction;
import com.example.instagram.controllers.recyclerView.treeViewBinders.CommentNodeBinder;
import com.example.instagram.controllers.recyclerView.treeViewBinders.CommentWithCommentsNodeBinder;
import com.example.instagram.controllers.recyclerView.treeViewBinders.PostNodeBinder;
import com.example.instagram.controllers.recyclertreeview_lib.TreeNode;
import com.example.instagram.controllers.recyclertreeview_lib.TreeViewAdapter;
import com.example.instagram.models.Comment;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class PostActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskInteraction {
    private Post post;
    private Context context = this;
    private RecyclerView postRecyclerView;
    private LayoutInflater inflater;
    private Camera camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        this.inflater = getLayoutInflater();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        MainActivity.DISPLAY_HEIGHT_PIXELS = displayMetrics.heightPixels;
        MainActivity.DISPLAY_WIDTH_PIXELS = displayMetrics.widthPixels;

        // Kupimo podatke iz prethodne aktivnosti
        Intent i = getIntent();

        String postId = i.getStringExtra("postId");
        HttpRequests.postRequest("query GetPost($postId: ID!){getPost(postId: $postId){_id,content,like {_id},comments {_id},imageUrl, creator { _id, username }, createdAt,updatedAt}}", "{\"postId\": \"" + postId + "\"}", (AsyncTaskInteraction)this, HttpRequests.REQUEST_NAME.GET_POST);
        initMenu();
    }

    private void initComponents(){
        this.postRecyclerView = findViewById(R.id.postRecyclerView);

        ArrayList<Post> posts = new ArrayList<>();
        posts.add(post);
        drawTreeView(posts);

        findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
        postRecyclerView.setVisibility(View.VISIBLE);
    }

    private void initMenu(){
        LinearLayout menuLinearLayout = findViewById(R.id.menuLinearLayout);
        if(State.getToken() != null) {
            menuLinearLayout.setVisibility(View.VISIBLE);
            LinearLayout menu = (LinearLayout) this.inflater.inflate(R.layout.menu, menuLinearLayout, true);
            ((ImageButton) menu.findViewById(R.id.homeImageButton)).setOnClickListener(this);
            ((ImageButton) menu.findViewById(R.id.searchImageButton)).setOnClickListener(this);

            this.camera = new Camera(this, (ImageButton) menu.findViewById(R.id.getImageImageButton));

            ((ImageButton) menu.findViewById(R.id.likeImageButton)).setOnClickListener(this);
            ((ImageButton) menu.findViewById(R.id.profileImageButton)).setOnClickListener(this);
        } else {
            menuLinearLayout.setVisibility(View.GONE);
            menuLinearLayout.removeAllViews();
        }
    }

    public List<TreeNode> getTreeCommentNodesForPost(Post post){
        List<TreeNode> nodes = new ArrayList<>();

        ArrayList<Comment> neposeceni = new ArrayList<>();
        for (Comment comment : post.getComments()) {
            neposeceni.add(comment);
        }
        ArrayList<Comment> poseceni = new ArrayList<>();


        ArrayList<HashMap<String, Object>> parents = new ArrayList<>();

        TreeNode<Comment> rootCommentNode = new TreeNode<>(new Comment());
        HashMap<String, Object> rootEl = new HashMap<>();
        rootEl.put("el", rootCommentNode);
        rootEl.put("numChildren", -1);
        rootEl.put("added", 0);
        parents.add(rootEl);

        while (neposeceni.size() > 0) {
            Comment naObradi = neposeceni.remove(neposeceni.size() - 1);

            TreeNode<Comment> treeNode = new TreeNode<>(naObradi);

            ((TreeNode) parents.get(parents.size() - 1).get("el")).addChild(treeNode);
            parents.get(parents.size() - 1).put("added", (Integer)parents.get(parents.size() - 1).get("added") + 1);
            if (((Integer) parents.get(parents.size() - 1).get("numChildren")).equals((Integer)parents.get(parents.size() - 1).get("added"))) {
                parents.remove(parents.size() - 1);
            }

            if (!poseceni.contains(naObradi)) {
                poseceni.add(naObradi);

                ArrayList<Comment> children = naObradi.getComments();
                if (children != null && !children.isEmpty()) {
//                    dodajemo novog roditelja
                    HashMap<String, Object> newParent = new HashMap<>();
                    newParent.put("el", treeNode);
                    newParent.put("numChildren", children.size());
                    newParent.put("added", 0);
                    parents.add(newParent);

                    for (Comment child : children) {
                        if (!poseceni.contains(child)) {
                            neposeceni.add(child);
                        }
                    }
                }
            }
        }
        return rootCommentNode.getChildList();
    }

    public void drawTreeView(ArrayList<Post> posts){
        List<TreeNode> treeNodeList = new ArrayList<>();
        for(Post post : posts){
            TreeNode parentNode = new TreeNode(post);
            for(TreeNode treeNode : getTreeCommentNodesForPost(post)){
                parentNode.addChild(treeNode);
            }
            treeNodeList.add(parentNode);
        }

        postRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        TreeViewAdapter treeViewAdapter = new TreeViewAdapter(treeNodeList, Arrays.asList(new PostNodeBinder(context), new CommentNodeBinder(context), new CommentWithCommentsNodeBinder(context)));
        treeViewAdapter.setOnTreeNodeListener(new TreeViewAdapter.OnTreeNodeListener() {
            @Override
            public boolean onClick(TreeNode node, RecyclerView.ViewHolder holder) {
                if (!node.isLeaf()) {
                    //Update and toggle the node.
                    onToggle(!node.isExpand(), holder);
//                    if (!node.isExpand())
//                        adapter.collapseBrotherNode(node);
                }
//                Toast.makeText(getContext(), node.getContent().getTheComment().getContent(), Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public void onToggle(boolean isExpand, RecyclerView.ViewHolder holder) {
                try{
                    CommentWithCommentsNodeBinder.ViewHolder dirViewHolder = (CommentWithCommentsNodeBinder.ViewHolder) holder;
                    final ImageView ivArrow = dirViewHolder.getImageViewArrow();
                    int rotateDegree = isExpand ? 180 : -180;
                    ivArrow.animate().rotationBy(rotateDegree).start();
                } catch (Exception e) {

                }
            }
        });
        postRecyclerView.setAdapter(treeViewAdapter);
    }

    @Override
    public void onBackPressed() {
        State.getPreviousState();
        super.onBackPressed();
    }

//    ----------------------------- INTERFACE METHODS IMPLEMENTATION -------------------------------

    @Override
    public void onPostGotBitmap() {
        initComponents();
    }

    @Override
    public void onHttpRequestReceived(HttpRequests.REQUEST_NAME requestName) {

    }

    @Override
    public void onHttpPostRequestRecieved(HashMap<String, Object> hashMap, HttpRequests.REQUEST_NAME requestName) {
        JSONObject responseJSON = null;
        int statusCode = (int) hashMap.get("statusCode");
        String response = (String) hashMap.get("response");

        if(statusCode == 200) {
            try {
                responseJSON = new JSONObject(response);
                JSONObject jsonObject = responseJSON.getJSONObject("data");

                switch (requestName) {
                    case GET_POST:
                        if(!jsonObject.has("getPost")){
                            break;
                        }
                        jsonObject = jsonObject.getJSONObject("getPost");
                        post = Post.fromJson(jsonObject);

                        ArrayList<Post> posts = new ArrayList<>();
                        posts.add(post);
                        HttpRequests.getPostsBitmaps((AsyncTaskInteraction) context, posts);
                        break;
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        } else {
            try {
                responseJSON = new JSONObject(response);
                JSONArray lista = responseJSON.getJSONArray("errors");
                responseJSON = lista.getJSONObject(0);
                String message = responseJSON.getString("message");
                int status = statusCode;
                if(responseJSON.has("status")){
                    status = responseJSON.getInt("status");
                }
                System.out.println("Status code: " + String.valueOf(statusCode) + ", server sent: \nmessage: " + message + "\nstatus: " + Integer.toString(status));

                if(message.equals("Not authenticated.")){
                    State.setToken(null);
                }
                Toast.makeText(State.getMainActivityContext(), message, Toast.LENGTH_SHORT).show();
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println("Error occurred! Http Status Code: " + statusCode);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.homeImageButton:
                State.setCurrentState(State.STATE.HOME);
                finish();
                break;
            case R.id.searchImageButton:
                State.setCurrentState(State.STATE.SEARCH);
                finish();
                break;
            case R.id.likeImageButton:
                State.setCurrentState(State.STATE.LIKE);
                finish();
                break;
            case R.id.profileImageButton:
                State.setCurrentState(State.STATE.PROFILE);
                finish();
                break;
        }
    }
}
