package com.example.instagram.controllers;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.instagram.controllers.fragments.InstagramFragment;
import com.example.instagram.controllers.interfaces.AsyncTaskInteraction;
import com.example.instagram.models.Comment;
import com.example.instagram.models.Post;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class HttpRequests {
    public enum REQUEST_NAME {
        GET_LOGIN,
        POST_POST,
        GET_POST
    }

    public static void getRequest(String query, String variables){

        AsyncTask<Object, Integer, HashMap<String, Object>> task = new AsyncTask<Object, Integer, HashMap<String, Object>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(HashMap<String, Object> result) {
                State.onRequestRecieved(result);
                super.onPostExecute(result);
            }

            @Override
            protected void onProgressUpdate(Integer... progress) {
            }

            @Override
            protected HashMap<String, Object> doInBackground(Object... params) {
                return sendGetRequest(params);
            }

            @SuppressWarnings("deprecation")
            private HashMap<String, Object> sendGetRequest(Object... params) {
                HashMap<String, Object> responseHashMap = new HashMap<>();

                try {
                    URIBuilder builder = new URIBuilder("http://" + State.IPv4Address + ":" + State.port + "/graphql");
                    builder.setParameter("query", (String) params[0]);
                    builder.setParameter("variables", (String) params[1]);

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(builder.build());

                    httpGet.setHeader("Accept", "application/json");
                    httpGet.setHeader("Content-type", "application/json");
                    httpGet.setHeader("Authorization", State.getToken());

                    // Making server call
                    HttpResponse response = httpClient.execute(httpGet);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();

                    responseHashMap.put("statusCode", statusCode);
                    responseHashMap.put("response", EntityUtils.toString(r_entity));
                } catch (Exception e){
                    e.printStackTrace();
                    responseHashMap.put("statusCode", 500);
                }
                return responseHashMap;
            }
        };
        task.execute(query, variables);
    }

    public static void getRequest(String query, String variables, final AsyncTaskInteraction asyncTaskInteraction, final REQUEST_NAME requestName){
        final AsyncTaskInteraction aTI = asyncTaskInteraction;

        AsyncTask<Object, Integer, HashMap<String, Object>> task = new AsyncTask<Object, Integer, HashMap<String, Object>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(HashMap<String, Object> result) {
                State.onRequestRecieved(result);
                if(aTI != null){
                    aTI.onHttpRequestReceived(requestName);
                }
                super.onPostExecute(result);
            }

            @Override
            protected void onProgressUpdate(Integer... progress) {
            }

            @Override
            protected HashMap<String, Object> doInBackground(Object... params) {
                return sendGetRequest(params);
            }

            @SuppressWarnings("deprecation")
            private HashMap<String, Object> sendGetRequest(Object... params) {
                HashMap<String, Object> responseHashMap = new HashMap<>();

                try {
                    URIBuilder builder = new URIBuilder("http://" + State.IPv4Address + ":" + State.port + "/graphql");
                    builder.setParameter("query", (String) params[0]);
                    builder.setParameter("variables", (String) params[1]);

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(builder.build());

                    httpGet.setHeader("Accept", "application/json");
                    httpGet.setHeader("Content-type", "application/json");
                    httpGet.setHeader("Authorization", State.getToken());

                    // Making server call
                    HttpResponse response = httpClient.execute(httpGet);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();

                    responseHashMap.put("statusCode", statusCode);
                    responseHashMap.put("response", EntityUtils.toString(r_entity));
                } catch (Exception e){
                    e.printStackTrace();
                    responseHashMap.put("statusCode", 500);
                }
                return responseHashMap;
            }
        };
        task.execute(query, variables);
    }

//    public static void getRequest(String query, String variables, AppCompatActivity activity, final REQUEST_NAME requestName){
//        final AsyncTaskInteraction asyncTaskInteraction = (AsyncTaskInteraction)activity;
//
//        AsyncTask<Object, Integer, HashMap<String, Object>> task = new AsyncTask<Object, Integer, HashMap<String, Object>>() {
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//            }
//
//            @Override
//            protected void onPostExecute(HashMap<String, Object> result) {
//                State.onRequestRecieved(result);
//                asyncTaskInteraction.onHttpRequestReceived(requestName);
//                super.onPostExecute(result);
//            }
//
//            @Override
//            protected void onProgressUpdate(Integer... progress) {
//            }
//
//            @Override
//            protected HashMap<String, Object> doInBackground(Object... params) {
//                return sendGetRequest(params);
//            }
//
//            @SuppressWarnings("deprecation")
//            private HashMap<String, Object> sendGetRequest(Object... params) {
//                HashMap<String, Object> responseHashMap = new HashMap<>();
//
//                try {
//                    URIBuilder builder = new URIBuilder("http://" + State.IPv4Address + ":" + State.port + "/graphql");
//                    builder.setParameter("query", (String) params[0]);
//                    builder.setParameter("variables", (String) params[1]);
//
//                    HttpClient httpClient = new DefaultHttpClient();
//                    HttpGet httpGet = new HttpGet(builder.build());
//
//                    httpGet.setHeader("Accept", "application/json");
//                    httpGet.setHeader("Content-type", "application/json");
//                    httpGet.setHeader("Authorization", State.getToken());
//
//                    // Making server call
//                    HttpResponse response = httpClient.execute(httpGet);
//                    HttpEntity r_entity = response.getEntity();
//
//                    int statusCode = response.getStatusLine().getStatusCode();
//
//                    responseHashMap.put("statusCode", statusCode);
//                    responseHashMap.put("response", EntityUtils.toString(r_entity));
//                } catch (Exception e){
//                    e.printStackTrace();
//                    responseHashMap.put("statusCode", 500);
//                }
//                return responseHashMap;
//            }
//        };
//        task.execute(query, variables);
//    }

//    public static void getRequest(String query, String variables, InstagramFragment instagramFragment, final REQUEST_NAME requestName){
//        final AsyncTaskInteraction asyncTaskInteraction = (AsyncTaskInteraction)instagramFragment;
//
//        AsyncTask<Object, Integer, HashMap<String, Object>> task = new AsyncTask<Object, Integer, HashMap<String, Object>>() {
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//            }
//
//            @Override
//            protected void onPostExecute(HashMap<String, Object> result) {
//                State.onRequestRecieved(result);
//                asyncTaskInteraction.onHttpRequestReceived(requestName);
//                super.onPostExecute(result);
//            }
//
//            @Override
//            protected void onProgressUpdate(Integer... progress) {
//            }
//
//            @Override
//            protected HashMap<String, Object> doInBackground(Object... params) {
//                return sendGetRequest(params);
//            }
//
//            @SuppressWarnings("deprecation")
//            private HashMap<String, Object> sendGetRequest(Object... params) {
//                HashMap<String, Object> responseHashMap = new HashMap<>();
//
//                try {
//                    URIBuilder builder = new URIBuilder("http://" + State.IPv4Address + ":" + State.port + "/graphql");
//                    builder.setParameter("query", (String) params[0]);
//                    builder.setParameter("variables", (String) params[1]);
//
//                    HttpClient httpClient = new DefaultHttpClient();
//                    HttpGet httpGet = new HttpGet(builder.build());
//
//                    httpGet.setHeader("Accept", "application/json");
//                    httpGet.setHeader("Content-type", "application/json");
//                    httpGet.setHeader("Authorization", State.getToken());
//
//                    // Making server call
//                    HttpResponse response = httpClient.execute(httpGet);
//                    HttpEntity r_entity = response.getEntity();
//
//                    int statusCode = response.getStatusLine().getStatusCode();
//
//                    responseHashMap.put("statusCode", statusCode);
//                    responseHashMap.put("response", EntityUtils.toString(r_entity));
//                } catch (Exception e){
//                    e.printStackTrace();
//                    responseHashMap.put("statusCode", 500);
//                }
//                return responseHashMap;
//            }
//        };
//        task.execute(query, variables);
//    }


    public static void postRequest(String query, String variables){
        AsyncTask<Object, Integer, HashMap<String, Object>> task = new AsyncTask<Object, Integer, HashMap<String, Object>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(HashMap<String, Object> result) {
                State.onRequestRecieved(result);
                super.onPostExecute(result);
            }

            @Override
            protected void onProgressUpdate(Integer... progress) {
            }

            @Override
            protected HashMap<String, Object> doInBackground(Object... params) {
                return sendGetRequest(params);
            }

            @SuppressWarnings("deprecation")
            private HashMap<String, Object> sendGetRequest(Object... params) {
                HashMap<String, Object> responseHashMap = new HashMap<>();

                try {
                    URIBuilder builder = new URIBuilder("http://" + State.IPv4Address + ":" + State.port + "/graphql");
                    builder.setParameter("query", (String) params[0]);
                    builder.setParameter("variables", (String) params[1]);

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(builder.build());

                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");
                    httpPost.setHeader("Authorization", State.getToken());

                    // Making server call
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();

                    responseHashMap.put("statusCode", statusCode);
                    responseHashMap.put("response", EntityUtils.toString(r_entity));
                } catch (Exception e){
                    e.printStackTrace();
                    responseHashMap.put("statusCode", 500);
                }
                return responseHashMap;
            }
        };
        task.execute(query, variables);
    }

    public static void postRequest(String query, String variables, final AsyncTaskInteraction asyncTaskInteraction, final REQUEST_NAME requestName){
        AsyncTask<Object, Integer, HashMap<String, Object>> task = new AsyncTask<Object, Integer, HashMap<String, Object>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(HashMap<String, Object> result) {
                asyncTaskInteraction.onHttpPostRequestRecieved(result, requestName);
                super.onPostExecute(result);
            }

            @Override
            protected void onProgressUpdate(Integer... progress) {
            }

            @Override
            protected HashMap<String, Object> doInBackground(Object... params) {
                return sendGetRequest(params);
            }

            @SuppressWarnings("deprecation")
            private HashMap<String, Object> sendGetRequest(Object... params) {
                HashMap<String, Object> responseHashMap = new HashMap<>();

                try {
                    URIBuilder builder = new URIBuilder("http://" + State.IPv4Address + ":" + State.port + "/graphql");
                    builder.setParameter("query", (String) params[0]);
                    builder.setParameter("variables", (String) params[1]);

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(builder.build());

                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");
                    httpPost.setHeader("Authorization", State.getToken());

                    // Making server call
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();

                    responseHashMap.put("statusCode", statusCode);
                    responseHashMap.put("response", EntityUtils.toString(r_entity));
                } catch (Exception e){
                    e.printStackTrace();
                    responseHashMap.put("statusCode", 500);
                }
                return responseHashMap;
            }
        };
        task.execute(query, variables);
    }


    public static void getPostsBitmaps(final AsyncTaskInteraction asyncTaskInteraction, ArrayList<Post> posts){
        AsyncTask<ArrayList<Post>, Integer, String> task = new AsyncTask<ArrayList<Post>, Integer, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String result) {
                if(asyncTaskInteraction != null){
                    asyncTaskInteraction.onPostGotBitmap();
                }
                super.onPostExecute(result);
            }

            @Override
            protected void onProgressUpdate(Integer... process) {
            }

            @Override
            protected String doInBackground(ArrayList<Post>... posts) {
                return getBitmap(posts[0]);
            }

            @SuppressWarnings("deprecation")
            private String getBitmap(ArrayList<Post> posts) {
                for(Post post : posts){
                    String postUrl = "http://" + State.IPv4Address + ":" + State.port + "/" + post.getImageUrl();
                    Bitmap decoded = ProcessBitmap.decodeSampledBitmapFromURL(postUrl);
                    if(decoded != null){
                        post.setBitmap(decoded);
                    }
                }
                return null;
            }
        };
        task.execute(posts);
    }

    public static void updateInstace(String query, String variables, Object object){
        final Object theObject = object;
        AsyncTask<Object, Integer, HashMap<String, Object>> task = new AsyncTask<Object, Integer, HashMap<String, Object>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(HashMap<String, Object> result) {
                result.put("instance", theObject);
                State.onRequestRecieved(result);
                super.onPostExecute(result);
            }

            @Override
            protected void onProgressUpdate(Integer... progress) {
            }

            @Override
            protected HashMap<String, Object> doInBackground(Object... params) {
                return sendGetRequest(params);
            }

            @SuppressWarnings("deprecation")
            private HashMap<String, Object> sendGetRequest(Object... params) {
                HashMap<String, Object> responseHashMap = new HashMap<>();

                try {
                    URIBuilder builder = new URIBuilder("http://" + State.IPv4Address + ":" + State.port + "/graphql");
                    builder.setParameter("query", (String) params[0]);
                    builder.setParameter("variables", (String) params[1]);

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(builder.build());

                    httpGet.setHeader("Accept", "application/json");
                    httpGet.setHeader("Content-type", "application/json");
                    httpGet.setHeader("Authorization", State.getToken());

                    // Making server call
                    HttpResponse response = httpClient.execute(httpGet);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();

                    responseHashMap.put("statusCode", statusCode);
                    responseHashMap.put("response", EntityUtils.toString(r_entity));
                } catch (Exception e){
                    e.printStackTrace();
                    responseHashMap.put("statusCode", 500);
                }
                return responseHashMap;
            }
        };
        task.execute(query, variables);
    }
}


//    public static void getPostsBitmaps(){
//
//
//        AsyncTask<String, Integer, String> task = new AsyncTask<String, Integer, String>() {
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//            }
//
//            @Override
//            protected void onPostExecute(String result) {
//                super.onPostExecute(result);
//            }
//
//            @Override
//            protected void onProgressUpdate(Integer... process) {
//            }
//
//            @Override
//            protected String doInBackground(String... params) {
//                return getLogin();
//            }
//
//            @SuppressWarnings("deprecation")
//            private String getLogin() {
//                ArrayList<LinearLayout> linearLayoutsList = new ArrayList<LinearLayout>();
//                for(Post post : State.getPosts()){
////                        final LinearLayout postView = (LinearLayout) inflater.inflate(R.layout.fragment_profile_post, ll, false);
////                        EditText inputContent = postView.findViewById(R.id.inputContent);
////                        Button buttonUpdate = postView.findViewById(R.id.buttonUpdate);
////
////                        final Post thePost = post;
////                        final EditText theInputRef = inputContent;
////
////                        buttonUpdate.setOnClickListener(new View.OnClickListener() {
////                            @Override
////                            public void onClick(View v) {
////                                String content = theInputRef.getText().toString();
////                                thePost.setContent(content);
////
////                                String query = "mutation PutPost($postId: ID!, $content: String!, $imageUrl: String!){putPost(postId: $postId, postInput: {content: $content, imageUrl: $imageUrl}){content}}";
////                                String variables = "{\"postId\": \"" + thePost.getId() + "\", \"content\": \"" + thePost.getContent() + "\", \"imageUrl\": \"undefined\"}";
////
////                                HttpRequests.postRequest(query, variables);
////                            }
////                        });
////
////                        inputContent.setText(post.getContent());
////
////                        ImageView imageView = ((ImageView) postView.findViewById(R.id.imageViewPost));
//                    String postUrl = "http://" + State.IPv4Address + ":" + State.port + "/" + post.getImageUrl();
//                    Bitmap decoded = ProcessBitmap.decodeSampledBitmapFromURL(postUrl);
//                    if(decoded != null){
//                        post.setBitmap(decoded);
//                    }
//
////                        linearLayoutsList.add(postView);
//
////                        ll.addView(postView);
//
//                }
//                return null;
//            }
//        };
//        task.execute();
//    }