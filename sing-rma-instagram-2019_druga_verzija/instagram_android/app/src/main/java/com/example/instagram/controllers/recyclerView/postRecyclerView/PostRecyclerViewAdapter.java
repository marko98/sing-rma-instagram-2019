//package com.example.instagram.controllers.recyclerView.postRecyclerView;
//
//import android.animation.ObjectAnimator;
//import android.content.Context;
//import android.util.SparseBooleanArray;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.example.instagram.R;
//import com.example.instagram.controllers.ProcessBitmap;
//import com.example.instagram.controllers.recyclerView.RecyclerViewItemListener;
//import com.example.instagram.models.Comment;
//import com.example.instagram.models.Post;
//import com.github.aakira.expandablelayout.ExpandableLayoutListener;
//import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
//import com.github.aakira.expandablelayout.ExpandableLinearLayout;
//import com.github.aakira.expandablelayout.Utils;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//class MyViewHolderWithoutChildren extends RecyclerView.ViewHolder{
//    public ImageView imageViewProfileImage;
//    public TextView textViewComment, textViewDate;
//
//    public MyViewHolderWithoutChildren(View itemView) {
//        super(itemView);
//        this.imageViewProfileImage = itemView.findViewById(R.id.imageViewProfileImage);
//        this.textViewComment = itemView.findViewById(R.id.textViewComment);
//        this.textViewDate = itemView.findViewById(R.id.textViewDate);
//    }
//}
//
//class MyViewHolderWithChildren extends RecyclerView.ViewHolder{
//    public ImageView imageViewProfileImage, imageViewArrow;
//    public TextView textViewComment, textViewDate;
//    public ExpandableLinearLayout expandableLinearLayout;
//
//    public MyViewHolderWithChildren(View itemView) {
//        super(itemView);
//        this.imageViewProfileImage = itemView.findViewById(R.id.imageViewProfileImage);
//        this.imageViewArrow = itemView.findViewById(R.id.imageViewArrow);
//        this.textViewComment = itemView.findViewById(R.id.textViewComment);
//        this.textViewDate = itemView.findViewById(R.id.textViewDate);
//        this.expandableLinearLayout = itemView.findViewById(R.id.expandableLinearLayout);
//    }
//}
//
//public class PostRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//    public ArrayList<Comment> values;
//    public Context context;
//    public SparseBooleanArray expandState = new SparseBooleanArray();
//    public LayoutInflater inflater;
//
//    public PostRecyclerViewAdapter(ArrayList<Comment> values){
//        this.values = values;
//        for(int i = 0; i < values.size(); i++){
//            expandState.append(i, false);
//        }
//    }
//    public PostRecyclerViewAdapter(Context context, ArrayList<Comment> values){
//        this.values = values;
//        for(int i = 0; i < values.size(); i++){
//            expandState.append(i, false);
//        }
//        this.context = context;
//        this.inflater = LayoutInflater.from(this.context);
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        if(values.get(position).getComments() != null &&
//            values.get(position).getComments().size() > 0){
//            return 1;
//        } else {
//            return 0;
//        }
//    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        this.context = parent.getContext();
//        if(viewType == 0) // Without item
//        {
//            LayoutInflater inflater = LayoutInflater.from(this.context);
//            View view = inflater.inflate(R.layout.comment, parent, false);
//            return new MyViewHolderWithoutChildren(view);
//        }
//        else {
//            LayoutInflater inflater = LayoutInflater.from(this.context);
//            View view = inflater.inflate(R.layout.comment_with_comments, parent, false);
//            return new MyViewHolderWithChildren(view);
//        }
//    }
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
//        switch (holder.getItemViewType()){
//            case 0: {
//                    MyViewHolderWithoutChildren viewHolder = (MyViewHolderWithoutChildren) holder;
//                    Comment comment = values.get(position);
//                    viewHolder.setIsRecyclable(false);
//                    viewHolder.imageViewProfileImage.setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource(context, R.drawable.user_icon, 50, 50));
//                    viewHolder.textViewComment.setText(comment.getContent());
//                    Date date = comment.getCreatedAt();
//                    String stringDate = date.getYear() + "-" + (date.getMonth() + 1) + "-" + date.getDay() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
//                    viewHolder.textViewDate.setText(stringDate);
//                }
//                break;
//            case 1: {
//                    final MyViewHolderWithChildren viewHolder = (MyViewHolderWithChildren) holder;
//                    Comment comment = values.get(position);
//                    viewHolder.setIsRecyclable(false);
//                    viewHolder.imageViewProfileImage.setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource(context, R.drawable.user_icon, 50, 50));
//                    viewHolder.textViewComment.setText(comment.getContent());
//                    Date date = comment.getCreatedAt();
//                    String stringDate = date.getYear() + "-" + (date.getMonth() + 1) + "-" + date.getDay() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
//                    viewHolder.textViewDate.setText(stringDate);
//
//                    viewHolder.expandableLinearLayout.setInRecyclerView(true);
//                    viewHolder.expandableLinearLayout.setExpanded(expandState.get(position));
//                    viewHolder.expandableLinearLayout.setListener(new ExpandableLayoutListenerAdapter() {
//                        @Override
//                        public void onAnimationStart() {
//
//                        }
//
//                        @Override
//                        public void onAnimationEnd() {
//
//                        }
//
//                        @Override
//                        public void onPreOpen() {
//                            changeRotate(viewHolder.imageViewArrow, 0f, 180f).start();
//                            expandState.put(position, true);
//                        }
//
//                        @Override
//                        public void onPreClose() {
//                            changeRotate(viewHolder.imageViewArrow, 180f, 0f).start();
//                            expandState.put(position, false);
//                        }
//
//                        @Override
//                        public void onOpened() {
//
//                        }
//
//                        @Override
//                        public void onClosed() {
//
//                        }
//                    });
//
//                    viewHolder.imageViewArrow.setRotation(expandState.get(position)?180f:0f);
//                    viewHolder.imageViewArrow.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
////                            Expandable child item
//                            viewHolder.expandableLinearLayout.toggle();
//                        }
//                    });
//
//                    ((TextView)viewHolder.expandableLinearLayout.findViewById(R.id.textView5)).setVisibility(View.VISIBLE);
//                    ((TextView)viewHolder.expandableLinearLayout.findViewById(R.id.textView5)).setText("jej");
//
////                    if(comment.getComments() != null){
////                        for(Comment c : comment.getComments()){
////                            if(c.getComments() != null && !c.getComments().isEmpty()){
////                                View view = inflater.inflate(R.layout.comment_with_comments, viewHolder.expandableLinearLayout, true);
////
////                                ((ImageView)view.findViewById(R.id.imageViewProfileImage)).setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource(context, R.drawable.user_icon, 50, 50));
////                                ((TextView)view.findViewById(R.id.textViewComment)).setText(c.getContent());
////
////                                Date d = comment.getCreatedAt();
////                                String sDate = d.getYear() + "-" + (d.getMonth() + 1) + "-" + d.getDay() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
////                                ((TextView)view.findViewById(R.id.textViewDate)).setText(sDate);
////
////                                // FALI JOS
////                            } else {
////                                View view = inflater.inflate(R.layout.comment, viewHolder.expandableLinearLayout, true);
////
////                                ((ImageView)view.findViewById(R.id.imageViewProfileImage)).setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource(context, R.drawable.user_icon, 50, 50));
////                                ((TextView)view.findViewById(R.id.textViewComment)).setText(c.getContent());
////
////                                Date d = comment.getCreatedAt();
////                                String sDate = d.getYear() + "-" + (d.getMonth() + 1) + "-" + d.getDay() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
////                                ((TextView)view.findViewById(R.id.textViewDate)).setText(sDate);
////                            }
////                        }
////                    }
//
//
//                }
//                break;
//        }
//    }
//
//    private ObjectAnimator changeRotate(ImageView imageViewArrow, float to, float from) {
//        ObjectAnimator animator = ObjectAnimator.ofFloat(imageViewArrow, "rotation", from, to);
//        animator.setDuration(300);
//        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
//        return animator;
//    }
//
//    @Override
//    public int getItemCount() {
//        return this.values.size();
//    }
//}
