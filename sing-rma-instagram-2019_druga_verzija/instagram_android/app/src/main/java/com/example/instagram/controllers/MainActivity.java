package com.example.instagram.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.instagram.R;
import com.example.instagram.controllers.fragmentAdapters.MenuPagerAdapter;
import com.example.instagram.controllers.fragments.InstagramFragment;
import com.example.instagram.controllers.interfaces.FragmentInteraction;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FragmentInteraction {
    public final static String SHARED_PREFERENCES_PREFIX = "MainActivitySharedPreferencesPrefix";
    public final static String SHARED_PREFERENCES_KEY_TOKEN = "token";

    public static int DISPLAY_HEIGHT_PIXELS;
    public static int DISPLAY_WIDTH_PIXELS;

    private ViewPager viewPager;
    private MenuPagerAdapter menuPagerAdapter;
    private Context context = this;
    private LayoutInflater inflater;
    private MyViewModel mvm;

    private Camera camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        MainActivity.DISPLAY_HEIGHT_PIXELS = displayMetrics.heightPixels;
        MainActivity.DISPLAY_WIDTH_PIXELS = displayMetrics.widthPixels;

        State.setMainActivityContext(this);
        this.readData();
        this.setWhatToObserve();
        this.initComponents();
    }

    private void setWhatToObserve(){
        this.mvm = new MyViewModel();
        this.mvm.getCurrentTokenOfState().observe((LifecycleOwner) context, new Observer<String>() {
            @Override
            public void onChanged(String token) {
                saveData();
                initMenu();
            }
        });
    }

    private void initComponents(){
        this.inflater = getLayoutInflater();
        this.viewPager = findViewById(R.id.pager);
        this.menuPagerAdapter = MenuPagerAdapter.getInstance(getSupportFragmentManager(), this.context, this.viewPager);
        this.viewPager.setAdapter(this.menuPagerAdapter);
        this.initMenu();
    }

    private void initMenu(){
        LinearLayout menuLinearLayout = findViewById(R.id.menuLinearLayout);
        if(State.getToken() != null) {
            menuLinearLayout.setVisibility(View.VISIBLE);
            LinearLayout menu = (LinearLayout) this.inflater.inflate(R.layout.menu, menuLinearLayout, true);
            ((ImageButton) menu.findViewById(R.id.homeImageButton)).setOnClickListener(this);
            ((ImageButton) menu.findViewById(R.id.searchImageButton)).setOnClickListener(this);

            this.camera = new Camera(this, (ImageButton) menu.findViewById(R.id.getImageImageButton));

            ((ImageButton) menu.findViewById(R.id.likeImageButton)).setOnClickListener(this);
            ((ImageButton) menu.findViewById(R.id.profileImageButton)).setOnClickListener(this);
        } else {
            menuLinearLayout.setVisibility(View.GONE);
            menuLinearLayout.removeAllViews();
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            Toast.makeText(context, "There's no internet connection.", Toast.LENGTH_SHORT).show();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void saveData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_PREFIX, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SHARED_PREFERENCES_KEY_TOKEN, State.getToken());
        editor.commit();
    }

    private void readData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_PREFIX,0);
        State.setToken(sharedPreferences.getString(SHARED_PREFERENCES_KEY_TOKEN, null));
    }

    @Override
    public void onBackPressed() {
        State.STATE currentState = State.getPreviousState();
        if (currentState == null){
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.homeImageButton:
                State.setCurrentState(State.STATE.HOME);
                break;
            case R.id.searchImageButton:
                State.setCurrentState(State.STATE.SEARCH);
                break;
            case R.id.likeImageButton:
                State.setCurrentState(State.STATE.LIKE);
                break;
            case R.id.profileImageButton:
                State.setCurrentState(State.STATE.PROFILE);
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.saveData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == Camera.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK)
            this.camera.onImageGet();
    }
}
