//package com.example.instagram.controllers.recyclerView.postRecyclerView;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.example.instagram.R;
//import com.example.instagram.controllers.ProcessBitmap;
//import com.example.instagram.models.Comment;
//import com.unnamed.b.atv.model.TreeNode;
//
//import java.util.Random;
//
///**
// * Created by Bogdan Melnychuk on 2/13/15.
// */
//public class SocialViewHolder extends TreeNode.BaseNodeViewHolder<SocialViewHolder.SocialItem> {
//
//    public SocialViewHolder(Context context) {
//        super(context);
//    }
//
//    @Override
//    public View createNodeView(TreeNode node, SocialItem value) {
//        final LayoutInflater inflater = LayoutInflater.from(context);
//        final View view = inflater.inflate(R.layout.comment, null, false);
//
////        final PrintView iconView = (PrintView) view.findViewById(R.id.icon);
////        iconView.setIconText(context.getResources().getString(value.icon));
////
////        TextView connectionsLabel = (TextView) view.findViewById(R.id.connections);
////        Random r = new Random();
////        connectionsLabel.setText(r.nextInt(150) + " connections");
////
////        TextView userNameLabel = (TextView) view.findViewById(R.id.username);
////        userNameLabel.setText(NAMES[r.nextInt(4)]);
////
////        TextView sizeText = (TextView) view.findViewById(R.id.size);
////        sizeText.setText(r.nextInt(10) + " items");
//
//        ImageView imageView = ((ImageView)view.findViewById(R.id.imageViewProfileImage));
//
//        ((ImageView)view.findViewById(R.id.imageViewProfileImage)).setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource(context, R.drawable.user_icon, 50, 50));
//
//        TextView tvValue = (TextView) view.findViewById(R.id.textViewComment);
//        tvValue.setText(value.getComment().getContent());
//
//        return view;
//    }
//
//    @Override
//    public void toggle(boolean active) {
//    }
//
//
//    public static class SocialItem {
//        public int icon;
//        public Comment comment;
//
//        public SocialItem(Comment comment) {
//            this.comment = comment;
//        }
//        // rest will be hardcoded
//
//
//        public int getIcon() {
//            return icon;
//        }
//
//        public void setIcon(int icon) {
//            this.icon = icon;
//        }
//
//        public Comment getComment() {
//            return comment;
//        }
//
//        public void setComment(Comment comment) {
//            this.comment = comment;
//        }
//    }
//
//}
