package com.example.instagram.controllers.recyclerView.treeViewBinders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.example.instagram.R;
import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.controllers.MyViewModel;
import com.example.instagram.controllers.ProcessBitmap;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.recyclertreeview_lib.TreeNode;
import com.example.instagram.controllers.recyclertreeview_lib.TreeViewBinder;
import com.example.instagram.models.Comment;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;

public class CommentWithCommentsNodeBinder extends TreeViewBinder<CommentWithCommentsNodeBinder.ViewHolder> {
    private Context context;

    public CommentWithCommentsNodeBinder(Context context){
        this.context = context;
    }

    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    public void bindView(ViewHolder holder, int position, TreeNode node) {
        final Comment theComment = node.getContent().getTheComment();
        final User user = theComment.getCreator();
        DateTime dateTime = theComment.getCreatedAt();

        if(user != null && user.getUsername() != null){
            if(user.getUsername().equals(State.getLoggedUser().getUsername())){
                holder.textViewProfileUsername.setText("You commented:");
            } else {
                holder.textViewProfileUsername.setText(user.getUsername() + " commented:");
            }
            holder.textViewComment.setText(theComment.getContent());

            holder.imageViewProfileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                Toast.makeText(context, user.getUsername(), Toast.LENGTH_SHORT).show();
                String query = "query GetOtherUserById($userId: ID!){getOtherUserById(userId: $userId){ _id, email, username, following { username }, followers { username }, posts {_id,content,like {_id},comments {_id},imageUrl,createdAt,updatedAt},status, createdAt, updatedAt}}";
                String variables = "{\"userId\": \"" + user.getId() + "\"}";
                HttpRequests.getRequest(query, variables);
                State.setCurrentState(State.STATE.OTHERS_PROFILE);
                }
            });
        } else {
            holder.textViewProfileUsername.setText("Unknown commented:");
            holder.textViewComment.setText(theComment.getContent());
        }

        String stringDate = "unknown";
        if(dateTime != null){
            stringDate = dateTime.getYear() + "-" + dateTime.getMonthOfYear() + "-" + dateTime.getDayOfMonth() + " " + dateTime.getHourOfDay() + ":" + dateTime.getMinuteOfHour() + ":" + dateTime.getSecondOfMinute();
            holder.textViewDate.setText(stringDate);
        } else {
            holder.textViewDate.setText(stringDate);
        }

        holder.imageViewProfileImage.setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource(context, R.drawable.user_icon, 50, 50));

        holder.commentImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, theComment.getId(), Toast.LENGTH_SHORT).show();
            }
        });

        holder.likeUnlikeImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Like/Unlike " + theComment.getId(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.comment_with_comments;
    }

    @Override
    public Comment getTheComment() {
        return this.getTheComment();
    }

    @Override
    public Post getThePost() {
        return null;
    }

    public static class ViewHolder extends TreeViewBinder.ViewHolder {
        public ImageView imageViewProfileImage, imageViewArrow, commentImageButton, likeUnlikeImageButton;
        public TextView textViewComment, textViewDate, textViewProfileUsername;

        public ViewHolder(View rootView) {
            super(rootView);
            this.imageViewProfileImage = (ImageView) rootView.findViewById(R.id.imageViewProfileImage);
            this.commentImageButton = (ImageView) rootView.findViewById(R.id.commentImageButton);
            this.likeUnlikeImageButton = (ImageView) rootView.findViewById(R.id.likeUnlikeImageButton);
            this.imageViewArrow = (ImageView) rootView.findViewById(R.id.imageViewArrow);

            this.textViewComment = (TextView) rootView.findViewById(R.id.textViewComment);
            this.textViewDate = (TextView) rootView.findViewById(R.id.textViewDate);
            this.textViewProfileUsername = (TextView) rootView.findViewById(R.id.textViewProfileUsername);
        }

        public ImageView getImageViewProfileImage() {
            return imageViewProfileImage;
        }

        public ImageView getImageViewArrow() {
            return imageViewArrow;
        }

        public TextView getTextViewComment() {
            return textViewComment;
        }

        public TextView getTextViewDate() {
            return textViewDate;
        }
    }
}