package com.example.instagram.controllers.fragments;

import androidx.fragment.app.Fragment;

import com.example.instagram.controllers.State;

public abstract class InstagramFragment extends Fragment {
    public static final String ARG_OBJECT = "Object";
    private State.STATE state;

    public enum INSTAGRAM_FRAGMENT_TYPES {
        HOME,
        SEARCH,
        LIKE,
        PROFILE,
        LOGIN,
        SIGNUP,
        POST,
        OTHER_PROFILE
    };

    public InstagramFragment(State.STATE state) {
        this.state = state;
    }

//    public void updateState(){
//        State.setCurrentState(this.state);
//    }
//    public void update(){};
}
