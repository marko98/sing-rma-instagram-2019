package com.example.instagram.controllers.volley;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class MySingleton {
    public static final String TAG = MySingleton.class.getSimpleName();
    private static MySingleton instance;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;
    private static Context context;

    private MySingleton(Context context) {
        this.context = context;
        this.requestQueue = this.getRequestQueue();

        this.imageLoader = new ImageLoader(this.requestQueue, new LruBitmapCache());
    }

    public static synchronized MySingleton getInstance(Context context) {
        if (MySingleton.instance == null) {
            MySingleton.instance = new MySingleton(context);
        }
        return MySingleton.instance;
    }

    public static synchronized MySingleton getInstance() {
        return MySingleton.instance;
    }

    public RequestQueue getRequestQueue() {
        if (this.requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            this.requestQueue = Volley.newRequestQueue(this.context.getApplicationContext());
        }
        return this.requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        this.getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (this.requestQueue != null) {
            this.requestQueue.cancelAll(tag);
        }
    }

    public ImageLoader getImageLoader() {
        return this.imageLoader;
    }
}
