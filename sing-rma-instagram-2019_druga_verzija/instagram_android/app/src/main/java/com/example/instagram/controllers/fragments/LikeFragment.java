package com.example.instagram.controllers.fragments;


import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.controllers.MyViewModel;
import com.example.instagram.controllers.interfaces.FragmentInteraction;
import com.example.instagram.R;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.recyclerView.RecyclerViewItemListener;
import com.example.instagram.controllers.recyclerView.LikeRecyclerViewAdapter;
import com.example.instagram.models.Comment;
import com.example.instagram.models.Like;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class LikeFragment extends InstagramFragment implements RecyclerViewItemListener {
    private static final String DATA = "LIKE";
    public static LikeFragment instance;

    private String data;
    private View mainView;
    private RecyclerView recyclerView;
    private LayoutInflater layoutInflater;
    private Button buttonChangeData;
    private MyViewModel mvm;

    private FragmentInteraction listener;
    private InstagramFragment likeFragment;

    public LikeFragment(String text) {
        super(State.STATE.LIKE);
        System.out.println("LikeFragment");
        Bundle args = new Bundle();
        args.putString(DATA, text);
        this.setArguments(args);
        this.mvm = new MyViewModel();
        this.likeFragment = this;
    }

    public static synchronized LikeFragment getInstance(String text) {
        if (LikeFragment.instance == null) {
            LikeFragment.instance = new LikeFragment(text);
        }
        return LikeFragment.instance;
    }

    @Override
    public void onAttach(Context context) {
        System.out.println("onAttach");
        super.onAttach(context);
        //ova metoda se poziva kada se nakaci na context, odnosno na activity i desi se sledece
        //proveravamo da li activity implementira nas interfejs
        if (context instanceof FragmentInteraction) {
            //ako implementira, dobijamo referencu na activity
            this.listener = (FragmentInteraction) context;

//            if(State.getLikes() != null && State.getLikes().isEmpty()){
////                String query = "query GetLikes{getLikes{_id, comment {_id},post {_id, imageUrl},creator {_id, username},createdAt,updatedAt}}";
//                String query = "query GetLikes{getLikes{_id, comment {_id, creator {_id}, content},post {_id, imageUrl, content, creator {username}, like {_id}, createdAt,updatedAt},creator {_id, username},createdAt,updatedAt}}";
//                String variables = "{}";
//                HttpRequests.getRequest(query, variables);
//            }
        } else {
            //u suportnom bacamo gresku, jer nije moguce komunicirati
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        System.out.println("onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            this.data = getArguments().getString(DATA);
            System.out.println("Data vrednost: " + this.data);
//            System.out.println("Data vrednost: " + this.data);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("onCreateView");
        // Inflate the layout for this fragment
        this.layoutInflater = inflater;
        View view = inflater.inflate(R.layout.fragment_like, container, false);
        this.mainView = view;

        this.recyclerView = mainView.findViewById(R.id.recyclerView);
        drawData();

        // setujemo sta zelimo da osluskujemo, inace postavljamo ga ovde jer su tu i view-ovi kreirani
        this.setWhatToObserve();
        return view;
    }

    private void setWhatToObserve(){

        this.mvm.getCurrentStateOfState().observe((LifecycleOwner) this.listener, new Observer<State.STATE>(){
            @Override
            public void onChanged(State.STATE state) {
                if(State.getCurrentState().equals(State.STATE.LIKE)) {
                    if (state.equals(State.STATE.LIKE)) {
                        drawData();
                    }
                }
            }
        });


//        this.mvm.getLikesOfState().observe((LifecycleOwner) this.listener, new Observer<ArrayList<Like>>(){
//            @Override
//            public void onChanged(ArrayList<Like> likes) {
//                if(State.getCurrentState().equals(State.STATE.LIKE)) {
//                    if (likes != null && !likes.isEmpty()) {
//                        drawData();
//                    }
//                }
//            }
//        });

        this.mvm.getLoggedUserOfState().observe((LifecycleOwner) this.listener, new Observer<User>(){
            @Override
            public void onChanged(User user) {
                drawData();
            }
        });
    }

    private void setWhatToObserve2(){
        this.mvm.getCurrentStateOfState().observe((LifecycleOwner) this.listener, new Observer<State.STATE>(){
            @Override
            public void onChanged(State.STATE state) {
                if (state.equals(State.STATE.LIKE)) {
                    drawData();
                }
            }
        });

        this.mvm.getLikesOfState().observe((LifecycleOwner) this.listener, new Observer<ArrayList<Like>>(){
            @Override
            public void onChanged(ArrayList<Like> likes) {
                if (likes != null && !likes.isEmpty()) {
                    drawData();
                }
            }
        });
    }

    private void drawData(){
        System.out.println("drawData");

        ArrayList<Like> likes = new ArrayList<>();
        User loggedUser = State.getLoggedUser();

        if(loggedUser != null){
            for(Comment comment : loggedUser.getComments()){
                for(Like like : comment.getLikes()){
                    likes.add(like);
                }
            }

            for(Post post : loggedUser.getPosts()){
                for(Like like : post.getLikes()){
                    likes.add(like);
                }
            }

            Collections.sort(likes, new Comparator<Like>() {
                @Override
                public int compare(Like l1, Like l2) {
                    return l2.getUpdatedAt().compareTo(l1.getUpdatedAt());
                }
            });

            if(!likes.isEmpty()){
                LikeRecyclerViewAdapter likeRecyclerViewAdapter = new LikeRecyclerViewAdapter((Context) listener, likes, (RecyclerViewItemListener) likeFragment);
                recyclerView.setAdapter(likeRecyclerViewAdapter);
                GridLayoutManager gridLayoutManager = new GridLayoutManager((Context) listener, 1, GridLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(gridLayoutManager);
            }
            mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
            mainView.findViewById(R.id.recyclerView).setVisibility(View.VISIBLE);
        }
    }

    public void manipulisiFragmentom() {
        System.out.println("Manipulacija nad LikeFragment-om");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        System.out.println("onViewCreated");
    }

    @Override
    public void onDetach() {
        System.out.println("onDetach");
        super.onDetach();
        this.listener = null;
    }

    //    ----------------------------- INTERFACE METHODS IMPLEMENTATION -------------------------------

    @Override
    public void onPostClick(Post post) {

    }

    @Override
    public void onLikeClick(Like like) {
        Toast.makeText((Context) listener, like.getCreator().getUsername() + " is clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCommentClick(Comment comment) {

    }
}
