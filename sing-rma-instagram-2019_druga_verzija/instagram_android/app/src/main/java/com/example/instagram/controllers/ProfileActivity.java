package com.example.instagram.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.instagram.R;
import com.example.instagram.controllers.interfaces.AsyncTaskInteraction;
import com.example.instagram.controllers.recyclerView.ProfilePostRecyclerViewAdapter;
import com.example.instagram.controllers.recyclerView.RecyclerViewItemListener;
import com.example.instagram.models.Comment;
import com.example.instagram.models.Like;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import java.util.ArrayList;
import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity implements AsyncTaskInteraction, RecyclerViewItemListener {
    private MyViewModel mvm;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        this.mvm = new MyViewModel();
        this.recyclerView = findViewById(R.id.recyclerView);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        MainActivity.DISPLAY_HEIGHT_PIXELS = displayMetrics.heightPixels;
        MainActivity.DISPLAY_WIDTH_PIXELS = displayMetrics.widthPixels;

        // Kupimo podatke iz prethodne aktivnosti
        Intent i = getIntent();

        String userId = i.getStringExtra("userId");
        String query = "query GetOtherUserById($userId: ID!){getOtherUserById(userId: $userId){ _id, email, username, following { username }, followers { username }, posts {_id,content,like {_id},comments {_id},imageUrl,createdAt,updatedAt},status, createdAt, updatedAt}}";
        String variables = "{\"userId\": \"" + userId + "\"}";
        HttpRequests.getRequest(query, variables);

        this.mvm.getOtherUserOfState().observe((LifecycleOwner) this, new Observer<User>(){
            @Override
            public void onChanged(User user) {
                if (user != null && user.getPosts() != null && user.getFollowers() != null && user.getFollowings() != null) {
                    ArrayList<Post> posts = user.getPosts();
                    if(!posts.isEmpty()){
                        drawData(posts);
                    }
                } else {
                    findViewById(R.id.progressBarSpinnerHead).setVisibility(View.VISIBLE);
                    findViewById(R.id.constraintLayoutHead).setVisibility(View.GONE);
                }
            }
        });
    }

    private void drawData(ArrayList<Post> posts){
        User user = State.getOtherUser();

        if (user != null && user.getPosts() != null && user.getFollowers() != null && user.getFollowings() != null) {
            findViewById(R.id.progressBarSpinnerHead).setVisibility(View.GONE);
            findViewById(R.id.constraintLayoutHead).setVisibility(View.VISIBLE);

            ((TextView) findViewById(R.id.textViewUsername)).setText(State.getOtherUser().getUsername());
            ((TextView) findViewById(R.id.textViewUsername2)).setText(State.getOtherUser().getUsername());
            ((EditText) findViewById(R.id.editTextStatus)).setText(State.getOtherUser().getStatus());

            ((TextView) findViewById(R.id.textViewNumOfPosts)).setText(String.valueOf(State.getOtherUser().getPosts().size()) + "\n Posts");
            ((TextView) findViewById(R.id.textViewNumOfFollowers)).setText(String.valueOf(State.getOtherUser().getFollowers().size()) + "\n Followers");
            ((TextView) findViewById(R.id.textViewNumOfFollowing)).setText(String.valueOf(State.getOtherUser().getFollowings().size()) + "\n Following");

            ((ImageView) findViewById(R.id.imageViewProfileImage)).setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource((Context) this, R.drawable.user_icon));
        }

        if(posts != null){
            ArrayList<Post> postsToGetBitmap = new ArrayList<>();
            for(Post post : posts){
                if(post.getBitmap() == null){
                    postsToGetBitmap.add(post);
                }
            }
            if(!postsToGetBitmap.isEmpty()){
                HttpRequests.getPostsBitmaps((AsyncTaskInteraction) this, postsToGetBitmap);
                ((ProgressBar) findViewById(R.id.progressBarSpinner)).setVisibility(View.VISIBLE);
                this.recyclerView.setVisibility(View.GONE);
            } else {
                ProfilePostRecyclerViewAdapter postRecyclerViewAdapter = new ProfilePostRecyclerViewAdapter((Context) this, posts, (RecyclerViewItemListener) this);
                this.recyclerView.setAdapter(postRecyclerViewAdapter);
                GridLayoutManager gridLayoutManager = new GridLayoutManager((Context) this, 3, GridLayoutManager.VERTICAL, false);
                this.recyclerView.setLayoutManager(gridLayoutManager);

                ((ProgressBar) findViewById(R.id.progressBarSpinner)).setVisibility(View.GONE);
                this.recyclerView.setVisibility(View.VISIBLE);
            }
        }
    }

    //    ----------------------------- INTERFACE METHODS IMPLEMENTATION -------------------------------

    @Override
    public void onPostGotBitmap() {
        if(State.getOtherUser() != null && State.getOtherUser().getPosts() != null)
            drawData(State.getOtherUser().getPosts());
        ((ProgressBar) findViewById(R.id.progressBarSpinner)).setVisibility(View.GONE);
        this.recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHttpRequestReceived(HttpRequests.REQUEST_NAME requestName) {

    }

    @Override
    public void onHttpPostRequestRecieved(HashMap<String, Object> hashMap, HttpRequests.REQUEST_NAME requestName) {

    }

    @Override
    public void onPostClick(Post post) {
        Toast.makeText((Context) this, post.getId() + " is clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLikeClick(Like like) {

    }

    @Override
    public void onCommentClick(Comment comment) {

    }
}
