package com.example.instagram.controllers;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.instagram.R;
import com.example.instagram.models.Post;

import org.apache.http.HttpHeaders;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.HttpResponse;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class UploadActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private String filePath = null;
    private TextView txtPercentage;
    private EditText editTextComment;
    private Button btnUpload;
    private Context context = this;
    long totalSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        MainActivity.DISPLAY_HEIGHT_PIXELS = displayMetrics.heightPixels;
        MainActivity.DISPLAY_WIDTH_PIXELS = displayMetrics.widthPixels;

        State.setCurrentState(State.STATE.POSTAPOST);

        txtPercentage = (TextView) findViewById(R.id.txtPercentage);
        editTextComment = (EditText) findViewById(R.id.editTextComment);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        // Receiving the data from previous activity
        Intent i = getIntent();

        // image path that is captured from previous activity
        filePath = i.getStringExtra("filePath");

        try{
            ((ImageView) findViewById(R.id.imageViewProfileImage)).setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource(context, R.drawable.user_icon, 50, 50));
            ((ImageView) findViewById(R.id.imageViewPost)).setImageBitmap(ProcessBitmap.decodeSampledBitmapFromFilePath(filePath, 200, 200));
            ((TextView) findViewById(R.id.textViewProfileUsername)).setText(State.getLoggedUser().getUsername());

            DateTime dateTime = DateTime.now();
            String stringDate = dateTime.getYear() + "-" + dateTime.getMonthOfYear() + "-" + dateTime.getDayOfMonth() + " " + dateTime.getHourOfDay() + ":" + dateTime.getMinuteOfHour() + ":" + dateTime.getSecondOfMinute();
            ((TextView) findViewById(R.id.textViewDate)).setText(stringDate);
        } catch (Exception e){
            e.printStackTrace();
        }


        // boolean flag to identify the media type, image or video
        boolean isImage = i.getBooleanExtra("isImage", true);

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // uploading the file to server
                UploadFileToServer uf = new UploadFileToServer(context);
                uf.uploadFileToServer();
            }
        });
    }

    private class UploadFileToServer {
        public Context context;

        public UploadFileToServer(Context context){
            this.context = context;
        }

        public void uploadFileToServer(){
            AsyncTask<Void, Integer, String> task = new AsyncTask<Void, Integer, String>(){
                @Override
                protected void onPreExecute() {
                    // setting progress bar to zero
                    progressBar.setProgress(0);
                    super.onPreExecute();
                }

                @Override
                protected void onPostExecute(String result) {
                    if(result.equals("Not authenticated.")){
                        Toast.makeText(context, result, Toast.LENGTH_SHORT).show();

                        SharedPreferences sharedPreferences = context.getSharedPreferences(MainActivity.SHARED_PREFERENCES_PREFIX, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(MainActivity.SHARED_PREFERENCES_KEY_TOKEN, null);
                        editor.commit();
                        super.onPostExecute(result);
                    } else {
                        State.setCurrentState(State.getPreviousState());
                    }
                    super.onPostExecute(result);
                    finish();
                }

                @Override
                protected void onProgressUpdate(Integer... progress) {
                    // Making progress bar visible
                    progressBar.setVisibility(View.VISIBLE);

                    // updating progress bar value
                    progressBar.setProgress(progress[0]);

                    // updating percentage value
                    txtPercentage.setText(String.valueOf(progress[0]) + "%");
                }


                @Override
                protected String doInBackground(Void... params) {
                    return uploadFile();
                }

                @SuppressWarnings("deprecation")
                private String uploadFile() {
                    String responseString = null;

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost("http://" + State.IPv4Address + ":" + State.port + "/feed/post-image");
                    String token = State.getToken();
                    httpPost.setHeader(HttpHeaders.AUTHORIZATION, token);

                    try {
                        AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                                new AndroidMultiPartEntity.ProgressListener() {
                                    @Override
                                    public void transferred(long num) {
                                        publishProgress((int) ((num / (float) totalSize) * 100));
                                    }
                                }
                        );

                        File sourceFile = new File(filePath);

                        // Adding file to http body
                        entity.addPart("image", new FileBody(sourceFile));

                        totalSize = entity.getContentLength();
                        httpPost.setEntity(entity);

                        // Making server call
                        HttpResponse response = httpClient.execute(httpPost);
                        HttpEntity r_entity = response.getEntity();

                        int statusCode = response.getStatusLine().getStatusCode();
                        if (statusCode == 200){
                            // Server response
                            responseString = EntityUtils.toString(r_entity);

                            JSONObject responseJSON = null;
                            try {
                                responseJSON = new JSONObject(responseString);
                                String imagePath = (String) responseJSON.get("filePath");

                                String query = "mutation PostPost($content: String!, $imageUrl: String!){postPost(postInput: {content: $content, imageUrl: $imageUrl}){_id, content, like { _id }, comments { _id }, imageUrl, creator { _id }, createdAt, updatedAt}}";
                                String content = editTextComment.getText().toString();
                                String variables =  "{\"content\": \"" + content + "\",\"imageUrl\": \"" + imagePath + "\"}";
                                HttpRequests.postRequest(query, variables);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            responseString = EntityUtils.toString(r_entity);

                            JSONObject responseJSON = null;
                            try {
                                responseJSON = new JSONObject(responseString);
                                String message = responseJSON.getString("message");

                                if(message.equals("Not authenticated.")){
                                    State.setToken(null);
                                    State.setPosts(new ArrayList<Post>());
                                    responseString = "Not authenticated.";
                                } else {
                                    responseString = "\nStatus code: " + statusCode + ", server sent: \nmessage: " + message + "";
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                responseString = "Error occurred! Http Status Code: " + statusCode;
                            }
                        }
                    } catch (ClientProtocolException e) {
                        responseString = e.toString();
                    } catch (IOException e) {
                        responseString = e.toString();
                    }

                    return responseString;
                }
            };
            task.execute();
        }
    }
}



