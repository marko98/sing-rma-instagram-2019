//package com.example.instagram.controllers.recyclerView.postRecyclerView;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.example.instagram.R;
//import com.example.instagram.controllers.ProcessBitmap;
//import com.example.instagram.models.Comment;
//import com.unnamed.b.atv.model.TreeNode;
//
//public class TreeViewMyHolder extends TreeNode.BaseNodeViewHolder<TreeViewMyHolder.IconTreeItem> {
//    public Context context;
//
//    public TreeViewMyHolder(Context context) {
//        super(context);
//        this.context = context;
//    }
//
//    @Override
//    public View createNodeView(TreeNode node, IconTreeItem value) {
//        Comment comment = value.getComment();
//
//        final LayoutInflater inflater = LayoutInflater.from(this.context);
//        final View view = inflater.inflate(R.layout.comment, null, false);
//
//        ((ImageView)view.findViewById(R.id.imageViewProfileImage)).setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource(this.context, R.drawable.user_icon, 50, 50));
//
//        final TextView textViewComment = (TextView) view.findViewById(R.id.textViewComment);
//        textViewComment.setText("asdas");
//
//        return view;
//    }
//
//    @Override
//    public void toggle(boolean active) {
//    }
//
//    public static class IconTreeItem {
//        public int icon;
//        public String text;
//        public Comment comment;
//
//        public IconTreeItem(Comment comment){
//            this.comment = comment;
//        }
//
//        public int getIcon() {
//            return icon;
//        }
//
//        public void setIcon(int icon) {
//            this.icon = icon;
//        }
//
//        public String getText() {
//            return text;
//        }
//
//        public void setText(String text) {
//            this.text = text;
//        }
//
//        public Comment getComment() {
//            return comment;
//        }
//
//        public void setComment(Comment comment) {
//            this.comment = comment;
//        }
//    }
//
//}
