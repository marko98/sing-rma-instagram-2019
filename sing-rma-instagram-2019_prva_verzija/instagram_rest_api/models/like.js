const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const likeSchema = new Schema({
    comment: {
        type: Schema.Types.ObjectId,
        ref: 'Comment',
    },
    post: {
        type: Schema.Types.ObjectId,
        ref: 'Post',
    },
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
}, {
    // cini da se zapisuje vreme kreiranje i update-ovanja kao dodatna dva jos polja(createdAt, updatedAt)
    timestamps: true
})

module.exports = mongoose.model('Like', likeSchema);