package com.example.instagram.controllers.recyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.instagram.R;
import com.example.instagram.controllers.MainActivity;
import com.example.instagram.controllers.PostActivity;
import com.example.instagram.models.Post;

import java.util.ArrayList;

public class ProfilePostRecyclerViewAdapter extends RecyclerView.Adapter<ProfilePostRecyclerViewAdapter.ViewHolder> {
    public ArrayList<Post> values;
    public Context context;
    protected RecyclerViewItemListener itemListener;

    public ProfilePostRecyclerViewAdapter(Context context, ArrayList values, RecyclerViewItemListener itemListener){
        this.context = context;
        this.values = values;
        this.itemListener = itemListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imageView;
        public RelativeLayout relativeLayout;
        public Post post;

        public ViewHolder(View v){
            super(v);
            v.setOnClickListener(this);
            this.relativeLayout = v.findViewById(R.id.constraintLayout);
            this.imageView = v.findViewById(R.id.imageView);
        }

        public void setData(Post post){
            this.post = post;

            final Post thePost = post;

            this.imageView.setImageBitmap(post.getBitmap());
            this.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(context instanceof MainActivity){
                        Toast.makeText(context, thePost.getId(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(context , PostActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("postId", thePost.getId());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public void onClick(View v) {
            if(itemListener != null){
                itemListener.onPostClick(this.post);
            }
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.setData(this.values.get(position));
    }

    @Override
    public ProfilePostRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.fragment_profile_post, parent,false);

        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            int margin = MainActivity.DISPLAY_WIDTH_PIXELS/10/8;
            p.setMargins(margin, margin, margin, margin);
            view.requestLayout();
        }

        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return this.values.size();
    }
}
