package com.example.instagram.controllers;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.instagram.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Camera {
    // Standard storage location for digital camera files
    private static final String CAMERA_DIR = "/dcim/";
    private static final String ALBUM_NAME = "MyInstagram";
    public static final int REQUEST_IMAGE_CAPTURE = 1;

    private String currentImagePath;
//    private ImageView imageView;
//    private Button buttonTakePic;
    private Context context;

    public Camera(Context context, ImageButton imageButton){
        this.context = context;
        this.setBtnListenerOrDisable(imageButton, takePictureOnClickListener, MediaStore.ACTION_IMAGE_CAPTURE);
    }

    ImageButton.OnClickListener takePictureOnClickListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            dispatchTakePictureIntent();
        }
    };

    public static boolean isIntentAvailable(Context context, String action){
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent(action);
        if(intent.resolveActivity(packageManager) != null)
            return true;
        return false;
    }

    private void setBtnListenerOrDisable(ImageButton btn, ImageButton.OnClickListener onClickListener, String intentName){
        if(isIntentAvailable(this.context, intentName)){
            btn.setOnClickListener(onClickListener);
        }
        else {
            Toast.makeText(this.context, "No Intent to handle get pic from camera.", Toast.LENGTH_SHORT);
//            btn.setText(R.string.cannot + " " + btn.getText());
            btn.setClickable(false);
        }
    }

    private void dispatchTakePictureIntent(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        /**
         * resolveActivity() returns the first activity component that can handle the intent
         */
        if(takePictureIntent.resolveActivity(this.context.getPackageManager()) != null) {
            // Create the File where the photo should go
            File imageFile = null;

            try {
                imageFile = setupImageFile();

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));

                State.setCurrentState(State.STATE.CAMERAGALARY);
                ((AppCompatActivity)this.context).startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            } catch (IOException ex) {
                ex.printStackTrace();
                imageFile = null;
                this.currentImagePath = null;
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "_IMG_" + timeStamp + "_";
        File albumDirectory = getAlbumDir();
        File imageFile = File.createTempFile(imageFileName, ".jpg", albumDirectory);
        return imageFile;
    }

    private File getAlbumDir(){
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        File storageDir = null;

        // Check if external storage is mounted
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            storageDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + CAMERA_DIR + ALBUM_NAME);

//            String absolutePath = storageDir.getAbsolutePath(); //abstract pathname
            if(storageDir != null){
                /**
                 * Creates the directory named by this abstract pathname, including any necessary
                 * but nonexistent parent directories
                 *
                 * Note that if this operation fails it may have succeeded in creating some of the
                 * necessary parent directories
                 */
                if(!storageDir.mkdirs()){
                    // Tests whether the file or directory denoted by this abstract pathname exists
                    if(!storageDir.exists()){
                        // Send a DEBUG log message
                        Log.d("MyInstagram", "failed to create directory");
                        return null;
                    }
                }
            }
        } else {
            // Priority constant for the println method; use Log.v
            // Send a VERBOSE log message
            Log.v(this.context.getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }
        return storageDir;
    }

    private File setupImageFile() throws IOException {
        File imageFile = createImageFile();
        this.currentImagePath = imageFile.getAbsolutePath();

        return imageFile;
    }

    protected void onImageGet(){
        if(this.currentImagePath != null){
//                this.setPic();
            this.galleryAddPic();
            this.currentImagePath = null;
        }
    }

    /**
     * The following example method demonstrates how to invoke the system's media scanner to add
     * your photo to the Media Provider's database, making it available in the Android Gallery
     * application and to other apps.
     */
    private void galleryAddPic(){
        System.out.println("SAVE TO GALLERY");
        /**
         * action android.intent.action.MEDIA_SCANNER_SCAN_FILE -> Broadcast Action: Request the
         * media scanner to scan a file and add it to the media database
         */
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(this.currentImagePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.context.sendBroadcast(mediaScanIntent);

        Intent i = new Intent(this.context, UploadActivity.class);
        i.putExtra("filePath", this.currentImagePath);
        i.putExtra("isImage", true);
        this.context.startActivity(i);
    }
}
