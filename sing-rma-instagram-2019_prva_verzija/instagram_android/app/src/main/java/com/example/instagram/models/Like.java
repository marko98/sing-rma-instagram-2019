package com.example.instagram.models;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class Like {
    private String id;
    private Post post;
    private Comment comment;
    private User creator;
    private DateTime createdAt, updatedAt;

    public Like() {}

    public Like(String id, Post post, Comment comment, User creator,
                DateTime createdAt, DateTime updatedAt) {
        this.id = id;
        this.post = post;
        this.comment = comment;
        this.creator = creator;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public static Like fromJson(JSONObject o){
        Like like = new Like();
        if(o.has("_id")){
            try {
                like.setId(o.getString("_id"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("comment_with_comments")){
            try {
                like.setComment(Comment.fromJson(o.getJSONObject("comment_with_comments")));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("post")){
            try {
                like.setPost(Post.fromJson(o.getJSONObject("post")));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("creator")){
            try {
                like.setCreator(User.fromJson(o.getJSONObject("creator")));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("createdAt")){
            try {
                String dateTimeString = o.getString("createdAt");
                DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                DateTime dateTime = fmt.parseDateTime(dateTimeString);
                dateTime = dateTime.plusHours(1);

                like.setCreatedAt(dateTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(o.has("updatedAt")){
            try {
                String dateTimeString = o.getString("updatedAt");
                DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                DateTime dateTime = fmt.parseDateTime(dateTimeString);
                dateTime = dateTime.plusHours(1);
                like.setUpdatedAt(dateTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return like;
    }

    public static ArrayList<Like> fromJsonArray(JSONArray array){
            ArrayList<Like> lista = new ArrayList<Like>();

            for (int i = 0; i < array.length(); i++) {
                try {
                    JSONObject o = array.getJSONObject(i);
                    lista.add(Like.fromJson(o));
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        return lista;
    }
}
