package com.example.instagram.controllers.recyclerView;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.instagram.R;
import com.example.instagram.controllers.MainActivity;
import com.example.instagram.controllers.ProcessBitmap;
import com.example.instagram.controllers.State;
import com.example.instagram.models.Like;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;

public class LikeRecyclerViewAdapter extends RecyclerView.Adapter<LikeRecyclerViewAdapter.ViewHolder> {
    public ArrayList<Like> values;
    public Context context;
    protected RecyclerViewItemListener itemListener;

    public LikeRecyclerViewAdapter(Context context, ArrayList values, RecyclerViewItemListener itemListener){
        this.context = context;
        this.values = values;
        this.itemListener = itemListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ProgressBar progressBarSpinner;
        public ImageView imageView, postImageView;
        public TextView textView;
        public ConstraintLayout constraintLayout;
        public Like like;

        public ViewHolder(View v){
            super(v);
            v.setOnClickListener(this);
            this.constraintLayout = v.findViewById(R.id.constraintLayout);
            this.textView = v.findViewById(R.id.textView);
            this.imageView = v.findViewById(R.id.imageView);
            this.postImageView = v.findViewById(R.id.postImageView);
            this.progressBarSpinner = v.findViewById(R.id.progressBarSpinner);
        }

        public void setData(Like like){
            this.like = like;

            this.imageView.setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource(context, R.drawable.user_icon, 50, 50));
            DateTime dateTime = this.like.getCreatedAt();
//            yyyy-mm-dd hh:mm:ss
            String stringDate = dateTime.getYear() + "-" + dateTime.getMonthOfYear() + "-" + dateTime.getDayOfMonth() + " " + dateTime.getHourOfDay() + ":" + dateTime.getMinuteOfHour() + ":" + dateTime.getSecondOfMinute();
            final Like theLike = like;
            if(this.like.getPost() != null){
                this.textView.setText(this.like.getCreator().getUsername() + " has liked your post on " + stringDate);

                AsyncTask<String, Integer, Bitmap> task = new AsyncTask<String, Integer, Bitmap>() {
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                    }

                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        progressBarSpinner.setVisibility(View.GONE);
                        postImageView.setImageBitmap(bitmap);
                        postImageView.setVisibility(View.VISIBLE);
                        super.onPostExecute(bitmap);
                    }

                    @Override
                    protected void onProgressUpdate(Integer... process) {
                    }

                    @Override
                    protected Bitmap doInBackground(String... params) {
                        return getBitmap();
                    }

                    @SuppressWarnings("deprecation")
                    private Bitmap getBitmap() {
                        return ProcessBitmap.decodeSampledBitmapFromURL("http://" + State.IPv4Address + ":" + State.port + "/" + theLike.getPost().getImageUrl(), 100, 100);
                    }
                };
                task.execute();
            } else {
                progressBarSpinner.setVisibility(View.GONE);
                this.textView.setText(this.like.getCreator().getUsername() + " has liked your comment on " + stringDate);
            }
        }

        @Override
        public void onClick(View v) {
            if(itemListener != null){
                itemListener.onLikeClick(this.like);
            }
        }
    }

    @Override
    public void onBindViewHolder(LikeRecyclerViewAdapter.ViewHolder viewHolder, int position) {
        viewHolder.setData(this.values.get(position));
    }

    @Override
    public LikeRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.like, parent,false);

        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            int margin = MainActivity.DISPLAY_WIDTH_PIXELS/10/8;
            p.setMargins(margin, margin, margin, margin);
            view.requestLayout();
        }

        return new LikeRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return this.values.size();
    }
}
