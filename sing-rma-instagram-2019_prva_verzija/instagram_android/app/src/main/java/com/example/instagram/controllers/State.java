package com.example.instagram.controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.instagram.models.Comment;
import com.example.instagram.models.Like;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class State{
    public enum STATE {
        LOGIN,
        SIGNUP,
        HOME,
        SEARCH,
        LIKE,
        PROFILE,
        CAMERAGALARY,
        POSTAPOST,
        POST,
        OTHERS_PROFILE
    }
    private static Context mainActivityContext = null;
    private static STATE currentState = STATE.LOGIN;
    private static String TOKEN = null;
    private static User loggedUser = null;
    private static ArrayList<Like> likes = new ArrayList<Like>();
    private static ArrayList<Post> posts = new ArrayList<Post>();

    public static String IPv4Address = "192.168.0.29";
    public static String port = "8080";
    public static int requestsForUpdatingPostsToRecive = 0;

    public State() {
        State.posts = new ArrayList<Post>();
    }

    public static STATE getCurrentState() {
        return currentState;
    }

    public static void setCurrentState(STATE currentState) {
        System.out.println("previous state changed(" + State.currentState + "), current state: " + currentState);
        State.currentState = currentState;
        MyViewModel.fetchStateOfState();
    }

    public static ArrayList<Post> getPosts() {
        return posts;
    }

    public static void setPosts(ArrayList<Post> posts) {
        State.posts = posts;
        if(State.requestsForUpdatingPostsToRecive == 0){
            MyViewModel.fetchPostsOfState();
        }
    }

    public static void addPostToPosts(Post post){

        post.setCreator(State.getLoggedUser());
        ArrayList<Post> posts = new ArrayList<>();
        posts.add(post);
        for(Post p : State.posts){
          posts.add(p);
        }
        State.posts = posts;
        if(State.requestsForUpdatingPostsToRecive == 0){
            MyViewModel.fetchPostsOfState();
        }
    }

    public static void addPostsToPosts(ArrayList<Post> posts){
        if(State.getPosts() == null){
            State.setPosts(new ArrayList<Post>());
        }

        for(Post postToAdd : posts){
            boolean exists = false;
            for(Post postInPosts : State.getPosts()){
                if(postToAdd.getId().equals(postInPosts.getId())){
                    exists = true;
                }
            }
            if(!exists){
                State.posts.add(postToAdd);
            }
        }
        if(State.requestsForUpdatingPostsToRecive == 0){
            MyViewModel.fetchPostsOfState();
        }
    }

    public static void updatePost(Post post){
        for(int i = 0; i < State.posts.size(); i++){
            if(State.posts.get(i).getId().equals(post.getId())){
                ArrayList<Post> posts = new ArrayList<>();
                for(int j = 0; j < State.posts.size(); j++){
                    if(i == j){
                        posts.add(post);
                    } else {
                        posts.add(State.posts.get(j));
                    }
                }
                if(State.requestsForUpdatingPostsToRecive == 0){
                    MyViewModel.fetchPostsOfState();
                }
                return;
            }
        }
    }

    public static String getToken() {
        return State.TOKEN;
    }

    public static void setToken(String token) {
        if(token == null){
            State.TOKEN = token;
            MyViewModel.fetchTokenOfState();
            State.setPosts(new ArrayList<Post>());
            State.setCurrentState(State.STATE.LOGIN);
        } else {
            System.out.println("Previous token: " + State.TOKEN + "), current token: " + token);
            State.TOKEN = token;
            MyViewModel.fetchTokenOfState();
        }
    }

    public static User getLoggedUser() {
        return loggedUser;
    }

    public static void setLoggedUser(User loggedUser) {
        State.loggedUser = loggedUser;
    }

    public static ArrayList<Like> getLikes() {
        return State.likes;
    }

    public static void setLikes(ArrayList<Like> likes) {
        State.likes = likes;
        MyViewModel.fetchLikesOfState();
    }

    public static void addLikesToLikes(Like like){
        ArrayList<Like> likes = new ArrayList<>();
        likes.add(like);
        for(Like l : State.likes){
            likes.add(l);
        }
        State.likes = likes;
        MyViewModel.fetchLikesOfState();
    }

    public static void addLikesToLikes(ArrayList<Like> likes){
        for(Like likeToAdd : likes){
            boolean exists = false;
            for(Like likeInLikes : State.likes){
                if(likeToAdd.getId().equals(likeInLikes.getId())){
                    exists = true;
                }
            }
            if(!exists){
                State.likes.add(likeToAdd);
            }
        }
        MyViewModel.fetchLikesOfState();
    }

    public static void onRequestRecieved(HashMap<String, Object> responseHashMap) {
        JSONObject responseJSON = null;
        int statusCode = (int)responseHashMap.get("statusCode");
        String response = (String) responseHashMap.get("response");
        Object instance = responseHashMap.get("instance");

        if(statusCode == 200){
            try {
                String token = null;
                responseJSON = new JSONObject(response);
                JSONObject jsonObject = responseJSON.getJSONObject("data");

                if(jsonObject.has("getLogin")){
                    jsonObject = jsonObject.getJSONObject("getLogin");
                    token = (String) jsonObject.get("token");
                    token = "Bearer " + token;

                    State.setLoggedUser(User.fromJson(jsonObject.getJSONObject("loggedUser")));
                    State.setToken(token);
                    State.setCurrentState(State.STATE.HOME);
                }
                else if(jsonObject.has("getUser")){
                    State.setLoggedUser(User.fromJson(jsonObject.getJSONObject("getUser")));
                }
                else if(jsonObject.has("postPost")) {
                    jsonObject = jsonObject.getJSONObject("postPost");
                    State.addPostToPosts(Post.fromJson(jsonObject));
                }
                else if(jsonObject.has("getAllPosts")){
                    JSONArray lista = jsonObject.getJSONArray("getAllPosts");
                    State.addPostsToPosts(Post.fromJsonArray(lista));
                }
                else if(jsonObject.has("postUser")){
                    Toast.makeText(State.getMainActivityContext(), "Log in", Toast.LENGTH_SHORT).show();
                    State.setCurrentState(STATE.LOGIN);
                }
                else if(jsonObject.has("getLikes")){
                    JSONArray lista = jsonObject.getJSONArray("getLikes");
                    State.addLikesToLikes(Like.fromJsonArray(lista));
                }

                if(instance != null){
                    if(jsonObject.has("getComment")){
                        State.requestsForUpdatingPostsToRecive --;
                        jsonObject = jsonObject.getJSONObject("getComment");
                        Comment oldComment = (Comment) instance;
                        oldComment.updateComment(Comment.fromJson(jsonObject));
                        if(State.requestsForUpdatingPostsToRecive == 0){
                            MyViewModel.fetchLoggedUserOfState();
                            MyViewModel.fetchPostsOfState();
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                responseJSON = new JSONObject(response);
                JSONArray lista = responseJSON.getJSONArray("errors");
                responseJSON = lista.getJSONObject(0);
                String message = responseJSON.getString("message");
                int status = statusCode;
                if(responseJSON.has("status")){
                    status = responseJSON.getInt("status");
                }
                System.out.println("Status code: " + String.valueOf(statusCode) + ", server sent: \nmessage: " + message + "\nstatus: " + Integer.toString(status));

                if(message.equals("Not authenticated.")){
                    State.setToken(null);
                }
                Toast.makeText(State.getMainActivityContext(), message, Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println("Error occurred! Http Status Code: " + statusCode);
            }
        }
    }

    public static Context getMainActivityContext() {
        return mainActivityContext;
    }

    public static void setMainActivityContext(Context mainActivityContext) {
        State.mainActivityContext = mainActivityContext;
    }
}
