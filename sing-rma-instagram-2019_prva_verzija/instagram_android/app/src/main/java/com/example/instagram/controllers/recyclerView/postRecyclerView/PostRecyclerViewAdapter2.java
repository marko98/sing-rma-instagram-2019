//package com.example.instagram.controllers.recyclerView.postRecyclerView;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import androidx.constraintlayout.widget.ConstraintLayout;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.example.instagram.R;
//import com.example.instagram.controllers.MainActivity;
//import com.example.instagram.controllers.fragments.HomeFragment;
//import com.example.instagram.controllers.recyclerView.RecyclerViewItemListener;
//import com.example.instagram.models.MyTree;
//import com.example.instagram.models.Post;
//import com.unnamed.b.atv.model.TreeNode;
//import com.unnamed.b.atv.view.AndroidTreeView;
//
//import java.util.ArrayList;
//
//public class PostRecyclerViewAdapter2 extends RecyclerView.Adapter<PostRecyclerViewAdapter2.ViewHolder> {
//    public ArrayList<Post> values;
//    public Context context;
//    protected RecyclerViewItemListener itemListener;
//
//    public PostRecyclerViewAdapter2(Context context, ArrayList values, RecyclerViewItemListener itemListener){
//        this.context = context;
//        this.values = values;
//        this.itemListener = itemListener;
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//        public ImageView imageView;
//        public TextView textViewPostContent, textViewPostNumLikes;
//        public LinearLayout linearLayout;
//        public ConstraintLayout constraintLayout;
//        public Post post;
//
//        public ViewHolder(View v){
//            super(v);
//            v.setOnClickListener(this);
//            this.constraintLayout = v.findViewById(R.id.constraintLayout);
//            this.imageView = v.findViewById(R.id.imageView);
//            this.textViewPostContent = v.findViewById(R.id.textViewPostContent);
//            this.textViewPostNumLikes = v.findViewById(R.id.textViewPostNumLikes);
//            this.linearLayout = v.findViewById(R.id.linearLayout);
//        }
//
//        public void setData(Post post){
//            this.post = post;
//
//            this.imageView.setImageBitmap(post.getBitmap());
//            this.textViewPostContent.setText(post.getContent());
//
//            if(post.getLikes().size() > 0){
//                this.textViewPostNumLikes.setText("Number of likes: " + post.getLikes().size());
//            } else {
//                this.textViewPostNumLikes.setVisibility(View.GONE);
//            }
//
//            if(post.getComments() != null && post.getComments().size() > 0){
//                TreeNode treeNode = MyTree.getAndroidTreeView(this.post, context);
//                AndroidTreeView treeView = new AndroidTreeView(((HomeFragment)itemListener).getActivity(), treeNode);
//
//                View view = treeView.getView();
//                treeView.setDefaultAnimation(true);
//
//                this.linearLayout.removeAllViews();
//                this.linearLayout.addView(view);
//            }
//        }
//
//        @Override
//        public void onClick(View v) {
//            if(itemListener != null){
//                itemListener.onPostClick(this.post);
//            }
//        }
//    }
//
//    @Override
//    public void onBindViewHolder(ViewHolder viewHolder, int position) {
//        viewHolder.setData(this.values.get(position));
//    }
//
//    @Override
//    public PostRecyclerViewAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(this.context).inflate(R.layout.post, parent,false);
//
//        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
//            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
//            int margin = MainActivity.DISPLAY_WIDTH_PIXELS/10/8;
//            p.setMargins(margin, margin, margin, margin);
//            view.requestLayout();
//        }
//
//        return new ViewHolder(view);
//    }
//
//    @Override
//    public int getItemCount() {
//        return this.values.size();
//    }
//}
