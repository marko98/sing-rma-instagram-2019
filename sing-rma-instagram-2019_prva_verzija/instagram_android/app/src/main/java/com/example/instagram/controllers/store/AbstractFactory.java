package com.example.instagram.controllers.store;

public interface AbstractFactory {
    public Object create(Object type);
}