package com.example.instagram.controllers.recyclertreeview_lib;

import com.example.instagram.models.Comment;
import com.example.instagram.models.Post;

/**
 * Created by tlh on 2016/10/1 :)
 */

public interface LayoutItemType {
    int getLayoutId();

    Comment getTheComment();

    Post getThePost();
}
