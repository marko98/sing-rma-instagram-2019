package com.example.instagram.controllers.recyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.instagram.R;
import com.example.instagram.controllers.MainActivity;
import com.example.instagram.controllers.ProcessBitmap;
import com.example.instagram.models.Comment;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;

public class CommentRecyclerViewAdapter extends RecyclerView.Adapter<CommentRecyclerViewAdapter.ViewHolder> {
    public ArrayList<Comment> values;
    public Context context;
    protected RecyclerViewItemListener itemListener;

    public CommentRecyclerViewAdapter(Context context, ArrayList values, RecyclerViewItemListener itemListener){
        this.context = context;
        this.values = values;
        this.itemListener = itemListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imageView;
        public TextView textViewComment, textViewDate;
        public Comment comment;

        public ViewHolder(View v){
            super(v);
            v.setOnClickListener(this);
            this.imageView = v.findViewById(R.id.imageViewProfileImage);
            this.textViewComment = v.findViewById(R.id.textViewComment);
            this.textViewDate = v.findViewById(R.id.textViewDate);
        }

        public void setData(Comment comment){
            this.comment = comment;

            String content = comment.getContent();

            this.imageView.setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource(context, R.drawable.user_icon, 50, 50));
            this.textViewComment.setText(this.comment.getContent());
            DateTime dateTime = comment.getCreatedAt();
            String stringDate = dateTime.getYear() + "-" + dateTime.getMonthOfYear() + "-" + dateTime.getDayOfMonth() + " " + dateTime.getHourOfDay() + ":" + dateTime.getMinuteOfHour() + ":" + dateTime.getSecondOfMinute();
            this.textViewDate.setText(stringDate);
        }

        @Override
        public void onClick(View v) {
            if(itemListener != null){
                itemListener.onCommentClick(this.comment);
            }
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.setData(this.values.get(position));
    }

    @Override
    public CommentRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.comment, parent,false);

        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            int margin = MainActivity.DISPLAY_WIDTH_PIXELS/10/8;
            p.setMargins(margin, margin, margin, margin);
            view.requestLayout();
        }

        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return this.values.size();
    }
}
