//package com.example.instagram.controllers;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Cache;
//import com.android.volley.NetworkResponse;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//
//import java.util.Map;
//
//public class Request extends com.android.volley.Request {
//    public enum REQUEST_TYPES {
//        JSONOBJECTREQUEST
//    }
//    public enum REQUEST_ACTIONS {
//        LOGIN,
//        SIGNUP,
//        POSTAPOST,
//        HOME_GET_DATA
//    }
//
//    public Request(String url, Response.ErrorListener listener) {
//        super(url, listener);
//    }
//
//    @Override
//    public int getMethod() {
//        return super.getMethod();
//    }
//
//    @Override
//    public com.android.volley.Request<?> setTag(Object tag) {
//        return super.setTag(tag);
//    }
//
//    @Override
//    public Object getTag() {
//        return super.getTag();
//    }
//
//    @Nullable
//    @Override
//    public Response.ErrorListener getErrorListener() {
//        return super.getErrorListener();
//    }
//
//    @Override
//    public int getTrafficStatsTag() {
//        return super.getTrafficStatsTag();
//    }
//
//    @Override
//    public com.android.volley.Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
//        return super.setRetryPolicy(retryPolicy);
//    }
//
//    @Override
//    public void addMarker(String tag) {
//        super.addMarker(tag);
//    }
//
//    @Override
//    public com.android.volley.Request<?> setRequestQueue(RequestQueue requestQueue) {
//        return super.setRequestQueue(requestQueue);
//    }
//
//    @Override
//    public String getUrl() {
//        return super.getUrl();
//    }
//
//    @Override
//    public String getCacheKey() {
//        return super.getCacheKey();
//    }
//
//    @Override
//    public com.android.volley.Request<?> setCacheEntry(Cache.Entry entry) {
//        return super.setCacheEntry(entry);
//    }
//
//    @Override
//    public Cache.Entry getCacheEntry() {
//        return super.getCacheEntry();
//    }
//
//    @Override
//    public void cancel() {
//        super.cancel();
//    }
//
//    @Override
//    public boolean isCanceled() {
//        return super.isCanceled();
//    }
//
//    @Override
//    public Map<String, String> getHeaders() throws AuthFailureError {
//        return super.getHeaders();
//    }
//
//    @Override
//    protected Map<String, String> getPostParams() throws AuthFailureError {
//        return super.getPostParams();
//    }
//
//    @Override
//    protected String getPostParamsEncoding() {
//        return super.getPostParamsEncoding();
//    }
//
//    @Override
//    public String getPostBodyContentType() {
//        return super.getPostBodyContentType();
//    }
//
//    @Override
//    public byte[] getPostBody() throws AuthFailureError {
//        return super.getPostBody();
//    }
//
//    @Override
//    protected Map<String, String> getParams() throws AuthFailureError {
//        return super.getParams();
//    }
//
//    @Override
//    protected String getParamsEncoding() {
//        return super.getParamsEncoding();
//    }
//
//    @Override
//    public String getBodyContentType() {
//        return super.getBodyContentType();
//    }
//
//    @Override
//    public byte[] getBody() throws AuthFailureError {
//        return super.getBody();
//    }
//
//    @Override
//    public Priority getPriority() {
//        return super.getPriority();
//    }
//
//    @Override
//    public RetryPolicy getRetryPolicy() {
//        return super.getRetryPolicy();
//    }
//
//    @Override
//    public void markDelivered() {
//        super.markDelivered();
//    }
//
//    @Override
//    public boolean hasHadResponseDelivered() {
//        return super.hasHadResponseDelivered();
//    }
//
//    @Override
//    protected VolleyError parseNetworkError(VolleyError volleyError) {
//        return super.parseNetworkError(volleyError);
//    }
//
//    @Override
//    public void deliverError(VolleyError error) {
//        super.deliverError(error);
//    }
//
//    @Override
//    public int compareTo(com.android.volley.Request other) {
//        return super.compareTo(other);
//    }
//
//    @Override
//    public String toString() {
//        return super.toString();
//    }
//
//    @Override
//    public int hashCode() {
//        return super.hashCode();
//    }
//
//    @Override
//    public boolean equals(@Nullable Object obj) {
//        return super.equals(obj);
//    }
//
//    @NonNull
//    @Override
//    protected Object clone() throws CloneNotSupportedException {
//        return super.clone();
//    }
//
//    @Override
//    protected void finalize() throws Throwable {
//        super.finalize();
//    }
//
//    @Override
//    protected Response parseNetworkResponse(NetworkResponse response) {
//        return null;
//    }
//
//    @Override
//    protected void deliverResponse(Object response) {
//
//    }
//
//    @Override
//    public int compareTo(Object o) {
//        return 0;
//    }
//}
