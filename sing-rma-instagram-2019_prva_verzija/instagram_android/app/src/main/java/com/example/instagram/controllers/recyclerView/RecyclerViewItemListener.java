package com.example.instagram.controllers.recyclerView;

import com.example.instagram.models.Comment;
import com.example.instagram.models.Like;
import com.example.instagram.models.Post;

public interface RecyclerViewItemListener {
    void onPostClick(Post post);
    void onLikeClick(Like like);
    void onCommentClick(Comment comment);
}
