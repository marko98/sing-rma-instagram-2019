package com.example.instagram.controllers.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.controllers.MyViewModel;
import com.example.instagram.controllers.ProcessBitmap;
import com.example.instagram.controllers.interfaces.AsyncTaskInteraction;
import com.example.instagram.controllers.interfaces.FragmentInteraction;
import com.example.instagram.R;
import com.example.instagram.controllers.State;
import com.example.instagram.models.User;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;

public class LoginFragment extends InstagramFragment implements AsyncTaskInteraction {
    private static final String DATA = "LOGIN";
    private static LoginFragment instance;

    private String data;
    private View mainView;
    private LayoutInflater layoutInflater;
    private MyViewModel mvm;

    private FragmentInteraction listener;

    public LoginFragment() {
        super(State.STATE.LOGIN);
        System.out.println("LoginFragment");
        Bundle args = new Bundle();
        args.putString(DATA, "Login");
        this.setArguments(args);
        this.mvm = new MyViewModel();
    }

    public static synchronized LoginFragment getInstance() {
        if (LoginFragment.instance == null) {
            LoginFragment.instance = new LoginFragment();
        }
        return LoginFragment.instance;
    }

    @Override
    public void onAttach(Context context) {
        System.out.println("onAttach");
        super.onAttach(context);
        //ova metoda se poziva kada se nakaci na context, odnosno na activity i desi se sledece
        //proveravamo da li activity implementira nas interfejs
        if (context instanceof FragmentInteraction) {
            //ako implementira, dobijamo referencu na activity
            this.listener = (FragmentInteraction) context;
//            listener.onLoginLoaded();
        } else {
            //u suportnom bacamo gresku, jer nije moguce komunicirati
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        System.out.println("onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            this.data = getArguments().getString(DATA);
            System.out.println("Data vrednost: " + this.data);
//            System.out.println("Data vrednost: " + this.data);

            this.data = "Login";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("onCreateView");
        // Inflate the layout for this fragment
        this.layoutInflater = inflater;
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        this.mainView = view;
        drawData();
        return view;
    }

    @Override
    public void update() {
        super.update();
    }

    private void drawData(){
        System.out.println("drawData");

        ((TextView) this.mainView.findViewById(R.id.labelSignup)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                State.setCurrentState(State.STATE.SIGNUP);
            }
        });

        ((Button) this.mainView.findViewById(R.id.buttonLogin)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText inputUsername = (EditText) mainView.findViewById(R.id.inputUsername);
                final EditText inputPassword = (EditText) mainView.findViewById(R.id.inputPassword);

                String username = inputUsername.getText().toString();
                String password = inputPassword.getText().toString();

                String query = "query GetLogin($username: String!, $password: String!){getLogin(username: $username, password: $password){token, loggedUser { userId, email, username, following { username }, followers { username }, status, createdAt, updatedAt }}}";
                String variables = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\"}";

                mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.VISIBLE);
                HttpRequests.getRequest(query, variables, instance, HttpRequests.REQUEST_NAME.GET_LOGIN);
            }
        });
    }

    public View getView(){
        return this.mainView;
    }

    public void manipulisiFragmentom() {
        System.out.println("Manipulacija nad LoginFragmentom-om");
    }

    @Override
    public void onDetach() {
        System.out.println("onDetach");
        super.onDetach();
        this.listener = null;
    }

    //    ----------------------------- INTERFACE METHODS IMPLEMENTATION -------------------------------

    @Override
    public void onPostGotBitmap() {

    }

    @Override
    public void onHttpRequestReceived(HttpRequests.REQUEST_NAME requestName) {
        switch (requestName){
            case GET_LOGIN:
                mainView.findViewById(R.id.progressBarSpinner).setVisibility(View.GONE);
                break;
            default:
        }
    }

    @Override
    public void onHttpPostRequestRecieved(HashMap<String, Object> hashMap, HttpRequests.REQUEST_NAME requestName) {

    }
}
