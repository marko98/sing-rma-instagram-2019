package com.example.instagram.models;

import com.example.instagram.R;
import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.recyclertreeview_lib.LayoutItemType;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class Comment implements LayoutItemType {
    private String id, content;
    private ArrayList<Like> likes;
    private ArrayList<Comment> comments;
    private Comment comment;
    private Post post;
    private User creator;
    private DateTime createdAt, updatedAt;

    public Comment() {
        this.comments = new ArrayList<>();
        this.likes = new ArrayList<>();
    }

    public Comment(String id, String content,
                   ArrayList<Like> likes, ArrayList<Comment> comments,
                   Comment comment, Post post, User creator,
                   DateTime createdAt, DateTime updatedAt) {
        this.id = id;
        this.content = content;
        this.likes = likes;
        this.comments = comments;
        this.comment = comment;
        this.post = post;
        this.creator = creator;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public void addInComments(Comment comment){
        this.comments.add(comment);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ArrayList<Like> getLikes() {
        return likes;
    }

    public void setLikes(ArrayList<Like> likes) {
        this.likes = likes;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void updateComment(Comment newComment){
        this.setId(newComment.getId());
        this.setContent(newComment.getContent());
        this.setLikes(newComment.getLikes());
        this.setComments(newComment.getComments());
        this.setComment(newComment.getComment());
        this.setPost(newComment.getPost());
        this.setCreator(newComment.getCreator());
        this.setCreatedAt(newComment.getCreatedAt());
        this.setUpdatedAt(newComment.getUpdatedAt());
    }

    public static Comment fromJson(JSONObject o){
        Comment comment = new Comment();
        if(o.has("_id")){
            try {
                comment.setId(o.getString("_id"));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("content")){
            try {
                comment.setContent(o.getString("content"));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("like")){
            try {
                comment.setLikes(Like.fromJsonArray(o.getJSONArray("like")));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("comments")){
            try {
                comment.setComments(Comment.fromJsonArray(o.getJSONArray("comments")));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("comment_with_comments")){
            try {
                comment.setComment(Comment.fromJson(o.getJSONObject("comment_with_comments")));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("post")){
            try {
                comment.setPost(Post.fromJson(o.getJSONObject("post")));
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        if(o.has("creator")){
            try {
                comment.setCreator(User.fromJson(o.getJSONObject("creator")));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("createdAt")){
            try {
                String dateTimeString = o.getString("createdAt");
                DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                DateTime dateTime = fmt.parseDateTime(dateTimeString);
                dateTime = dateTime.plusHours(1);
                comment.setCreatedAt(dateTime);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if(o.has("updatedAt")){
            try {
                String dateTimeString = o.getString("updatedAt");
                DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
                DateTime dateTime = fmt.parseDateTime(dateTimeString);
                dateTime = dateTime.plusHours(1);
                comment.setUpdatedAt(dateTime);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        return comment;
    }

    public static ArrayList<Comment> fromJsonArray(JSONArray array){
        ArrayList<Comment> lista = new ArrayList<Comment>();

        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject o = array.getJSONObject(i);
                Comment comment = Comment.fromJson(o);


                State.requestsForUpdatingPostsToRecive ++;
                String query = "query GetComment($commentId: ID!){getComment(commentId: $commentId){_id,content,comments {_id}creator {_id,username} createdAt, updatedAt }}";
                HttpRequests.updateInstace(query, "{\"commentId\": \"" + comment.getId() + "\"}", comment);


                lista.add(comment);
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        return lista;
    }

    //    ----------------------------- INTERAFCE METHODS IMPLEMENTATION -------------------------------

    @Override
    public int getLayoutId() {
        if(this.getComments() != null && this.getComments().size() > 0){
            return R.layout.comment_with_comments;
        }
        else {
            return R.layout.comment;
        }
    }

    @Override
    public Comment getTheComment() {
        return this;
    }

    @Override
    public Post getThePost() {
        return null;
    }
}
