package com.example.instagram.controllers.recyclerView.treeViewBinders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.instagram.controllers.MainActivity;
import com.example.instagram.controllers.PostActivity;
import com.example.instagram.R;
import com.example.instagram.controllers.ProcessBitmap;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.recyclertreeview_lib.TreeNode;
import com.example.instagram.controllers.recyclertreeview_lib.TreeViewBinder;
import com.example.instagram.models.Comment;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import org.joda.time.DateTime;

public class PostNodeBinder extends TreeViewBinder<PostNodeBinder.ViewHolder> {
    private Context context;

    public PostNodeBinder(Context context){
        this.context = context;
    }

    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    public void bindView(ViewHolder holder, int position, TreeNode node) {
        final Post thePost = node.getContent().getThePost();
        final User user = thePost.getCreator();
        DateTime dateTime = thePost.getCreatedAt();

        if(context instanceof PostActivity){
            if(thePost.getCreator().getUsername().equals(State.getLoggedUser().getUsername())){
                ViewGroup viewGroup = (ViewGroup) holder.linearLayoutMyPost;

                View view = LayoutInflater.from(this.context).inflate(R.layout.my_post, viewGroup,true);

                viewGroup.setVisibility(View.VISIBLE);
            }
        }

        if(thePost.getComments() != null && thePost.getComments().size() > 0){
            holder.textViewShowComments.setVisibility(View.VISIBLE);
        }

        if(user != null && user.getUsername() != null){
            holder.textViewProfileUsername.setText(user.getUsername());
            holder.textViewPostContent.setText(thePost.getContent());
        } else {
            holder.textViewProfileUsername.setText("Unknown");
            holder.textViewPostContent.setText(thePost.getContent());
        }
        if(user != null && user.getId() != null) {
            holder.textViewProfileUsername.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Go to page, " + user.getId(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        if(thePost != null){
            String stringDate = "unknown";
            if(dateTime != null){
                stringDate = dateTime.getYear() + "-" + dateTime.getMonthOfYear() + "-" + dateTime.getDayOfMonth() + " " + dateTime.getHourOfDay() + ":" + dateTime.getMinuteOfHour() + ":" + dateTime.getSecondOfMinute();
                holder.textViewDate.setText(stringDate);
            } else {
                holder.textViewDate.setText(stringDate);
            }

            if(thePost.getLikes() != null){
                holder.textViewNumLikes.setText(thePost.getLikes().size() + " likes");
            } else {
                holder.textViewNumLikes.setText("0 likes");
            }
        }

        holder.imageViewPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof MainActivity){
                    Toast.makeText(context, thePost.getId(), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(context ,PostActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("postId", thePost.getId());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            }
        });
        holder.imageViewPost.setImageBitmap(thePost.getBitmap());
        holder.imageViewProfileImage.setImageBitmap(ProcessBitmap.decodeSampledBitmapFromResource(context, R.drawable.user_icon, 50, 50));

        holder.commentImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Comment: " + thePost.getId(), Toast.LENGTH_SHORT).show();
            }
        });

        holder.likeUnlikeImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Like/Unlike " + thePost.getId(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.post;
    }

    @Override
    public Comment getTheComment() {
        return null;
    }

    @Override
    public Post getThePost() {
        return this.getThePost();
    }

    public static class ViewHolder extends TreeViewBinder.ViewHolder {
        public ImageView imageViewPost, imageViewProfileImage, commentImageButton, likeUnlikeImageButton;
        public TextView textViewProfileUsername, textViewDate, textViewNumLikes, textViewPostContent, textViewShowComments;
        private LinearLayout linearLayoutMyPost;

        public ViewHolder(View rootView) {
            super(rootView);
            this.imageViewPost = (ImageView) rootView.findViewById(R.id.imageViewPost);
            this.imageViewProfileImage = (ImageView) rootView.findViewById(R.id.imageViewProfileImage);
            this.commentImageButton = (ImageView) rootView.findViewById(R.id.commentImageButton);
            this.likeUnlikeImageButton = (ImageView) rootView.findViewById(R.id.likeUnlikeImageButton);

            this.textViewProfileUsername = (TextView) rootView.findViewById(R.id.textViewProfileUsername);
            this.textViewDate = (TextView) rootView.findViewById(R.id.textViewDate);
            this.textViewNumLikes = (TextView) rootView.findViewById(R.id.textViewNumLikes);
            this.textViewPostContent = (TextView) rootView.findViewById(R.id.textViewPostContent);
            this.textViewShowComments = (TextView) rootView.findViewById(R.id.textViewShowComments);

            this.linearLayoutMyPost = (LinearLayout) rootView.findViewById(R.id.linearLayoutMyPost);
        }
    }
}
