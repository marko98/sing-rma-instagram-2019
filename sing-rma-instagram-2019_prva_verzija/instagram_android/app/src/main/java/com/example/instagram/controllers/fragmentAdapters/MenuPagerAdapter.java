package com.example.instagram.controllers.fragmentAdapters;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.viewpager.widget.ViewPager;

import com.example.instagram.controllers.fragments.InstagramFragment;
import com.example.instagram.controllers.MyViewModel;
import com.example.instagram.controllers.State;
import com.example.instagram.controllers.store.InstagramFragmentFactory;

import java.util.ArrayList;

public class MenuPagerAdapter extends FragmentStatePagerAdapter {
    private static MenuPagerAdapter instance;
    private ViewPager viewPager;
    private InstagramFragmentFactory instagramFragmentFactory;
    private MyViewModel mvm;

    private ArrayList<InstagramFragment> fragments;

    private MenuPagerAdapter(FragmentManager fm, final Context context, final ViewPager viewPager){
        super(fm);
        this.viewPager = viewPager;

        this.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                InstagramFragment i = (InstagramFragment) getItem(position);
                i.updateState();
                i.update();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        this.instagramFragmentFactory = new InstagramFragmentFactory();
        fragments = new ArrayList<>();
        if((State.getCurrentState().equals(State.STATE.LOGIN) || State.getCurrentState().equals(State.STATE.SIGNUP))){
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.LOGIN));
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.SIGNUP));
        } else {
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.HOME));
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.SEARCH));
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.LIKE));
            fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.PROFILE));
        }


        this.mvm = new MyViewModel();
        this.mvm.getCurrentStateOfState().observe((LifecycleOwner) context, new Observer<State.STATE>() {
            @Override
            public void onChanged(State.STATE state) {
                if((state.equals(State.STATE.LOGIN) || state.equals(State.STATE.SIGNUP)) && fragments.size()!=2){
                    fragments.clear();
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.LOGIN));
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.SIGNUP));
                    notifyDataSetChanged();
                } else if((state.equals(State.STATE.HOME) || state.equals(State.STATE.SEARCH) || state.equals(State.STATE.LIKE) || state.equals(State.STATE.PROFILE)) && fragments.size()!=4){
                    fragments.clear();
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.HOME));
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.SEARCH));
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.LIKE));
                    fragments.add((InstagramFragment) instagramFragmentFactory.create(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.PROFILE));
                    notifyDataSetChanged();
                }

                switch (state){
                    case HOME:
                        setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.HOME);
                        break;
                    case LOGIN:
                        setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.LOGIN);
                        break;
                    case SIGNUP:
                        setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES.SIGNUP);
                        break;
                }
            }
        });

        this.mvm.getCurrentTokenOfState().observe((LifecycleOwner) context, new Observer<String>() {
            @Override
            public void onChanged(String token) {
                if(token != null){
                    State.setCurrentState(State.STATE.HOME);
                } else {
                    State.setCurrentState(State.STATE.LOGIN);
                }
            }
        });
    }

    public static synchronized MenuPagerAdapter getInstance(FragmentManager fm, Context context, ViewPager viewPager){
        if(MenuPagerAdapter.instance == null){
            MenuPagerAdapter.instance = new MenuPagerAdapter(fm, context, viewPager);
        }
        return MenuPagerAdapter.instance;
    }

    public static synchronized MenuPagerAdapter getInstance(){
        return MenuPagerAdapter.instance;
    }

    @Override
    public int getItemPosition(Object object) {
        int index = this.fragments.indexOf(object);
        if(this.fragments.indexOf(object) == -1){
            return POSITION_NONE;
        }
        return index;
    }

    @Override
    public Fragment getItem(int position) {
        System.out.println(position);
        Fragment fragment = (Fragment) this.fragments.get(position);

        Bundle args = new Bundle();
        args.putInt(InstagramFragment.ARG_OBJECT, position + 1);
        fragment.setArguments(args);
        return fragment;
    }

    public void setCurrentItem(InstagramFragment.INSTAGRAM_FRAGMENT_TYPES type){
        switch (type){
            case LOGIN:
            case HOME:
                this.viewPager.setCurrentItem(0);
                break;
            case SIGNUP:
            case SEARCH:
                this.viewPager.setCurrentItem(1);
                break;
            case LIKE:
                this.viewPager.setCurrentItem(2);
                break;
            case PROFILE:
                this.viewPager.setCurrentItem(3);
                break;
        }
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return "OBJECT " + (position + 1);
    }

}
