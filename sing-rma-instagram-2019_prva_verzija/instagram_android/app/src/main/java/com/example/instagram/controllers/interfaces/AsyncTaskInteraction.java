package com.example.instagram.controllers.interfaces;

import com.example.instagram.controllers.HttpRequests;
import com.example.instagram.models.Post;

import java.util.HashMap;

public interface AsyncTaskInteraction {
    void onPostGotBitmap();
    void onHttpRequestReceived(HttpRequests.REQUEST_NAME requestName);
    void onHttpPostRequestRecieved(HashMap<String, Object> hashMap, HttpRequests.REQUEST_NAME requestName);
}
