package com.example.instagram.controllers;

import android.os.Looper;
import android.widget.LinearLayout;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.instagram.models.Like;
import com.example.instagram.models.Post;
import com.example.instagram.models.User;

import java.util.ArrayList;

public class MyViewModel extends ViewModel {
    private static MutableLiveData<State.STATE> state;
    private static MutableLiveData<ArrayList<Post>> posts;
    private static MutableLiveData<String> token;
    private static MutableLiveData<User> loggedUser;
    private static MutableLiveData<ArrayList<Like>> likes;

    public LiveData<State.STATE> getCurrentStateOfState(){
        if(MyViewModel.state == null){
            MyViewModel.state = new MutableLiveData<>();
            MyViewModel.fetchStateOfState();
        }
        return state;
    }

    public static void fetchStateOfState(){
        if(MyViewModel.state == null){
            MyViewModel.state = new MutableLiveData<>();
        }
        MyViewModel.state.setValue(State.getCurrentState());
    }

    public LiveData<String> getCurrentTokenOfState(){
        if(MyViewModel.token == null){
            MyViewModel.token = new MutableLiveData<>();
            MyViewModel.fetchTokenOfState();
        }
        return token;
    }

    public static void fetchTokenOfState(){
        if(MyViewModel.token == null){
            MyViewModel.token = new MutableLiveData<>();
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            MyViewModel.token.setValue(State.getToken());
        } else {
//          ukoliko nismo u glavnom, main Thread-u mora se koristiti postValue, a ne setValue
            MyViewModel.token.postValue(State.getToken());
        }
    }

    public LiveData<ArrayList<Post>> getPostsOfState(){
        if(MyViewModel.posts == null){
            MyViewModel.posts = new MutableLiveData<>();
            MyViewModel.fetchPostsOfState();
        }
        return posts;
    }

    public static void fetchPostsOfState(){
        if(MyViewModel.posts == null){
            MyViewModel.posts = new MutableLiveData<>();
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            MyViewModel.posts.setValue(State.getPosts());
        } else {
//          ukoliko nismo u glavnom, main Thread-u mora se koristiti postValue, a ne setValue
            MyViewModel.posts.postValue(State.getPosts());
        }
    }

    public LiveData<ArrayList<Like>> getLikesOfState(){
        if(MyViewModel.likes == null){
            MyViewModel.likes = new MutableLiveData<>();
            MyViewModel.fetchLikesOfState();
        }
        return MyViewModel.likes;
    }

    public static void fetchLikesOfState(){
        if(MyViewModel.likes == null){
            MyViewModel.likes = new MutableLiveData<>();
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            MyViewModel.likes.setValue(State.getLikes());
        } else {
//          ukoliko nismo u glavnom, main Thread-u mora se koristiti postValue, a ne setValue
            MyViewModel.likes.postValue(State.getLikes());
        }
    }

    public LiveData<User> getLoggedUserOfState(){
        if(MyViewModel.loggedUser == null){
            MyViewModel.loggedUser = new MutableLiveData<>();
            MyViewModel.fetchLoggedUserOfState();
        }
        return MyViewModel.loggedUser;
    }

    public static void fetchLoggedUserOfState(){
        if(MyViewModel.loggedUser == null){
            MyViewModel.loggedUser = new MutableLiveData<>();
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            MyViewModel.loggedUser.setValue(State.getLoggedUser());
        } else {
//          ukoliko nismo u glavnom, main Thread-u mora se koristiti postValue, a ne setValue
            MyViewModel.loggedUser.postValue(State.getLoggedUser());
        }
    }
}
